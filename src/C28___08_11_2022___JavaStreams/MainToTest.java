package C28___08_11_2022___JavaStreams;

import java.util.*;
import java.util.stream.Collectors;

public class MainToTest {
    public static void main(String[] args) {
        Employee employeeOana = new Employee("Oana", 1000, 33);
        Employee employeeMarinela = new Employee("Marinela", 900, 33);
        Employee employeeIoan = new Employee("Ioan", 1500, 24);
        Employee employeeDarius = new Employee("Darius", 1500, 32);
        Employee employeeAdrian = new Employee("Adrian", 1500, 30);
        Employee employeeCristina = new Employee("Cristina", 1500, 26);

        Department departmentHR = new Department("HR");
        departmentHR.addEmployee(employeeIoan);
        Department departmentDev = new Department("Development");
        departmentDev.addEmployee(employeeOana);
        departmentDev.addEmployee(employeeMarinela);
        departmentDev.addEmployee(employeeDarius);
        departmentDev.addEmployee(employeeAdrian);
        departmentDev.addEmployee(employeeCristina);

        List<Department> departments = new ArrayList<>();
        departments.add(departmentHR);
        departments.add(departmentDev);

        System.out.println("1. -----------------------------------------------");
        departments.stream()        // Aici parcurge lista de departamente  - Here it goes through the department list.
                .flatMap(department -> department.getEmployeeList().stream())          // -> through the employee list
                .forEach(System.out::println);                                        //              Method reference
        System.out.println("2. -----------------------------------------------");
        departments.stream()
                .flatMap(department -> department.getEmployeeList().stream())
                .reduce((employee1, employee2) -> employee1.getAge() < employee2.getAge() ? employee1 : employee2)
                .ifPresent(System.out::println);
        System.out.println("3. -----------------------------------------------");
        departments.stream()
                .flatMap(department -> department.getEmployeeList().stream())
                .reduce((employee1, employee2) -> employee1.getAge() > employee2.getAge() ? employee1 : employee2)
                .ifPresent(System.out::println);
        System.out.println("4. All the employees grouped by age ----------------->");
       Map <Integer, List<Employee>> groupByAge = departments.stream()
                .flatMap(department -> department.getEmployeeList().stream())
                .collect(Collectors.groupingBy(Employee::getAge)); // or
//                .collect(Collectors.groupingBy(employee ->  employee.getAge()));
        System.out.println(groupByAge);

/* --------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------  */
        List<String> bingoNumbers = Arrays.asList("White12", "Blue10", "Green23", "Red56", "Grey23", "golden67");
        List<String> gNumbers = new ArrayList<>();
        bingoNumbers.forEach(currentNumber -> {
            if(currentNumber.toLowerCase().startsWith("g")){
                gNumbers.add(currentNumber);
                System.out.println("5a. " + currentNumber);
            }
        });
       System.out.println("5b ----------------------------------------");
//       gNumbers.forEach((String str) -> System.out.println(str));
        System.out.println("5c ----------------------------------------");
        gNumbers.forEach(System.out::println);
        System.out.println("5d ----------------------------------------");
        gNumbers.sort(Comparator.naturalOrder());
        gNumbers.forEach(System.out::println);

        System.out.println("5e ----- with stream ------");
        bingoNumbers.stream()
                .map(String::toLowerCase)
                .filter(str -> str.startsWith("g"))
                .sorted()
                .forEach(System.out::println);
        System.out.println("5f ----- with stream, peek  ------");
        bingoNumbers.stream()
                .map(String::toLowerCase)
                .filter(str -> str.startsWith("g"))
                .peek(System.out::println)      // It shows the elements in my collection, in the order they're written.
                .sorted()
                .forEach(System.out::println); // It shows ordered.
        System.out.println("5g ----- with stream ------");
        List <String> listOfGNumbers = bingoNumbers.stream()
                .map(String::toLowerCase)
                .filter(str -> str.startsWith("g"))
                .sorted()
                .collect(Collectors.toList());
        for (String currentNumber: bingoNumbers ) {
            System.out.println("5.1.g - Print all the items ---" + currentNumber);}
        System.out.println("5.2.g --- It prints elements that begin with g ---> " + listOfGNumbers);
    }
}