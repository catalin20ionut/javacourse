package C28___08_11_2022___JavaStreams;

import java.util.ArrayList;
import java.util.List;

public class Department {
    private List<Employee>  employeeList = new ArrayList<>();
    private String name;

    public Department(String name) {
        this.name = name;
    }
    public List<Employee> getEmployeeList() {
        return employeeList;
    }
        public void addEmployee (Employee employee) {
        employeeList.add(employee);
    }
}
