package C08___21_07_2022;

/**
2. Find the missing number from array. Assume that the elements in array are in consecutive numbers and integers.
1, 2, 3, 4, 5 -- toate numele
1, 2, 4, 5 --> lipsește 3
 */
public class Exercise_3___Missing_Number {
    public  static  int displayMissedNumber (int [] anyArray){
        int sum = 0;
        for (int number : anyArray) {
            sum += number;
        }
        System.out.println("1. sum = " + sum);                                            // the sum of the given array

        int sum1= 0;
        for (int i = anyArray[0]; i <= anyArray[anyArray.length -1]; i += minimumDifference(anyArray)) {
//        for (int i = anyArray[0]; i <= anyArray[anyArray.length -1]; i ++) {                          // for myArray1
//        for (int i = anyArray[0]; i <= anyArray[anyArray.length -1]; i +=2) {                        //  for myArray2
//        for (int i = anyArray[0]; i <= anyArray[anyArray.length -1]; i += anyArray[1]-anyArray[0]) {// for myArray1,2
            sum1 += i;}
        System.out.println("2. sum1 = " + sum1);
        return (sum1 - sum);                                          // the sum of an array including the missing item
    }

    // it is used to find out the size of the step for i
    static int minimumDifference(int[] anyArray)    {
        int minimumDifference = anyArray[1] - anyArray[0];
        int i, j;
        for (i = 0; i < anyArray.length; i++) {
            for (j = i + 1; j < anyArray.length; j++) {
                if (anyArray[j] - anyArray[i] <  minimumDifference) {
                    minimumDifference = anyArray[j] - anyArray[i];}
            }
        }
        return minimumDifference;
    }

    public static void main(String[] args) {
        int[] myArray1 = {-1, 0, 2, 3, 4};
        int[] myArray2 = {-4, -2, 0, 4, 6};
        int[] myArray3 = {-4, 0, 2, 4, 6};
        int[] myArray4 = {0, 1, 2};

        System.out.println("The missing number for myArray1: " + displayMissedNumber(myArray1) + "\n");
        System.out.println("The missing number for myArray2: " + displayMissedNumber(myArray2) + "\n");
        System.out.println("The missing number for myArray3: " + displayMissedNumber(myArray3));
        System.out.println("The missing number for myArray4: " + displayMissedNumber(myArray4));
    }
}