package C08___21_07_2022;

/**  Determine if a number is an Armstrong number.
An Armstrong number is a number that is the sum of its own digits each raised to the power of the number of digits.
For example: 9 is an Armstrong number, because 9 = 9^1 = 9
            10 is not an Armstrong number, because 10 != 1^2 + 0^2 = 1
           153 is an Armstrong number, because: 153 = 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153
           154 is not an Armstrong number, because: 154 != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190
Now write some code to determine whether a number is an Armstrong number.                                            */
public class Exercise_2___Armstrong_Number {
    public static boolean numberArmstrong (int no){
        String noToString = no + "";
        int noDigits = noToString.length();
        int sum = 0;
        for (int i = 0; i < noDigits; i++){
            sum +=  Math.pow(Character.getNumericValue(noToString.charAt(i)), noDigits);}
        if (sum == no) {
            return true; }
        else {
            return false;}
    }

    public static void main(String[] args) {
        System.out.println(numberArmstrong(153));
    }
}