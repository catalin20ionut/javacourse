package C06___14_07_2022;

/** 2. Given a number n, write a method that sums all multiples of 3 or 5 up to n inclusive.
--> It finds all the numbers that are divided to 3 or 5 then sums them.                                              */
public class S6___SumMultiples {
    public static void calculateSumOfTheMultiples(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++){
            if (i % 3 == 0 || i % 5 == 0){
                sum += i;
                System.out.println("The multiples are: " + i);}
        }
        System.out.println("The sum of the multiples is " + sum +".");
    }

    public static void main(String[] args) {
        calculateSumOfTheMultiples(15);
    }
}