package C06___14_07_2022;

/**  Write a program to sum the following series:
1 / 3 + 3 / 5 + 5 / 7 + 7 / 9 + 9 / 11 + 11 / 13 + ... + 95 / 97 + 97 / 99                                           */

public class S5___SumWithIntegers {
    public static void calculateSumOfTheFractions() {
        double suma = 0.0;
        for (double i = 1; i <= 3; i += 2) {
            suma += i / (i+2);}
            System.out.println(suma);
    }

    public static void main(String[] args) {
        calculateSumOfTheFractions();
    }
}