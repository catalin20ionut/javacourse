package C06___14_07_2022;

/** Write a program to compute the following summation.
1 / (1 + √2) + 1 / (√2 + √3) + 1 / (√3 + √4) + ... +  1 / (√624 + √625)                                              */
public class S7___SumWithRadicals {
    public static void calculateSumWithRadicals(){
        double  suma = 0.0;
        for (double i = 1; i <= 624; i++){
            suma = suma + 1/(Math.sqrt(i) + Math.sqrt(i+1));}
        System.out.println(suma);
    }

    public static void main(String[] args) {
        calculateSumWithRadicals();
    }
}