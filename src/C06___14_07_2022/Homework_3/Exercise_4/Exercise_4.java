package C06___14_07_2022.Homework_3.Exercise_4;
/** 4. Find the difference between the square of the sum and the sum of the squares of the first N natural numbers. */

public class Exercise_4 {
    public static int difference(int n){
        int sumSquareN = (n * (n + 1) * (2 * n + 1)) / 6;                        // sum of squares of n natural numbers
        int sumN = (n * (n + 1)) / 2;                                           //             sum of n natural numbers
        int squareSumN = sumN * sumN;                                          //    square of sum of n natural numbers
        return Math.abs(sumSquareN - squareSumN);                             //                         the difference
    }
    public static void main(String args[]){
        int n = 3;
        System.out.println("Number: " + n);
        System.out.println("Difference: " + difference(n));
        System.out.println("Number: " + n + " \nDifference: " +  difference(n));
        System.out.printf("Number: %s%nDifference: %s  ", n, difference(n));
    }
}
