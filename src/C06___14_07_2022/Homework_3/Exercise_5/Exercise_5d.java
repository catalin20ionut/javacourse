package C06___14_07_2022.Homework_3.Exercise_5;

public class Exercise_5d {
    public static int findChar2(String str) {
        for(int i=0; i<str.length(); i++) {
            String iChar = Character.toString(str.charAt(i));
            if(str.substring(str.indexOf(iChar) + 1).contains(iChar)) {
                str = str.replace(iChar, "*");
                System.out.println(str);
            }
        }
        for(int j=0; j<str.length(); j++) {
            String jChar = Character.toString(str.charAt(j));
            if(!jChar.equals("*")) {
                return str.indexOf(jChar);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(findChar2("java"));
    }
}
