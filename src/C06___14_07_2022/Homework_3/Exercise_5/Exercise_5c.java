package C06___14_07_2022.Homework_3.Exercise_5;

public class Exercise_5c {
    public static int findChar(String str) {
        for(int i = 0; i < str.length(); i++) {
            String iChar = "" + str.charAt(i);
//            System.out.println(iChar);
            String frontAndBack = str.substring(0, str.indexOf(iChar))
                                + str.substring(str.indexOf(iChar) + 1);              // str without the iChar
//            System.out.println(frontAndBack);
            if(!frontAndBack.contains(iChar)) {
                return str.indexOf(iChar);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(findChar("java"));
    }
}
