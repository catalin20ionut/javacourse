package C06___14_07_2022.Homework_3.Exercise_5;

import java.util.*;
/**
5. Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.
Example:
s = “jaatcoda”                            print: am returnat indexul 0 (in acest caz j e prima litera care nu se repeta)
s = "okejavacode",                        print: am returnat indexul 1 (in acest caz k e prima litera care nu se repeta)
*/
public class Exercise_5b {
    public static int first_unique_character(String str1) {
        HashMap<Character, Integer> map = new HashMap<>();
        char chr;
        for (int i = 0; i < str1.length(); i++) {
            chr = str1.charAt(i);
            map.put(chr, map.containsKey(chr) ? map.get(chr) + 1 : 1);}

        for (int i = 0; i < str1.length(); i++) {
            if (map.get(str1.charAt(i)) < 2)
                return i; }
        return -1;}

    public static void main(String[] args) {
        String str1 = "java code";
        int index = first_unique_character(str1);
        System.out.println(
                index == -1
                        ? "-1 or \"Either all characters are repeating or string is empty.\""
                        : "Index of first non-repeating character that is " + str1.charAt(first_unique_character(str1))
                                                + " in the word \"" + str1 + "\" is: " + first_unique_character(str1));

/**
        // or
        if (index == -1)
            System.out.println("-1 or \"Either all characters are repeating or string is empty.\"");
        else{
            System.out.println("Index of first non-repeating character that is "
                                                                            + str1.charAt(first_unique_character(str1))
                                               + " in the word \"" + str1 + "\" is: " + first_unique_character(str1));}

        // or without curly brackets (sau fără acolade)
        if (index == -1)
            System.out.println("-1 or \"Either all characters are repeating or string is empty.\"");
        else
            System.out.println("Index of first non-repeating character that is "
                                                  + str1.charAt(first_unique_character(str1))+ " in the word \"" + str1
                                                                           + "\" is: " + first_unique_character(str1));
*/
    }
}
