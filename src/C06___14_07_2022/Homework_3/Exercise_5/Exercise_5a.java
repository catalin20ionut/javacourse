package C06___14_07_2022.Homework_3.Exercise_5;

/**
5. Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.
Example:
s = “jaatcoda”
print: am returnat indexul 0 (in acest caz j e prima litera care nu se repeta)
s = "ovejavacode",
print: am returnat indexul 1 (in acest caz v e prima litera care nu se repeta)
*/
public class Exercise_5a {
    static final int numbersOfCharacters = 256;
    static char[] count = new char[numbersOfCharacters];

    // This calculates count of characters in the string
    static void getCharCountArray(String str) {
        for (int i = 0; i < str.length(); i++)
            count[str.charAt(i)]++;
    }
    /* The method returns index of first non-repeating character in a string.
    If all characters are repeating or the string is empty then it returns -1. */
    static int firstNonRepeating(String str)  {
        getCharCountArray(str);
        int i;
        int index = -1;

        for (i = 0; i < str.length(); i++) {
            if (count[str.charAt(i)] == 1) {
                index = i;
                break;}
        }
        return index;
    }

    public static void main(String[] args) {
        String str = "java";
        int index = firstNonRepeating(str);
        System.out.println(
                index == -1 ?
                           "-1 or \"Either all characters are repeating or string is empty.\""
                        : "Index of first non-repeating character that is " + str.charAt(index) + " in the word \""
                                                                                             + str + "\" is: " + index);
        // For explanations see exercise_5b! (? and :)
    }
}