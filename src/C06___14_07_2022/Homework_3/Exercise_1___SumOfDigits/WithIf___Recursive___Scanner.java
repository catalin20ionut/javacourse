package C06___14_07_2022.Homework_3.Exercise_1___SumOfDigits;

import java.util.Scanner;

public class WithIf___Recursive___Scanner {
    public static long sumDigits(long n){
        if (n == 0)
            return n;
        else
            return ((n % 10) + sumDigits(n / 10));
    }

    public static void main(String[] args) {
        long n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        n = sc.nextLong();

        System.out.println("The sum of the digits for the chosen number is " + sumDigits(n) + ".");
    }
}
