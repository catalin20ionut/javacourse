package C06___14_07_2022.Homework_3.Exercise_1___SumOfDigits;

public class WithIf___Recursive {
    public static long sumDigits(long n){
        if (n == 0){
           return n;}
        else {
            return ((n % 10) + sumDigits(n / 10));}
    }

    public static void main(String[] args) {
        long result = sumDigits(1234);
        System.out.println("The sum of the digits for the chosen number is " + result + ".");
    }
}