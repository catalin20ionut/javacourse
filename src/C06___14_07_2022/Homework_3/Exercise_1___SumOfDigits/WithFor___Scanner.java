package C06___14_07_2022.Homework_3.Exercise_1___SumOfDigits;

import java.util.Scanner;

public class WithFor___Scanner {
    public static void main(String[] args) {
        long n, sum;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        n = sc.nextLong();

        for (sum = 0; n != 0; n = n / 10) {
            sum = sum + n % 10;}
        System.out.println("Sum of digits for the chosen number is " + sum + ".");
    }
}