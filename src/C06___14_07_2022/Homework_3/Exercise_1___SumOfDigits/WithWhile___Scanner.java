package C06___14_07_2022.Homework_3.Exercise_1___SumOfDigits;

import java.util.Scanner;
public class WithWhile___Scanner{
    public static void main(String[] args) {
        int n, sum = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number: ");
        n = sc.nextInt();

        while(n != 0){
            sum += n % 10;
            n /= 10;}
        System.out.println("The sum of the digits for the chosen number is " + sum + ".");
    }
}
