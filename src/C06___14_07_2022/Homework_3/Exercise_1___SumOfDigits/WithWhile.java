package C06___14_07_2022.Homework_3.Exercise_1___SumOfDigits;

/**
1. Write a method that computes the sum of the digits in an integer.
Use the following method header: public static int sumDigits(long n).
For example, sumDigits(234) returns 9 (2 + 3 + 4). (Hint: Use the % operator to extract digits, and the / operator to
remove the extracted digit. For instance, to extract 4 from 234, use 234 % 10 (= 4). To remove 4 from 234, use 234 / 10
(= 23). Use a loop to repeatedly extract and remove the digit until all the digits are extracted.
*/

public class WithWhile {
    public static void sumDigits(long n){
        long sum = 0, digit;
        while (n != 0){
            digit = n % 10;                                           // This finds the last digit of the given number.
            sum += digit;                                            //              It adds the last digit to the sum.
            n /= 10;}                                               //     This removes the last digit from the number.
                System.out.println("Sum of digits for the chosen number is " + sum + ".");
    }

    public static void main(String[] args) {
        sumDigits(12345);
    }
}
