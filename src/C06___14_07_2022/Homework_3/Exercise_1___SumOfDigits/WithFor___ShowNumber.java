package C06___14_07_2022.Homework_3.Exercise_1___SumOfDigits;

/**
1. Write a method that computes the sum of the digits in an integer.
Use the following method header: public static int sumDigits(long n).
For example, sumDigits(234) returns 9 (2 + 3 + 4). (Hint: Use the % operator to extract digits, and the / operator to
remove the extracted digit. For instance, to extract 4 from 234, use 234 % 10 (= 4). To remove 4 from 234, use 234 / 10
(= 23). Use a loop to repeatedly extract and remove the digit until all the digits are extracted.
*/

public class WithFor___ShowNumber {
    public static void sumDigits(long n) {
        long sum;
        long x = n;      // I declared this variable before the for loop, because it shows the n from "sumDigits(1235);"
        for (sum = 0; n != 0; n /= 10) {
            sum += n % 10;}
        System.out.println("Sum of digits for the number " + x + " is " + sum + ".");
    }

    public static void main(String[] args) {
        sumDigits(1235);
    }
}
