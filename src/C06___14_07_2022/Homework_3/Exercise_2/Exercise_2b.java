package C06___14_07_2022.Homework_3.Exercise_2;

/**
2. Write a program that displays all the numbers from 100 to 1,000, ten per line, that are divisible by 5 and 6. Numbers
are separated by exactly one space.
 */

public class Exercise_2b {
    static void result(int N) {
        for (int num = 100; num < N; num++) {
            if (num % 5 == 0 && num % 6 == 0){
//            System.out.print(num + "\n");}                  //       This prints all the numbers one below the other.
            System.out.print(num + " ");}                    // It doesn't work with --- System.out.print(num + "\n");}
        }
    }
    public static void main(String[] args) { result(1000); }
}
