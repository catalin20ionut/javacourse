package C06___14_07_2022.Homework_3.Exercise_2;

/**
2. Write a program that displays all the numbers from 100 to 1,000, ten per line, that are divisible by 5 and 6. Numbers
are separated by exactly one space.
 */

public class Exercise_2a {
    public static void main(String[] args) {
        int count = 0;
        for (int  numbers = 100;  numbers <= 1000;  numbers++) {
            if (numbers % 5 == 0 && numbers % 6 == 0) {
                count++;
                if (count % 10 == 0){
                    System.out.println(numbers);}
                else {
                    System.out.print(numbers + " ");}
            }
        }
    }
}