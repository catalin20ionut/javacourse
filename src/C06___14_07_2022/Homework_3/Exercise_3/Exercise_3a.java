package C06___14_07_2022.Homework_3.Exercise_3;

/**
3. Write a program that allows to enter two strings and reports whether the second string is a substring of the first
 string.*
 */
public class Exercise_3a {
    public static void stringComparator (String s1, String s2){
        if (s1.contains(s2)) {
            System.out.printf("The string " + s2 + " is a substring of " + s1 + ".\n");}
        else {
            System.out.printf("The string " + s2 + " is not a substring of " + s1 + ".\n");}
    }
    public static void main(String[] args) {
        stringComparator("AutoCar", "utoC");
        stringComparator("AutoCar", "utoR");
    }
}
