package C06___14_07_2022.Homework_3.Exercise_3;

import java.util.Scanner;

/**
3. Write a program that allows to enter two strings and reports whether the second string is a substring of the first
 string.*
 */
public class Exercise_3c___Input {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the first string: ");
        String s1 = input.nextLine();

        System.out.print("Enter the second string: ");
        String s2 = input.nextLine();                                                  // sau String s2 = input.next();

        if (s1.contains(s2)) {
            System.out.printf("%s is a substring of %s.%n", s2, s1);}
            // primul %s e pentru s2; al doilea %s e pentru s1; %n afișează tot ceea ce este după el într-un nou rând
        else{
            System.out.printf("%s is not a substring of %s.%n", s2, s1);}
    }
}
