package C06___14_07_2022.Homework_3.Exercise_6;

/**
6. Write a program with the following requests:
- Create a java class called Circle
- Create a variable of type double in Circle class, name the variable as radius
- Create one constructor in Circle without parameters, but the body of this constructor will assign value 2 for radius
variable
- Create second constructor in Circle with radius as parameter
- Create a method that will calculate and print the area of the Circle (please search for the formula in order to
calculate the area)
- Create another class TestCircle with the main method
- In the main method you will need to have 3 Circle objects:
- one object is created using the constructor without parameter
- two objects are created using the constructor with parameter
- for every object you will need to calculate the area and print in console the value of the area
 */
public class Circle {
    double radius;
    public Circle(){
        radius = 2;
    }
    public Circle(double radius){
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public double getArea() {
        return radius * radius * Math.PI;
    }
}