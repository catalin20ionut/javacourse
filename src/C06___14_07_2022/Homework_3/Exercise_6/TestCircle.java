package C06___14_07_2022.Homework_3.Exercise_6;


public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("The circle with the radius of " + c1.getRadius() + " has the area of "+ c1.getArea() + ".");

        Circle c2 = new Circle(2.0);
        System.out.println("The circle with the radius of " + c2.getRadius() + " has the area of "+ c2.getArea() + ".");

        Circle c3 = new Circle(9.0);
        System.out.printf("The circle with the radius of %s has the area of %s.", c3.getRadius(), c3.getArea());
    }
}