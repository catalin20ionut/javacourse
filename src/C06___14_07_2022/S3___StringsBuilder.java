package C06___14_07_2022;

public class S3___StringsBuilder {
    public static void main(String[] args) {
        String s = "";
        s += "Java";
        s += " este";
        s += " limbajul";
        s += " meu";
        s += " preferat.";
        System.out.println("1. Stringul este " + s);                                     // mai multe căsuțe de memorie

        StringBuilder mySB = new StringBuilder("");
        mySB.append("Java");
        mySB.append(" este");
        mySB.append(" limbajul");
        mySB.append(" meu");
        mySB.append(" preferat.");
        System.out.println("2. StringBuilder-ul -> " + mySB);                   // se ține într-o singură căsuță de memorie

        StringBuilder mySB2 = new StringBuilder("");
        mySB2.append("Java").append(" este").append(" limbajul").append(" meu").append(" preferat.");
        System.out.println(mySB2);
    }
}