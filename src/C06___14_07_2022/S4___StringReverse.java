package C06___14_07_2022;
/** Write a method will receive a string as parameter and displays the string in reverse order. Use a loop. */

public class S4___StringReverse {
    public static void reverseString (String name){
        String reverseName = "";
        for(int i = name.length()-1; i >= 0; i--){
            reverseName += name.charAt(i);}
        System.out.println("Reverse name is: " + reverseName);
    }

    public static void main(String[] args) {
        reverseString("Andrei");
    }
}