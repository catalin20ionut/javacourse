package C06___14_07_2022;

public class S2___Loops {
    public static void main(String[] args) {
        for(int index = 0;  index <= 10; index++){
            System.out.println("1. Indexul este: " + index);
            System.out.println("2. Indexul este: " + index);}

        for(int index = 10;  index > 5; index--){
            System.out.println("3. Indexul este: " + index);}

//        while (true){
//            System.out.println("while loop  infinite");}

        int counter = 0;
        boolean flag = true;
        while(flag) {
            counter ++;
            System.out.println("4. counter value " + counter);
            flag = false;}

        int i= 1;
        while (i <= 10){
            System.out.println("5. i este " + i);
            i++;}

        int j = 2;
        while (j <=10){
            System.out.println("6. j este " + j);
            j+=2;}

        int x = 1;
        while (x <=10){
            System.out.println("7. x este " + x);
            x+=2;}

        int y = 10;
        while (y>0){
            System.out.println("8. invers " + y);
            y--;}

        int k = 15;
        do {
            System.out.println("9. Print nr " +  k);
            k++;}
        while(k <= 10);


        for(int p = 0; p <= 10; p++){
            if (p == 7){
                continue;}
            else {
                System.out.println("10. continue: " + p);
            }
        }
    }
}