package C06___14_07_2022;

public class S1___StringExample {
    public static void main(String[] args) {
        String s = "Welcome";
        int len = s.length();
        System.out.println(len);

        String s1 = "test";
        String s2 = "java";
        System.out.println(s1 + s2);
        System.out.println(s1 + " " + s2);
        System.out.println("test" + "java");
        System.out.println(s1.concat(s2));
        System.out.println("test".concat("java"));

        System.out.println("2. trim() ---->");
        String s3 = "              java            ";
        String s4 = "              cafes            ";
        String s5 = " mar         afina";
        System.out.println(s3.trim()); // string buffer, string builder - se modifica stringul
        System.out.println(s4.trim());
        System.out.println(s5.trim());
        System.out.println("-----------------");

        String s6 = "marinela ";
        System.out.println(s6.charAt(3));
        System.out.println(s6.contains("ri"));
        System.out.println(s6.contains("aa"));
        System.out.println(s6.equals("Marinela"));
        System.out.println(s6.equalsIgnoreCase("Marinela "));
        System.out.println("trim" + s6.trim().equalsIgnoreCase("Marinela"));

        System.out.println(s6.replace("a", "あ"));
        String s7 = "abbblhtbbbbennns";
        System.out.println(s7.replaceAll("bbb", "ぶぶぶ"));

        System.out.println(s6.substring(2,5));
        System.out.println(s6.toLowerCase());
        System.out.println(s6.toUpperCase());
        System.out.println(s6.indexOf("a"));
        System.out.println(s6.lastIndexOf("a"));

        String s8 = "mArIA";
        System.out.println(s8.substring(0,1).toUpperCase() + s8.substring(1).toLowerCase());
    }
}