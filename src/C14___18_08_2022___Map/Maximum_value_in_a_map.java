package C14___18_08_2022___Map;

import java.util.HashMap;
import java.util.Map;
// finding the maximum value in a map
public class Maximum_value_in_a_map {
    public static void displayingMaximumValueInAMap (Map <Character, Integer> anyMap){
        Map.Entry<Character, Integer> maxEntry = null;
        for (Map.Entry<Character, Integer> entry : anyMap.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;}
        }
        System.out.println(maxEntry);
    }

    public static void main(String[] args) {
        Map<Character, Integer> myMap = new HashMap<>();
        myMap.put('A', 1);
        myMap.put('B', 2);
        myMap.put('C', 3);
        myMap.put('D', 4);

        displayingMaximumValueInAMap(myMap);
    }
}
