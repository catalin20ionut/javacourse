package C14___18_08_2022___Map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExample {
    public static void main(String[] args) {
        Map<String, String> hashMap = new HashMap<>();                                               // declaring a map
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        Map<String, String> treeMap = new TreeMap<>();

        System.out.println("We test hashMap");
        testMapType(hashMap);
        System.out.println();
        System.out.println("We test LinkedHashMap");
        testMapType(linkedHashMap);
        System.out.println();
        System.out.println("We test treeMap");
        testMapType(treeMap);
    }

    public static void testMapType (Map<String, String> map){
        map.put("dog name", "Rex");
        map.put("dog name1", "Brownie");
        map.put("dog name2", "Blackie");
        map.put("dog name3", "Sarah");
        map.put("dog name4", "Bella");

        map.put("age", "2");
        map.put("age1", "4");
        map.put("age2", "6");
        map.put("age3", "9");
        map.put("age4", "10");


        for (String key: map.keySet()) {
            String currentValue = map.get(key);
            System.out.println("1.The key is: " + key + " the value is: " + currentValue);
        }

        for(Map.Entry<String, String> elem: map.entrySet()){
            System.out.println("2.The key is: " + elem.getKey());
            System.out.println("3.The value is:" + elem.getValue());
        }

        map.replace("dog name", "Rex", "RexJunior");
        map.remove("age");
        System.out.println("••• Print after delete and replace ");
        for (String key: map.keySet()) {
            String currentValue = map.get(key);
            System.out.println("4.The key is: " + key + " the value is:" + currentValue);
        }
        System.out.println("5.Number of elements: " + map.size());
        System.out.println("6.Is it the map empty? -" + map.isEmpty());
        System.out.println("7. Verify if it contains.");
        System.out.println(map.containsKey("age3"));
        System.out.println(map.containsKey("9"));
        System.out.println(map.containsKey("19"));
        System.out.println(map.containsValue("10"));
        System.out.println("map -> " + map);

        System.out.println("8a. The value for the key 0:  " + map.get("age1"));
        System.out.println("8a. The value for the key 0:  " + map.get("age2"));
        System.out.println("8a. The value for the key 0:  " + map.get("age3"));
        System.out.println("8a. The value for the key 0: " + map.get("age4"));
        System.out.println("8b. The value for the key 0:  " + map.getOrDefault("age1", ""));
        System.out.println("8b. The value for the key 1:  " + map.getOrDefault("age2", " "));
        System.out.println("8b. The value for the key 2:  " + map.getOrDefault("age3", "   "));
        System.out.println("8b. The value for the key 3: " + map.getOrDefault("age4", "unknown"));

        for (String key: map.keySet()){
            if (map.get(key).equals("4")) {
                System.out.println("9. The key for the value 4: " + key);}
        }
    }
}
