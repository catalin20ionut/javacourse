package C05___12_07_2022;

/**
3. Write a program that will receive a three-digit integer and determines whether it is a palindrome number or not. A
number is palindrome if it reads the same from right to left and from left to right.
 */

public class Exercise_3 {
    public static void getNumber(int number){
        int firstDigit = number / 100;
        int lastDigit = number % 10;

        if (firstDigit == lastDigit) {
            System.out.println("The number is a palindrom.");}
        else {
            System.out.println(("The number is not a palindrom."));}
    }

    public static void main(String[] args) {
        getNumber(157);
    }
}
