package C05___12_07_2022;

/**
1. You would like to write a program to compare the cost. The program will receive the weight and price of each package
and displays the one with the better price.
 */
public class Exercise_1 {
    public void compareCost(double weight1, double price1, double weight2, double price2){
        double cost1 = price1 / weight1;
        double cost2 = price2 / weight2;

        if (cost1 > cost2) {
            System.out.println("The price for the second package is better.");}
        else if (cost1 < cost2){
            System.out.println("The price for the first package is better.");}
        else {
            System.out.println("The two packages have the same price.");}}

    public static void main(String[] args) {
        Exercise_1 pachet1= new Exercise_1();
        pachet1.compareCost(25.1, 12.3, 25.4, 8.9);

//        compareCost(25.1, 12.3, 25.4, 8.9);
        // Ca să funcționeze trebuie să avem "public static void compareCost()"
        // Static poate fi apelat înainte ca obiectul de clasă să fie apelat, fără instanță.
    }
}
