package C05___12_07_2022;

public class StaticExample {
    static int a = 10;
    int b = 10;

    static void met1(){
        System.out.println("Methode 1 is static.");
    }

    void met2(){
        System.out.println("Methode 2 is not static.");
    }

    void met3(){
        System.out.println(a); // static
        System.out.println(b);
        met1();
        met2();
    }

    public static void main(String[] args) {
        System.out.println("1. >>> " + a);
        met1();
//        System.out.println(b); // trebuie int b sa fie static
        StaticExample st1 = new StaticExample();
        System.out.println("3. >>> " + st1.b);
        st1.met1(); // It works, but met1() is static. It should be met1();
        st1.met2();
        st1.met3();
    }
}
