package C05___12_07_2022;

/**
5. Write a program that reads investment amount, annual interest rate, and number of years, and displays the future
investment value using the following formula:
futureInvestmentValue = investmentAmount * (1 + monthlyInterestRate)^numberOfYears * 12
For example, if you enter amount 1000, annual interest rate 3.25%, and number of years 1, the future investment value is
1032.98.
*/

public class Exercise_5 {
    public static void calculateRate (int investmentAmount, double rate, int years){
        double monthlyInterestRate = rate / 1200;
        double futureInvestmentValue = investmentAmount * Math.pow((1 + monthlyInterestRate), (years * 12));
        System.out.println("The future investment is " + futureInvestmentValue + ".");
    }

    public static void main(String[] args) {
        calculateRate(1000, 3.25, 1);
    }
}
