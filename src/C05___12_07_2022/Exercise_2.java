package C05___12_07_2022;

/**
2. A shipping company uses the following function to calculate the cost (in dollars) of shipping based on the weight of
the package (in pounds).
 3.5, if 0 < w < = 1
 5.5, if 1 < w < = 3
 8.5, if 3 < w < = 10
 10.5, if 10 < w < = 20
Write a program that will receive the weight of the package and display the shipping cost. If the weight is greater than
20, display a message “the package cannot be shipped.”
 */

public class Exercise_2 {
    public static void verifyWeightAndPrintTheCost(double weight) {
        if (weight > 20) {
            System.out.println("The package cannot be shipped.");}
        else if (weight <= 20) {
//        else {
            double costPerPound;
            if (weight <= 1 && weight > 0) {
                costPerPound = 3.5;}
            else if (weight <= 3) {
                costPerPound = 5.5;}
            else if (weight <= 10) {
                costPerPound = 8.5;}
            else {
                costPerPound = 8.5;}
            System.out.println("The total cost is: " + costPerPound * weight);
        }
    }

    public static void main(String[] args) {
        verifyWeightAndPrintTheCost(1);
        verifyWeightAndPrintTheCost(3);
        verifyWeightAndPrintTheCost(10);
        verifyWeightAndPrintTheCost(20);
        verifyWeightAndPrintTheCost(20.01);
    }
}

