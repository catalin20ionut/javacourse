package C05___12_07_2022;

/**
7. Write a program that prompts the user to enter an integer for today’s day of the week (Sunday is 0, Monday is 1, ...
and Saturday is 6). Also prompt the user to enter the number of days after today for a future day and display the future
day of the week.
*/

public class Exercise_7 {
    public static void calculateTheDay (int a) {
        switch (a){
            case 0:
                System.out.println("Sunday");
                break;
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            default:
                System.out.println("Wrong");
                break;
        }
    }

    public static void main(String[] args) {
        calculateTheDay(4);
        int day = (int) ((Math.random() * 7) + 1);
        System.out.println(day);
        calculateTheDay(day);
    }
}

