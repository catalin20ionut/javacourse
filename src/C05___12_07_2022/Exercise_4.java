package C05___12_07_2022;

// Program to randomly generate an addition question with two integers less than 100. Print the question and the answer.

public class Exercise_4 {
    public static void calculateSum(int a, int b, int sum) {
        int realSum = a + b;
        if (a < 100 && b < 100){
            if (realSum == sum) {
                System.out.println("You assume the right sum.");}
            else {
                System.out.println("The real sum of " + a + " and " + b + " is " + realSum + ".");}}
        else  {
            System.out.println("The numbers are bigger than 100.");
        }
    }

    public static void main(String[] args) {
        calculateSum(110, -20, -100);
        calculateSum(70, -20, 40);
        calculateSum(70, 20, 90);
    }
}