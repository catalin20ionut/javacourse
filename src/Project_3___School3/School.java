package Project_3___School3;


import java.util.ArrayList;
import java.util.List;

public class School {
    private final String schoolName;
    private List<User> schoolMembers = new ArrayList<>();
    private List<Classes> grades = new ArrayList<>(); // unknown+
    private List<TeachingSubjects> teachingSubjects = new ArrayList<>();  // unknown+

    public School(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public List<User> getSchoolMembers() {
        return schoolMembers;
    }

    public void setSchoolMembers(List<User> schoolMembers) {
        this.schoolMembers = schoolMembers;
    }

    public List<Classes> getGrades() {
        return grades;
    }

    public void setGrades(List<Classes> grades) {
        this.grades = grades;
    }

    public List<TeachingSubjects> getTeachingSubjects() {
        return teachingSubjects;
    }

    public void setTeachingSubjects(List<TeachingSubjects> teachingSubjects) {
        this.teachingSubjects = teachingSubjects;
    }

    public void addUser(User userName) {
        if (schoolMembers.contains(userName)) {
            throw new RuntimeException("The user has already been registered.");
        } else {
            System.out.println("The user " + userName.getUserName() + " is added to the school list.");
            schoolMembers.add(userName);
        }
    }

    public int getUserNo() {
        return this.schoolMembers.size();
    }


}
