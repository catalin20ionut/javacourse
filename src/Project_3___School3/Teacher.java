package Project_3___School3;


import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Teacher extends User {
    private Admin admin;
    private int numberOfHours;
    private double salary;
    private int marks;
    private LocalDate dateOfEmployment;
    private String phoneNumber;
    private TeacherType teacherType;
    private List<Classes> classesTeachingList = new ArrayList<>();
    private Disciplines disciplines;

    public Teacher(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
    }

    @Override
    public String getClassName() {
        return "Teacher";
    }

    @Override
    protected boolean checkPassword(User user) {
        if (getPassword() != null) {
            return true;
        }
        return false;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public int getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(int numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public List<Classes> getClassesTeachingList() {
        return classesTeachingList;
    }

    public void setClassesTeachingList(List<Classes> classesTeachingList) {
        this.classesTeachingList = classesTeachingList;
    }

    public Disciplines getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(Disciplines disciplines) {
        this.disciplines = disciplines;
    }

    public TeacherType getTeacherType() {
        return teacherType;
    }

    public void setTeacherType(TeacherType teacherType) {
        this.teacherType = teacherType;
    }

    public List<Classes> getClassesList() {
        return classesTeachingList;
    }

    public void setClassesList(List<Classes> classesList) {
        this.classesTeachingList = classesList;
    }


    public List<Classes> getEnumGradesList() {
        return classesTeachingList;
    }

    public void setEnumGradesList(List<Classes> classesList) {
        this.classesTeachingList = classesList;
    }

    public int getNumberTotalOfHours() {
        return numberOfHours;
    }

    public void setNumberTotalOfHours(int numberTotalOfHours) {
        this.numberOfHours = numberTotalOfHours;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public LocalDate getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(LocalDate dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void addClasses(Classes classes) {
        this.classesTeachingList.add(classes);
    }


    public void addMarks(Integer marks, Pupil pupil, Teacher teacher, Disciplines discipline) {
        if(this.getPassword()==null){
            throw new RuntimeException("PASSWORD IS NULL.");
        }else {
            for (Disciplines currentDiscipline : pupil.getDisciplinesList()) {
                if (teacher.getDisciplines().equals(discipline)) {
                    currentDiscipline.getMarksList().add(marks);
                    System.out.println("Grade is : " + marks);
                } else {
                    throw new RuntimeException("Error please generate password!!!");
                }
            }
        }

    }

    public void generateStudentFiles(School school, Classes classes) {
        StringBuilder studentName = new StringBuilder();
        int counter = 0;
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWORD IS NULL.");
        }else{
            for (User user1 : school.getSchoolMembers()) {
                if (user1 instanceof Pupil) {
                    Pupil pupilStudent = (Pupil) user1;
                    if (pupilStudent.getClasses().equals(classes)) {
                        counter++;
                        studentName.append("Nr. ").append(counter).append(" ").append(pupilStudent.getFullName()).append("\n");
                    }
                }
            }
        }
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School3\\TeacherFile\\StudentList.txt");
            file.createNewFile();
            FileWriter pw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(pw);
            bf.write(studentName.toString());
            bf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void readHomework(Pupil pupil, Teacher teacher) {

    }
}
