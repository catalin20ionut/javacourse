package Project_3___School3;

public enum TeacherType {
    PRIMARY_SCHOOL("Clasa 0--Clasa 4"),
    MIDDLE_SCHOOL("Clasa 5--Clasa8"),
    HIGH_SCHOOL("Clasa 9--Clasa 12");
    private String description;

    TeacherType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

