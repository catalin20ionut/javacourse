package Project_3___School3;

public enum TeachingSubjects {
    ROMANIAN_LANGUAGE_AND_LITERATURE("Limba și literatura română"),
    MATHEMATICS("Matematică"),
    PHYSICS("Fizică"),
    CHEMESTRY("Chimie"),
    BIOLOGY("Biologie"),
    GEOGRAPHY("Geografie"),
    HISTORY("Istorie"),
    ENGLISH_LANGUAGE("Limba engleză - The English Language"),
    GERMAN_LANGUAGE("Limba germană - Die Deutsche Sprache"),
    SPANISH_LANGUAGE("Limba spaniolă - El lenguaje español"),
    FRENCH_LANGUAGE("Limba franceză - La langue française"),
    RELIGION ("Religie"),
    PHYSICAL_EDUCATION_AND_SPORT("Educație Fizică și Sport"),
    MUSICAL_EDUCATION("Educație Muzicală"),
    INFORMATICS("Informatică");

    private final String teachingSubjectName;

    TeachingSubjects(String teachingSubjectName) {
        this.teachingSubjectName = teachingSubjectName;
    }
    public String getTeachingSubjectName () {
        return teachingSubjectName;
    }

    @ Override
    public String toString() {
        return  getTeachingSubjectName() ;
    }

}
