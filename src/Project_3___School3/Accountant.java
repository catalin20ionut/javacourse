package Project_3___School3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Accountant extends User {
    private boolean isExpert;
    private String phoneNumber;
    private String emailAlternative;
    private boolean multiAccountant;

    public Accountant(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
    }

    @Override
    public String getClassName() {
        return "Acountant";
    }

    @Override
    protected boolean checkPassword(User user) {
        if (getPassword() != null) {
            return true;
        }
        return false;
    }

    public boolean isExpert() {
        return isExpert;
    }

    public void setISExpert(boolean isExpert) {
        this.isExpert = isExpert;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAlternative() {
        return emailAlternative;
    }

    public void setEmailAlternative(String emailAlternative) {
        this.emailAlternative = emailAlternative;
    }

    public boolean isMultiAccountant() {
        return multiAccountant;
    }

    public void setMultiAccountant(boolean multiAccountant) {
        this.multiAccountant = multiAccountant;
    }

    public void createSalaryList(List<Teacher> teacherList) {
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWORD IS NULL");
        } else {
            String[] months = new String[]{"January", "February", "March", "April", "May", "June", "July", "August",
                    "September", "October", "November", "December"};
            int anualSalary = 0;
            StringBuilder text = new StringBuilder();
            for (String month : months) {
                int totalSalary = 0;
                text.append(month + "\n");
                for (Teacher teacher : teacherList) {
                    text.append(teacher.getClassName() + " - of  -" + teacher.getDisciplines() + "-" + teacher.getFullName() + "-" +
                            +teacher.getSalary() + "\n");
                    System.out.println(teacher.getFullName() + "-" + teacher.getDisciplines().getTeachingSubjects() + "-" +
                            +teacher.getSalary());
                    totalSalary += teacher.getSalary();
                    System.out.println("Total " + month + "-" + totalSalary);
                    anualSalary = totalSalary * 12;
                }
                text.append("Total " + month + "-" + totalSalary + "\n");
            }
            text.append("Total anual- " + anualSalary + "\n");
            System.out.println("Total anual- " + anualSalary);
            try {
                File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School3\\Contabil\\ListaSalarii.txt");
                file.createNewFile();
                FileWriter pw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bf = new BufferedWriter(pw);
                bf.write(text.toString());
                bf.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }
    public void generateScholarshipList(List<Pupil> pupilList) {
        StringBuilder text = new StringBuilder();
        int totalScholarship3 = 0;
        int totalScholarship2 = 0;
        int totalScholarship1 = 0;
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWORD IS NULL");
        } else {
            text.append("Students primarySchool :" + "\n");
            for (Pupil pupil : pupilList) {
                if (pupil.HasScholarship()) {
                    int pupilPrimarySchool = 300;
                    if (pupil.getPupilSchoolType().equals(PupilSchoolType.PRIMARY_SCHOOL_PUPIL)) {
                        text.append(pupil.getFullName() + "\n");
                        totalScholarship1 += pupilPrimarySchool;
                    }
                }
            }
            text.append("Total Primaryschool : 2 x 300 = " + totalScholarship1 + "\n");
            text.append("Students MiddleSchool :" + "\n");
            for (Pupil pupil : pupilList) {
                if (pupil.HasScholarship()) {
                    int pupilMiddleSchool = 500;
                    if (pupil.getPupilSchoolType().equals(PupilSchoolType.MIDDLE_SCHOOL_PUPIL)) {
                        text.append(pupil.getFullName() + "\n");
                        totalScholarship2 += pupilMiddleSchool;
                    }
                }
            }
            text.append("Total Middleschool : 2 x 500 = " + totalScholarship2 + "\n");
            text.append("Students HighSchool :" + "\n");
            for (Pupil pupil : pupilList) {
                if (pupil.HasScholarship()) {
                    int pupilHighSchool = 800;
                    if (pupil.getPupilSchoolType().equals(PupilSchoolType.HIGH_SCHOOL_PUPIL)) {
                        text.append(pupil.getFullName() + "\n");
                        totalScholarship3 += pupilHighSchool;
                    }
                }
            }
            text.append("Total highschool :  3 x 800 =" + totalScholarship3 + "\n");
            text.append("Total  scholarship is : " + (totalScholarship1 + totalScholarship2 + totalScholarship3));
        }
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School3\\Contabil\\ListaBurse.txt");
            file.createNewFile();
            FileWriter pw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(pw);
            bf.write(text.toString());
            bf.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

