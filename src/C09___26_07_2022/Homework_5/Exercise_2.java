package C09___26_07_2022.Homework_5;

/**
2.  Find the number of days in a month. Write a program that will receive the month and year and displays the number of
days in the month. For example, if you entered month 2 and year 2012, the program should display that February 2012 had
29 days. If you entered month 3 and year 2015, the program should display that March 2015 had 31 days.               */
public class Exercise_2 {
    public static void describingMonth (int month, int year) {
        switch (month) {
            case 1:
                System.out.println("January " + year + " had 31 days.");
                break;
            case 2:
                if (year % 4 == 0) {
                    System.out.println("February " + year + " had 29 days.");}
                else {
                    System.out.println("February " + year + " had 28 days.");}
                break;
            case 3:
                System.out.println("March " + year + " had 31 days.");
                break;
            case 4:
                System.out.println("April " + year + " had 30 days.");
                break;
            case 5:
                System.out.println("May " + year + " had 31 days.");
                break;
            case 6:
                System.out.println("June " + year + " had 30 days.");
                break;
            case 7:
                System.out.println("July " + year + " had 31 days.");
                break;
            case 8:
                System.out.println("August " + year + " had 31 days.");
                break;
            case 9:
                System.out.println("September " + year + " had 30 days.");
                break;
            case 10:
                System.out.println("October " + year + " had 31 days.");
                break;
            case 11:
                System.out.println("November " + year + " had 30 days.");
                break;
            case 12:
                System.out.println("December " + year + " had 31 days.");
                break;
            default:
                System.out.println("A year has only twelve months.");
                break;
        }
    }
    public static void main(String[] args) {
        describingMonth(1, 2021);
        describingMonth(2, 2021);
        describingMonth(2, 2020);
        describingMonth(7, 2005);
        describingMonth(22, 2021);
    }
}