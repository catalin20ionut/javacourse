package C09___26_07_2022.Homework_5;
import java.util.Objects;
import java.util.Random;

/**
3. Game: heads or tails ----- Write a program that lets you guess whether the flip of a coin results in heads or tails.
The program randomly generates an integer 0 or 1, which represents head or tail. The program allows you to write a guess
and reports whether the guess is correct or incorrect.                                                               */

public class Exercise_3b {
    public static void flippingCoin (int guessSide) {
        Random side = new Random();                                   //        I created the instance of Random class.
        int side1 = side.nextInt(2);                           //                   This generates random 0 or 1.
        System.out.println("The random number is: " + side1);        // It shows the number that the program generates.

        if (guessSide == side1) {
            System.out.println( "Congratulate! You guessed the side.");}
        else {
            System.out.println("Your guess is incorrect!"); }
    }

    public static void guessingSide (String guessSide2){//
        if (Objects.equals(guessSide2.toLowerCase(), "heads")){
            flippingCoin(0);}
        if (Objects.equals(guessSide2.toLowerCase(), "tails")){
            flippingCoin(1);}
    }

    public static void main(String[] args) {
        guessingSide("HeADS");
    }
}