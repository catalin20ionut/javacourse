package C09___26_07_2022.Homework_5;

/** 1. (Sort three integers) Write a program that will receive three integers and display the integers in non-decreasing
order.  */

public class Exercise_1 {
    public static void sortingIntegers (int x, int y, int z){
        if (x > y && y > z) {
            System.out.println("1st if: " + z + " ---" + y + " --- " + x);}
        else if (x > z && z > y) {
            System.out.println("2nd if: " + y + " --- " + z + " --- " + x);}
        else if (y > x && x > z) {
            System.out.println("3rd if: " + z+ " --- " + x + " --- " + y);}
        else if (y > z && z > x) {
            System.out.println("4th if: " + x + " --- " + z + " --- " + y);}
        else if (z > x && x > y) {
            System.out.println("5th if: " + y + " --- " + x + " --- " + z);}
        else if (z > y && y > x) {
            System.out.println("6th if: " + x + " --- " + y + " --- " + z);}
        else {
            System.out.println("Numbers cannot be arranged in non-decreasing order.");}
    }

    public static void main(String[] args) {
        sortingIntegers(2,6, 5);
        sortingIntegers(2,6, 6);
        sortingIntegers(2,6, 9);
    }
}