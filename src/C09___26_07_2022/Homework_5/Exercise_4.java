package C09___26_07_2022.Homework_5;

/**
4. Game: scissors, rock, paper ---- Write a program that plays the popular scissors-rock-paper game. A pair of scissors
can cut a paper, a rock can knock the scissors and a paper can wrap a rock. The program randomly generates a number 0, 1
or 2 representing scissors, rock and paper. The program will allow you to enter a number 0, 1 or 2 and  displays a mes-
sage indicating whether the user or the computer wins, loses, or it is draw.                                              */
public class Exercise_4 {
    public static String getComputerMove() {
        String computerMove;

        int computerChoice = (int) (Math.random() * 3);
        System.out.println("The random number is: " + computerChoice);

        if (computerChoice == 0) {
            computerMove = "scissors";}
        else if (computerChoice == 1) {
            computerMove = "rock";}
        else {
            computerMove = "paper";}
        System.out.println("So the computer's move is " + computerMove + ".");
        return computerMove;
    }

    public static void Facing (String playerMove) {
        String computerMove = getComputerMove();

        if (playerMove.equalsIgnoreCase(computerMove)){
            System.out.println("Game is draw!");}
        else if (playerMove.equalsIgnoreCase("rock"))
            if (computerMove.equalsIgnoreCase("paper")){
                System.out.println("Computer wins.");}
            else {
                System.out.println("Player wins.");}
        else if (playerMove.equalsIgnoreCase("paper"))
            System.out.println(computerMove.equalsIgnoreCase("scissors") ?"Computer wins.":"Player wins.");
        else
            System.out.println(computerMove.equalsIgnoreCase("rock") ? "Computer wins." : "Player wins.");
    }

    public static void main(String[] args) {
        Facing("RoCk");
    }
}
