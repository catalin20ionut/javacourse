package C09___26_07_2022.Homework_5;
import java.util.Objects;

/**
3. Game: heads or tails ----- Write a program that lets you guess whether the flip of a coin results in heads or tails.
The program randomly generates an integer 0 or 1, which represents head or tail. The program allows you to write a guess
and reports whether the guess is correct or incorrect.                                                               */

public class Exercise_3a {
    // guessSideNo represents the numeric value of heads = 0 and tails = 1
    public static void flippingCoin (int guessSideNo) {
        int side1 = (int) (Math.random() * 2);                          //                This generates random 0 or 1.
        System.out.println("The random number is: " + side1);        // It shows the number that the program generates.

        if (guessSideNo == side1) {
            System.out.println( "Congratulate! You guessed the side.");}
        else {
            System.out.println("Your guess is incorrect!");}
    }

    public static void guessingSide (String guessSide){//
        if (Objects.equals(guessSide.toLowerCase(), "heads")){
            flippingCoin(0);}
        if (Objects.equals(guessSide.toLowerCase(), "tails")){
            flippingCoin(1);}
    }

   public static void main(String[] args) {
       guessingSide("HeADS");
    }
}
