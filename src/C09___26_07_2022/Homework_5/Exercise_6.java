package C09___26_07_2022.Homework_5;

/**       6. Write a program that will receive a letter and check whether the letter is a vowel or consonant. Hint: use
Character.isLetter to verify if it is letter, if it's not a letter print that is an invalid input. Let the program check
no matter is the letter is lowercase or uppercase.              See also Ex_1___Vowel_OR_Consonant C07___26_07_2022! */

public class Exercise_6 {
    public static void checkingLetter(char c) {
         if (Character.isLetter(c)) {
             if (Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' ||
               Character.toLowerCase(c) == 'i' || Character.toLowerCase(c) == 'o' || Character.toLowerCase(c) == 'u') {
                 System.out.println(c + " is a vowel.");}
             else {
                 System.out.println(c + " is a consonant.");}
         }
         else {
               System.out.println("It is not a valid character!");}
    }

    public static void main(String[] args) {
        checkingLetter('A');
        checkingLetter('e');
        checkingLetter('d');
        checkingLetter('8');
    }
}
