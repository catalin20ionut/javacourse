package C09___26_07_2022;
/**
3. Financial application: find the sales amount ----- You have just started a sales job in a department store. Your pay
consists of a base salary and a commission. The base salary is $5,000. The scheme shown below is used to determine the
commission rate.

            Sales Amount                    Commission Rate
            -----------------------------------------------
            $0.01–$5,000                          8 percent
            $5,000.01–$10,000                    10 percent
            $10,000.01 and above                 12 percent

Note that this is a graduated rate. The rate for the first $5,000 is at 8%, the next $5000 is at 10%, and the rest is
at 12%. If the sales amount is 25,000, the commission is 5,000 * 8% + 5,000 * 10% + 15,000 * 12% = 2,700.
Your goal is to earn $30,000 a year. Write a program that finds the minimum sales you have to generate in order to make
$30,000. HINT: use a do ... while loop.

400 + 500 + (x - 10000) * 0.12 = 30000
x = (30000 - 900) / 12 + 10000
*/
public class Ex_3d {
    public static void main(String[] args) {
        // Create a constant value for commission sought
        final double finalCommission = 30000.0;   // COMMISSION_SOUGHT
        double totalSales = 0.0;  // salesAmount,
        double currentCommission = 0.0;  // commission,

        do {
            double currentBalance = 0.0; // balance;
            totalSales += 0.01;			// Increase sales amount by $0.01
            // If sales amount is $10,000.01 and above currentCommission  rate is 12%
            if (totalSales > 10000)
                currentCommission  += (currentBalance = totalSales - 10000) * 0.12;
            // If sales amount is $5,000.01-$10,000 currentCommission  rate is 10%
            if (totalSales > 5000)
                currentCommission  += (currentBalance -= currentBalance - 5000) * 0.10;
            // If sales amount is $0.01-$5,000 commission rate is 8%
            if (totalSales > 0)
                currentCommission  += currentBalance * 0.08;
            // While commission is less than commission sought loop
        } while (currentCommission  < finalCommission);
        // Display results
        System.out.println("Minimum sales to earn $30,000: " + totalSales);
    }
}