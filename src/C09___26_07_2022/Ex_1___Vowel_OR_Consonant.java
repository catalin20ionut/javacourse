package C09___26_07_2022;
/**
(Vowel or consonant?) Write a program that will receive a letter and check whether the letter is a vowel or consonant.
Hint: use Character.isLetter to verify if it's letter, unless it's not a letter print that is an invalid input. Let the
program check no matter is the letter is lowercase or uppercase.                     See also Homework_5 Exercise 6! */
public class Ex_1___Vowel_OR_Consonant {
    public static boolean checkLetter (char c) {
        if(Character.isLetter(c)) {
            switch (Character.toLowerCase(c)) {
                case 'a':
                    System.out.println("A is a vowel.");
                    return true;
                case 'e':
                    System.out.println("E is a vowel.");
                    return true;
                case 'i':
                    System.out.println("I is a vowel.");
                    return true;
                case 'o':
                    System.out.println("O is a vowel.");
                    return true;
                case 'u':
                    System.out.println("U is a vowel.");
                    return true;
                default:
                    System.out.println("That letter is a consonant.");
                    return false;
            }
        }
        else {
            System.out.println("It is not a valid character!");}
        return  false;
    }

    public static void main(String[] args) {
        checkLetter('A');
        checkLetter('d');
        checkLetter('?');
    }
}
