package C09___26_07_2022;

/**
2. Demonstrate cancellation errors ----------------------- A cancellation error occurs when you are manipulating a very
large number with a very small number.   The large number may cancel out the smaller number. For example, the result of
100000000.0 + 0.000000001 is equal to 100000000.0.  To avoid cancellation errors and obtain more accurate results, care-
fully select the order of computation. For example, in computing the following series, you will obtain more accurate re-
sults by computing from right to left rather than from left to right:                 1 + 1 / 2 +  1 / 3 + ... +  1 / n
Write a program that compares the results of the summation of the preceding series, computing from left to right and
from right to left with n = 50000.
*/

public class Ex_2___Difference_in_a_Sum {
    public static double calculatingSumFromLeftToRight(int n){
        double sum1 = 0.0;
        for (double i = 1.0; i <= n; i++){
            sum1 += 1 / i;}
        System.out.println("1. Sum is " + sum1);
        return sum1;
    }

    public static double calculatingSumFromRightToLeft(int n){
        double sum2 = 0.0;
        for (double i = n; i >= 1.0; i--){
            sum2 += 1 / i;}
        System.out.println("2. Sum is " + sum2);
        return sum2;
    }

    public static double calculateDifference (double sum1, double sum2){
        return sum1 -sum2;
        }

    public static void main(String[] args) {
         calculatingSumFromLeftToRight(50000);
         calculatingSumFromRightToLeft(50000);
         double sum1 = calculatingSumFromLeftToRight(50000);
        double sum2 = calculatingSumFromRightToLeft(50000);
        System.out.println("The difference is " + calculateDifference(sum1, sum2));
    }
}
