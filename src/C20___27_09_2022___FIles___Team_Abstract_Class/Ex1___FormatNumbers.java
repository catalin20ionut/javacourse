package C20___27_09_2022___FIles___Team_Abstract_Class;

/** Format an integer -------> Write a method with the following header to format the integer with the specified width.
public static String format(int number, int width)
The method returns a string for the number with one or more prefix 0s. The size of the string is the width. For example,
format(34, 4) returns 0034 and format(34, 5) returns 00034. If the number is longer than the width, the method returns
the string representation for the number. For example, format(34, 1) returns 34. Write a program that allows you to
enter a number and its width and displays a string returned by invoking format(number, width).                       */
public class Ex1___FormatNumbers {
    public static String format(int number, int width) {
        StringBuilder numberAsString = new StringBuilder(number + "");
        if (numberAsString.length() <= width) {
            for (int i = width - numberAsString.length(); i > 0; i--){
                numberAsString.insert(0, 0);}
        }
        return numberAsString.toString();
    }

    public static void main(String[] args) {
        System.out.println(format(34, 1));
        System.out.println(format(123, 3));
        System.out.println(format(34, 5));
    }
}
