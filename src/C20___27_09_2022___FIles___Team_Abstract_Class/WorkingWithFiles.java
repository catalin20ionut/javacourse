package C20___27_09_2022___FIles___Team_Abstract_Class;

import java.io.*;
import java.util.Date;


public class WorkingWithFiles {
    public static void main(String[] args) {
        try {
            System.out.println("A file is created and it will be written in the file.");
            String text = "Today we learn about files.\nHeute erfahren wir Wissenswertes die Arbeit mit Textdateien." +
                    "\nVandaag leren wij over tekstbestanden.\n我らは今日テキストファイルについて学びます。" +
                    "\nالْيَوْمَ نَتَعَرَّفُ أَنِ الْمِلَفَّاتِ.ِ";
            File file = new File("E:\\folder\\test.txt");
            if(!file.exists()){
                file.createNewFile();}
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(fw);
            bf.write(text);
            bf.close();
            System.out.println("ٍI have finished writing in the file.");

            System.out.println("Renaming the file");
            File fileRedenumit= new File("E:\\folder\\redenumit.txt");
            if(file.renameTo(fileRedenumit)){
                System.out.println("I renamed the file.");}

            System.out.println("We check whether the file was been modified or not.");
            long dateModified = fileRedenumit.lastModified();
            System.out.println("Unix Timestamp: " + dateModified);
            Date date = new Date(dateModified);
            System.out.println(date);

            System.out.println("Temporary File");
            // "E:\\folder"); // without the name of the file, until the name of th e folder
            File fileTempLocation= new File("E:\\folder");
            File tempFile = File.createTempFile("JavaTempFile", ".txt", fileTempLocation);
            System.out.println("I have just created the temporary file.");
            // if we want to see the file we put red dot for debugging in the 43rd line; we use the green bug button
            tempFile.deleteOnExit();

            File fileToCompare = new File("E:\\folder\\compare.txt");
            if(fileRedenumit.equals(fileToCompare)){
                System.out.println("We compare two files: The files are the same.");}
            else {
                System.out.println("We compare two files: The files are not the same.");}

            System.out.println("\nReading from the file.");
            BufferedReader br = new BufferedReader((new FileReader("E:\\folder\\redenumit.txt")));
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                System.out.println(currentLine);
            }
            System.out.println("----------------------");

            System.out.println("The size of string int the file: " + fileRedenumit.length()); //
            System.out.println("Can I write? -> " + fileRedenumit.canWrite());
            System.out.println("Can I read? -> " +fileRedenumit.canRead());
            System.out.println("setReadonly: " + fileRedenumit.setReadOnly());              // setting for reading only
            System.out.println("setWritable: " + fileRedenumit.setWritable(false));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
