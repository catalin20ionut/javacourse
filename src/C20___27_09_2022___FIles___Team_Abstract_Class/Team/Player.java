package C20___27_09_2022___FIles___Team_Abstract_Class.Team;

public abstract class Player {
    private String playerName;

    public Player(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }
}
