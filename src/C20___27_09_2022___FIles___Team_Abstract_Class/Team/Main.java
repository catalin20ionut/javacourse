package C20___27_09_2022___FIles___Team_Abstract_Class.Team;
/** Create a generic abstract Player, that will receive a name and is able to get the name of a generic Player. Create 3
different players objects like FootballPlayer, TennisPlayer and BasketPlayer, each player will be able to receive a name
Create a Team object that will have a team name and a list of players called teamMembers. Be able to add a player in a
team, team can be mixed of players, and also to be able to print the numbers of the members for each team. Create at
least 2 teams and a few players in main method.                                                                      */

public class Main {
    public static void main(String[] args) {
        FootballPLayer ronaldo = new FootballPLayer("Cristiano Ronaldo");
        TennisPlayer roger = new TennisPlayer("Roger Federer");
        BasketballPlayer jordan = new BasketballPlayer("Michael Jordan");

        Team romania = new Team("Romania");
        romania.addPlayer(ronaldo);
        romania.addPlayer(roger);
        romania.addPlayer(jordan);

//        romania.addPlayer(jordan);  // It throws an error.
        System.out.println("The team has " + romania.getPlayerNo() + " members.");
    }
}

