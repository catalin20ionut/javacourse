package C20___27_09_2022___FIles___Team_Abstract_Class.Team;

import java.util.LinkedList;
import java.util.List;

public class Team {
    private List<Player> teamMembers = new LinkedList<>();
    private String teamName;

    public Team(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void addPlayer (Player playerName) {
        if(teamMembers.contains(playerName)){
            throw new RuntimeException("The player is already in the team.");
        } else {
            System.out.println("The player " + playerName.getPlayerName() + " is added to the team.");
            teamMembers.add(playerName);}
    }

    public int getPlayerNo (){
        return this.teamMembers.size();
    }
}