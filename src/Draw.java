import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Draw {
    /*
        Pot 1  Pot 2    Pot 3     Pot 4
        1 E1      E5      E09       E13
        2 E2      E6      E10       E14
        3 E3      E7      E11       E15
        4 E4      E8      E12       E16

        Countries E1, E6, E10 = Es
        E2, E3, E5, E16 = En
        E4, E8, E15 = De
        E7, E9, E14 = Ga
        E11, E13 = Ru
        E12 = Ua
    */

    public static void mapCountries (Map<String, String > map) {
        map.put("E01", "Es");
        map.put("E06", "Es");
        map.put("E10", "Es");
        map.put("E02", "En");
        map.put("E03", "En");
        map.put("E05", "En");
        map.put("E16", "En");
        map.put("E04", "De");
        map.put("E08", "De");
        map.put("E15", "De");
        map.put("E07", "Ga");
        map.put("E09", "Ga");
        map.put("E14", "Ga");
        map.put("E11", "Ru");
        map.put("E13", "Ru");
        map.put("E12", "Ua");
        for (String key: map.keySet()) {
            String currentValue = map.get(key);
            System.out.println("Team " + key + " is from " + currentValue +".");
        }

    }

    public static void displayNumbers1_4(int maxvalue) {
        ArrayList<Integer> list1 = new ArrayList<>();
        for (int i = 1; i <= maxvalue; i++) {
            list1.add(i);}

        Collections.shuffle(list1);
        for (int i = 0; i < maxvalue; i++) {
            try {
                Thread.sleep(1000);}
            catch(InterruptedException ex) {
                Thread.currentThread().interrupt();}

//            System.out.format("%-26d", list.get(i));
            if (list1.get(i) == 1) {
                System.out.format("%-26s", "E1");}
            if (list1.get(i) == 2) {
                System.out.format("%-26s", "E2");}
            if (list1.get(i) == 3) {
                System.out.format("%-26s", "E3");}
            if (list1.get(i) == 4) {
                System.out.format("%-26s", "E4");}
        }
    }

    public static void displayNumbers5_8(int maxvalue2) {
        ArrayList<Integer> list2 = new ArrayList<>();
        for (int i = 1; i <= maxvalue2; i++) {
            list2.add(i);}

        Collections.shuffle(list2);
        for (int i = 0; i < maxvalue2; i++) {
            try {
                Thread.sleep(5000);}
            catch(InterruptedException ex) {
                Thread.currentThread().interrupt();}
//            System.out.format("%-26d", list2.get(i) + 4);

            if (list2.get(i) == 1) {
                System.out.format("%-26s", "E5");}
            if (list2.get(i) == 2) {
                System.out.format("%-26s", "E6");}
            if (list2.get(i) == 3) {
                System.out.format("%-26s", "E7");}
            if (list2.get(i) == 4) {
                System.out.format("%-26s", "E8");}}
    }

    public static void displayNumbers9_12(int maxvalue) {
        ArrayList<Integer> list3 = new ArrayList<>();
        for (int i = 1; i <= maxvalue; i++) {
            list3.add(i);}

        Collections.shuffle(list3);
        for (int i = 0; i < maxvalue; i++) {
            try {
                Thread.sleep(5000);}
            catch(InterruptedException ex) {
                Thread.currentThread().interrupt();}
//            System.out.format("%-26d", list3.get(i) + 8);
            if (list3.get(i) == 1) {
                System.out.format("%-26s", "E9");}
            if (list3.get(i) == 2) {
                System.out.format("%-26s", "E10");}
            if (list3.get(i) == 3) {
                System.out.format("%-26s", "E11");}
            if (list3.get(i) == 4) {
                System.out.format("%-26s", "E12");}
        }
    }

    public static void displayNumbers13_16(int maxvalue) {
        ArrayList<Integer> list4 = new ArrayList<>();
        for (int i = 1; i <= maxvalue; i++) {
            list4.add(i);}

        Collections.shuffle(list4);
        for (int i = 0; i < maxvalue; i++) {
            try {
                Thread.sleep(5000);}
            catch(InterruptedException ex) {
                Thread.currentThread().interrupt();}
//            System.out.format("%-26d", list4.get(i) + 12);
            if (list4.get(i) == 1) {
                System.out.format("%-26s", "E13");}
            if (list4.get(i) == 2) {
                System.out.format("%-26s", "E14");}
            if (list4.get(i) == 3) {
                System.out.format("%-26s", "E15");}
            if (list4.get(i) == 4) {
                System.out.format("%-26s", "E16");}
        }
    }

    public static void main(String[] args) {
        Map<String, String> countries = new TreeMap<>();
        mapCountries(countries);

        System.out.println("Group A                   Group B                   Group C                   Group D");
        displayNumbers1_4(4);
        System.out.println();
        displayNumbers5_8(4);
        System.out.println();
        displayNumbers9_12(4);
        System.out.println();
        displayNumbers13_16(4);
    }
}
