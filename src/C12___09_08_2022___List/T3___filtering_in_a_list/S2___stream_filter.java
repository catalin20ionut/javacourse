package C12___09_08_2022___List.T3___filtering_in_a_list;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class S2___stream_filter {
    public static void findingEvenNumber() {
        List<Integer> numberList = new ArrayList<>();
        numberList.add(10);
        numberList.add(11);
        numberList.add(12);
        numberList.add(13);
        numberList.add(14);
        numberList.add(15);
        List<Integer> evenNumberListObj = numberList.stream()
                .filter(number -> number % 2 == 0).collect(Collectors.toList());
        System.out.println(evenNumberListObj);

        /*
        import java.util.ArrayList;
        import java.util.List;

        System.out.print("Display 1 ---> ");
        List<Integer> evenNumberListObj = new ArrayList<>();
        for (Integer number: numberList) {
            if(number % 2 == 0) {
                evenNumberListObj.add(number);
            }
        }
         */

    }
    public static void main(String[] args) {
        findingEvenNumber();
    }
}
