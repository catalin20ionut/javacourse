package C12___09_08_2022___List.T3___filtering_in_a_list;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class S2___stream_filter_Strings {
    public static void displayingNameAndTHowMany() {
        List<String> nameList = new ArrayList<>();
        nameList.add("Maria");
        nameList.add("Andrei");
        nameList.add("George");
        nameList.add("Ana");
        nameList.add("Alexandra");
        nameList.add("Constantin");
        nameList.add("Silviu");
        nameList.add("Sorin");
        nameList.add("Sorina");
        nameList.add("Andreea");
        nameList.add("Alina");
        nameList.add("Alin");
        nameList.add("Xenia");
        nameList.add("Florin");
        nameList.add("Lucian");


        List<String> girlNameList = nameList.stream().filter(name -> name.endsWith("a")).collect(Collectors.toList());
        /* or
        List<String> girlNameList=nameList.stream().filter(name -> name.matches("(.*)a")).collect(Collectors.toList());
        */
        System.out.print(girlNameList);
        System.out.println(" --> Number of girls: " + girlNameList.size());
        List<String> boyNameList
                = nameList.stream().filter(name -> name.matches("(.*)[b-z]")).collect(Collectors.toList());
        System.out.print(boyNameList);
        System.out.println(" --> Number of boys: " + boyNameList.size());

    }
    public static void main(String[] args) {
        displayingNameAndTHowMany();
    }
}