package C12___09_08_2022___List.T3___filtering_in_a_list;

import java.util.ArrayList;
import java.util.List;
// https://zetcode.com/java/streamfilter/

public class S1___forEach {
    public static void findingEvenNumbers(List<Integer> numberList) {
        System.out.print("Display 1 ---> ");
        List<Integer> evenNumberListObj = new ArrayList<>();
        for (Integer number: numberList) {
            if(number % 2 == 0) {
                evenNumberListObj.add(number);
            }
        }
        System.out.println(evenNumberListObj);
        /* or import java.util.stream.Collectors;
        List<Integer> evenNumberListObj = numberList.stream().filter(i -> i % 2 == 0).collect(Collectors.toList());
        System.out.println(evenNumberListObj);
         */

        System.out.println("\nDisplay 2");
        for (Integer number: numberList) {
            System.out.println((number % 2 == 0));
        }

        System.out.println("\nDisplay 3");
        for (Integer number: numberList) {
            System.out.print(number + ": ");
            System.out.println(number % 2 == 0);
        }
    }
    public static void main(String[] args) {
        List<Integer> numberList = new ArrayList<>();
        numberList.add(10);
        numberList.add(11);
        numberList.add(12);
        numberList.add(13);
        numberList.add(14);
        numberList.add(15);
        findingEvenNumbers(numberList);
    }
}