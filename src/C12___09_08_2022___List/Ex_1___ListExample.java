package C12___09_08_2022___List;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
################################################################################################# ArrayList • LinkedList
- Duplicates are allowed.
- Elements are indexed via an integer.
- Looking an item up by index is fast. -> Checking for particular item in list is slow.
- Iterating (for while, forEach) through lists is relatively fast.
- Lists can be sorted.
- If you only add or remove items at end of list, use ArrayList.                 List<String> list1 = new ArrayList<>();
- Removing or adding items elsewhere in the list, use LinkedList                List<String> list2 = new LinkedList<>();
########################################################################################################################
################################################################################################################## Sets
- store only unique values, great for removing duplicates
- are not indexed, unlike lists
- very fast to check if a particular object exists
- order is unimportant and OK if it changes
- HashSet is not ordered.                                                            Set<String> set1 = new HashSet<>();
- Sorted in natural order? Use TreeSet (1,2,3 ..., a,b,c.... etc)                    Set<String> set2 = new TreeSet<>();
- Elements remain in order they were added                                     Set<String> set3 = new LinkedHashSet<>();
#######################################################################################################################
################################################################################################################### Maps
 - Key value pairs.
 - Retrieving a value by key is fast
 - Iterating over map values is very slow
 - Maps not really optimised for iteration
 - Keys not in any particular order,
 - and order liable to change.                                               Map<String, String> map1 = new HashMap<>();
 - keys sorted in natural order                                              Map<String, String> map2 = new TreeMap<>();
 - keys remain in order added                                          Map<String, String> map3 = new LinkedHashMap<>();
#################################################################################################################### */
public class Ex_1___ListExample {
    public static void main(String[] args) {
        List<Integer>  myArrayList = new ArrayList<>();                                       // declaring an ArrayList
        List<Integer>  myLinkedList = new LinkedList<>();
        myArrayList.add(1);                                    // declaring an ArrayList, adding items to the arrayList
        myArrayList.add(-1);
        myArrayList.add(45);
        myArrayList.add(3);
        System.out.println("1. Element with index 0: " + myArrayList.get(0));
        System.out.println("2. Element with index 3: " + myArrayList.get(3));
        System.out.println("3. My ArrayList is ----> " + myArrayList);

        for (int i = 0; i < myArrayList.size(); i++){
            System.out.println("4. Items in myArrayList: " + myArrayList.get(i));}

        myArrayList.remove(2);
        System.out.println("5. Info ------------- item[2] / \"45\" was removed.");
        for (Integer integer : myArrayList) {
            System.out.println("5: (forEach) items     : " + integer);}
        myArrayList.add(0);
        System.out.println("6. My list after value 0 is added: " + myArrayList);
    }
}
