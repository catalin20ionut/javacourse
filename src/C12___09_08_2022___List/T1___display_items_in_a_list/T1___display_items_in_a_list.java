package C12___09_08_2022___List.T1___display_items_in_a_list;

import java.util.ArrayList;

public class T1___display_items_in_a_list {
    public static void show (ArrayList<String> anyArrayList) {
        // taking the items in a list
        for(int i = 0; i < anyArrayList.size(); i++){
            System.out.println("1. --------> " + anyArrayList.get(i));}

        // taking the items in a list for
        for (Object item : anyArrayList) {
            System.out.println("2. For each: " + item);}

        System.out.println("3. Displayed as an ArrayList --> " + anyArrayList);
    }

    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("Ana");
        myArrayList.add("are");
        myArrayList.add("mere");
        show(myArrayList);
    }
}