package C12___09_08_2022___List.T4___possible_words;

import java.util.HashSet;
import java.util.Set;

public class S2_String_permutations {
    public static Set<String> possibleWords(String anyString) {
        Set<String> permutation = new HashSet<>();
        if (anyString == null) {
            return null;
        } else if (anyString.length() == 0) {
            permutation.add("");
            return permutation;
        }

        char initial = anyString.charAt(0);                                       //                     first character
        String remained = anyString.substring(1);                      // Full string without first character
        Set<String> words = possibleWords(remained);
        for (String strNew : words) {
            for (int i = 0;i<=strNew.length();i++){
                permutation.add(charInsert(strNew, initial, i));
            }
        }
        return permutation;
    }

    public static String charInsert(String anyString, char c, int j) {
        String begin = anyString.substring(0, j);
        String end = anyString.substring(j);
        return begin + c + end;
    }

    public static void main(String[] args) {
        String s = "AAC";
        String s1 = "ABC";
        String s2 = "ABCD";
        String s3 = "12345";
        String s4 = "racecar";
        System.out.println("\nPermutations for " + s + " are: \n" + possibleWords(s));
        System.out.println("\nPermutations for " + s1 + " are: \n" + possibleWords(s1));
        System.out.println("\nPermutations for " + s2 + " are: \n" + possibleWords(s2));
        System.out.println("\nPermutations for " + s3 + " are: \n" + possibleWords(s3));
        System.out.println("\nPermutations for " + s4 + " are: \n" + possibleWords(s4));
        System.out.println((long) possibleWords(s1).size());
        System.out.println((long) possibleWords(s4).size());
    }
}