package C12___09_08_2022___List.T5___maximum_in_an_arrayList;

import java.util.ArrayList;
import java.util.List;

// finding the maximum value in an ArrayList
public class Maximum___in_ArrayList {
    public static void displayingMaximumValue (List<Integer> anyArrayList){
        int maximum = anyArrayList.get(0);
        for (int i = 1; i < anyArrayList.size(); i++) {
            if (maximum < anyArrayList.get(i)){
                maximum = anyArrayList.get(i);}
        }
        System.out.println("Maximum value in the ArrayList is \u001B[32m" + maximum + "\u001B[0m.");
    }
    public static void main(String[] args)    {
        ArrayList<Integer> myArrayList = new ArrayList<>();
        myArrayList.add(16);
        myArrayList.add(26);
        myArrayList.add(3);
        myArrayList.add(52);
        myArrayList.add(70);
        myArrayList.add(12);
        myArrayList.add(70);
        displayingMaximumValue(myArrayList);
    }
}