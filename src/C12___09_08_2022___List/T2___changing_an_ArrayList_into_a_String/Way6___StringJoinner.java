package C12___09_08_2022___List.T2___changing_an_ArrayList_into_a_String;

import java.util.ArrayList;
import java.util.StringJoiner;


// Using Java 8 and above
public class Way6___StringJoinner {
    public static void displayString (ArrayList<String> anyArrayList) {
        System.out.println("1. mArrayList --> " + anyArrayList);
        String delimiter = " ";
        StringJoiner theStringMadeFromTheItemsOfAnyArrayList = new StringJoiner(delimiter);
        for (String word : anyArrayList) {
            theStringMadeFromTheItemsOfAnyArrayList.add(word);
        }
        System.out.println("3. " + theStringMadeFromTheItemsOfAnyArrayList + ".");
    }

    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("Ana");
        myArrayList.add("are");
        myArrayList.add("mere");
        displayString(myArrayList);
    }
}