package C12___09_08_2022___List.T2___changing_an_ArrayList_into_a_String;

import java.util.ArrayList;
import java.util.stream.Collectors;

// Using Java 8 and above
public class Way5___stream_map {
    public static void displayString (ArrayList<String> anyArrayList) {
        System.out.println("1. myArrayList -> " + anyArrayList);
        String delimiter = " ";
        String theStringMadeFromTheItemsOfAnyArrayList = anyArrayList
                .stream().map(Object::toString).collect(Collectors.joining(delimiter));
        System.out.println("2. The string -> " + theStringMadeFromTheItemsOfAnyArrayList + ".");
    }

    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("Ana");
        myArrayList.add("are");
        myArrayList.add("mere");
        displayString(myArrayList);
    }
}