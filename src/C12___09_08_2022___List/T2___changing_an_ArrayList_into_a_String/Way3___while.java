package C12___09_08_2022___List.T2___changing_an_ArrayList_into_a_String;

import java.util.ArrayList;

public class Way3___while {
    public static void displayString (ArrayList<String> anyArrayList) {
        System.out.println("1. mArrayList --> " + anyArrayList);
        String delimiter = " ";
        StringBuilder theStringMadeFromTheItemsOfAnyArrayList = new StringBuilder(delimiter);
        int i = 0;
        while (i < anyArrayList.size()) {
            theStringMadeFromTheItemsOfAnyArrayList.append(anyArrayList.get(i));
            theStringMadeFromTheItemsOfAnyArrayList .append(delimiter);
            i++;
        }
        System.out.println("3." + theStringMadeFromTheItemsOfAnyArrayList + "\b.");
    }

    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("Ana");
        myArrayList.add("are");
        myArrayList.add("mere");
        displayString(myArrayList);
    }
}