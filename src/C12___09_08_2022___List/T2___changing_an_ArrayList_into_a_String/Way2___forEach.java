package C12___09_08_2022___List.T2___changing_an_ArrayList_into_a_String;

import java.util.ArrayList;

public class Way2___forEach {
    public static void displayString (ArrayList<String> anyArrayList) {
        System.out.println("1. myArrayList --> " + anyArrayList);
        String delimiter = " ";
        StringBuilder theStringMadeFromTheItemsOfAnyArrayList = new StringBuilder(delimiter);

        for (String word : anyArrayList) {
            theStringMadeFromTheItemsOfAnyArrayList.append(word);
            theStringMadeFromTheItemsOfAnyArrayList.append(delimiter);}
        System.out.println("2." + theStringMadeFromTheItemsOfAnyArrayList + "\b.");
    }

    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("Ana");
        myArrayList.add("are");
        myArrayList.add("mere");
        displayString(myArrayList);
    }
}
