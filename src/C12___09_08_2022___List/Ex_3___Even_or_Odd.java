package C12___09_08_2022___List;

import java.util.ArrayList;
import java.util.List;

/**                              Write a method that returns a comma separated string based on a given list of integers.
Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o' if the number is
odd. For example, if the input list is (3, 44, 5, 10), the output should be 'o3,e44,o5,e10'.                         */

public class Ex_3___Even_or_Odd {
    public static String displayNumbers(List<Integer> numbers) {
        StringBuilder result = new StringBuilder();
        for (Integer i: numbers){
             if (i % 2 == 0){
//                 result.append("o" + i + ", ");}
                 result.append("e").append(i).append(", ");}
             else {
//                 result.append("o" + i + ", ");}
                 result.append("o").append(i).append(", ");}
        }
//        return  result.toString();                                                  // it displays a comma at the end
        return result.substring(0, result.length() - 2);                             // it displays no comma at the end
    }

    public static void main(String[] args) {
       List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(12);
        numbers.add(9);
        numbers.add(45);
        System.out.println(displayNumbers(numbers));
    }
}