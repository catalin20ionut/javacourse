package C12___09_08_2022___List;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EX_2___ArrayListVersusLinkedList {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();

        calculatingTime(arrayList, "arrayList");
        calculatingTime(linkedList, "linkedList");
    }

    public static void calculatingTime (List<Integer> list, String typeList){
        long start = System.currentTimeMillis();
        for(int i=0; i < 100_000; i++) {
            list.add(i);}
        for(int i=0; i < 100_000; i++) {
            list.add(100, i);}
        long end = System.currentTimeMillis();
        System.out.println("Execution time in milliseconds: " + (end - start) + " for the type " + typeList + ".");
    }
}
