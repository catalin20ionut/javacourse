package C07___19_07_2022___Array;
/**
 1. Determine if a triangle is equilateral, isosceles or scalene. An equilateral triangle has all three sides the same
length. An isosceles triangle has at least two sides the same length. (It is sometimes specified as having exactly two
sides the same length, but for the purposes of this exercise we'll say at least two.) A scalene triangle has all sides
of different lengths.
Note: For a shape to be a triangle at all, all sides have to be of length > 0  and the sum of the lengths of any two
sides.                                                                                                               */

public class Ex1___Triangle {
    private final double latura1;
    private final double latura2;
    private final double latura3;

    /** In the exercise we wrote
     private double latura1;
     private double latura2;
     private double latura3; */

    public Ex1___Triangle(double latura1, double latura2, double latura3) {
        if (latura1 <= 0 || latura2 <= 0 || latura3 <= 0 ||
                latura1+latura2 <= latura3 || latura1 +latura3 <= latura2 || latura2 + latura3 <= latura1){
            System.out.println("Nu este triunghi.");}
            this.latura1 = latura1;
            this.latura2 = latura2;
            this.latura3 = latura3;
    }

    public void verificaTriunghiIsoscel(){
        if(latura1 == latura2 || latura1 == latura3 || latura2 == latura3){
            System.out.println("Triunghiul este isoscel.");}
    }

    public void verificaTriunghiEchilateral(){
        if (latura1 == latura2 && latura2== latura3){
            System.out.println("Triunghiul este echilateral.");}
    }

    public void verificaTriunghiOarecare(){
        if (latura1 != latura2 && latura1!= latura3 && latura2 !=latura3){
            System.out.println("Triunghiul este oarecare.");}
    }

    public static void main(String[] args) {
        Ex1___Triangle tr1 = new Ex1___Triangle(5,5,5);
        tr1.verificaTriunghiOarecare();
        tr1.verificaTriunghiIsoscel();
        tr1.verificaTriunghiEchilateral();
    }
}