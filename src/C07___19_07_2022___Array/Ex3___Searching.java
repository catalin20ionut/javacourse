package C07___19_07_2022___Array;
/**
3. Write a method that returns the index of the first occurrence of given integer in a list.
Assume that the index of the first element in the list is zero. If the number does not exist return -1.              */

public class Ex3___Searching {
    public static int searchNumber(int n,  int [] array){
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n){
                index = i;
                break;}
        }
        return index;
    }

    public static void main(String[] args) {
        int[] numbers = {2, 3, 4, 5, 6};
        System.out.println(searchNumber(4,numbers));
        System.out.println(searchNumber(9,numbers));
    }
}