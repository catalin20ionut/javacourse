package C07___19_07_2022___Array.Homework_4;

/** Geometry: distance of two points --------------------------------------------- Write a program that will receive two
points (x1, y1) and (x2, y2) and displays their distance between them.                                               */
public class Exercise_10 {
    public static void distance(int x1, int x2, int y1, int y2) {
        double distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2) * 1);
        System.out.println("The distance between the two points is: " + distance);
    }

    public static void main(String[] args) {
        distance(3, 6, 6, 10);
    }
}