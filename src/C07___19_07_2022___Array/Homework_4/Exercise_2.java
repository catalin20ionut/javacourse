package C07___19_07_2022___Array.Homework_4;

/** Convert pounds into kilograms ----- Write a program that converts pounds into kilograms. The program will receive a
number in pounds, converts it to kilograms, and displays the result. One pound is 0.454 kilograms.                   */
public class Exercise_2 {
    public static void convertingPoundsIntoKilograms(double pounds) {
        double kilograms = pounds * 0.454;
        System.out.println(pounds + " pounds are " + kilograms + " kilograms.");
    }

    public static void main(String[] args) {
        convertingPoundsIntoKilograms(84);
    }
}