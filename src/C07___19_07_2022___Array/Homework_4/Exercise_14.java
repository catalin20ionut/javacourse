package C07___19_07_2022___Array.Homework_4;

/** Cost of driving --- Write a program that will receive the distance to drive, the fuel efficiency of the car in miles
per gallon, and the price per gallon and displays the cost of the trip.                                               */
public class Exercise_14 {
    public static void getCost(double distance, double fE, double pG){
        double cost = distance * pG / fE;
        System.out.println("The cost for the trip is: $" + cost);
    }

    public static void main(String[] args) {
        getCost(85, 6.5, 3.2);
    }
}