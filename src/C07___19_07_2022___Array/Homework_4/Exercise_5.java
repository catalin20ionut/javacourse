package C07___19_07_2022___Array.Homework_4;

/** Find the number of years - Write a program that will receive the minutes (e.g., 1 billion), and displays the number
of years and days for the minutes. For simplicity, assume a year has 365 days.                                       */

public class Exercise_5 {
    public static void findNumberOfYears(int minutes) {
        int years = minutes / 525600;
        System.out.println("Years:    "+ years);

        int days = ((minutes - 525600) / 1440);
        System.out.println("Days : " + days);
    }

    public static void main(String[] args) {
        findNumberOfYears(322546533);
    }
}
