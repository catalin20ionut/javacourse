package C07___19_07_2022___Array.Homework_4;

/** See homework 3 - Exercise_1 !!!
Sum the digits in an integer --- Write a program that will receive an integer between 0 and 1000 and adds all the digits
in the integer. For example, if an integer is 932, the sum of all its digits is 14.
Hint:          Use the % operator to extract digits, and use the / operator to remove the extracted digit. For instance,
32 % 10 = 2 and 932 / 10 = 93. In case that the number is not between 0 and 1000 you will not calculate the sum.     */
public class Exercise_4___Sum_of_Digits {
    public  static void sumDigits(int n) {
        if (n <= 1000) {
            int sum = 0;
            for (int i = 0; i <= 1000; i++) {
                sum += n % 10;
                n = n / 10;
            }
            System.out.println(sum);
        }
        else {
            System.out.println("Your number is not between 0 and 1000");
        }
    }

    public static void main(String[] args) {
        sumDigits(933);
    }
}
