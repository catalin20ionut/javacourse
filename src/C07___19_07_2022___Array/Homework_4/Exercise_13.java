package C07___19_07_2022___Array.Homework_4;

/** Print a table -------------- Write a program that displays the following table. Cast floating numbers into integers.
a b pow(a, b)
1 2        1
2 3        8
3 4       81
4 5     1024
5 6    15625                                                                                                         */
public class Exercise_13 {
    public static void calculatePow(){
        System.out.println("a" + " b " + "pow(a, b)");
        for(int i=1; i<6; i++){
            System.out.format("%s %9s%n", i + " " + (i+1), (int) Math.pow(i,i+1));}
}
    public static void main(String[] args) {
        calculatePow();
    }
}