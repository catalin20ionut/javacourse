package C07___19_07_2022___Array.Homework_4;

/** Financial application: compound value ------ Suppose you save $100 each month into a savings account with the annual
interest rate 5%. So, the monthly interest rate is 0.05 / 12 = 0.00417. After the first month, the value in the account
becomes 100 * (1 + 0.00417) = 100.417.
After the second month, the value in the account becomes (100 + 100.417) * (1 + 0.00417) = 201.252
After the third month, the value in the account becomes  (100 + 201.252) * (1 + 0.00417) = 302.507            and so on.
Write a program that will receive an amount (e.g., 100), the annual interest rate (e.g., 5), and the number of months
(e.g., 6) and displays the amount in the savings account after the given month.                                      */

public class Exercise_9 {
    public static void getFinalValue(double amount, double interestRate, int noOfMonths){
        interestRate = (interestRate/100)/12;
        double result = 0;
        for (int i = 0; i<=noOfMonths; i++) {
            result = (result + amount) * (1 + interestRate);}
        System.out.println("The savings account is "+ result + " euros after "+ noOfMonths + " months.");
    }

    public static void main(String[] args) {
        getFinalValue(100,5,6);
    }
}
