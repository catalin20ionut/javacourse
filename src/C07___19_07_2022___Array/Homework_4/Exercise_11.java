package C07___19_07_2022___Array.Homework_4;

/** Geometry: area of a hexagon ----- Write a program that will receive the side of a hexagon and displays its area. */
public class Exercise_11 {
    public static void hexagonArea(double side){
        double area= 3*Math.sqrt(3)*Math.pow(side,2)/2;
        System.out.println("The area of the hexagon is: "+area);
    }

    public static void main(String[] args) {
        hexagonArea(5);
    }
}