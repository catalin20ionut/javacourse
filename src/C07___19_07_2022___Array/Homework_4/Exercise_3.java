package C07___19_07_2022___Array.Homework_4;
/** Financial application: calculate tips - Write a program that will receive the subtotal and the gratuity rate, then
computes the gratuity and total. For example, if you will enter 10 for subtotal and 15% for gratuity rate, the program
displays $1.5 as gratuity and $11.5 as total.                                                                       */
public class Exercise_3 {
    public static void calculateTips(double subtotal, double gratuityRate) {
        double gratuity = subtotal * (gratuityRate/100);
        double total = subtotal + gratuity;
        System.out.format("%-11s: %s%n", "Total is", "$" + total);
        System.out.format("%-11s: %5s%n", "Gratuity is", "$" + gratuity);
    }

    public static void main(String[] args) {
        calculateTips(10, 15);
    }
}

