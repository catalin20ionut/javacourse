package C07___19_07_2022___Array.Homework_4;

/** Science: wind-chill temperature - How cold is it outside? The temperature alone is not enough to provide the answer.
Other factors including wind speed, relative humidity and sunshine play important roles in determining coldness outside.
In 2001, the National Weather Service (NWS) implemented the new wind-chill temperature to measure the coldness using
temperature and wind speed. The formula is                        twc = 35.74 + 0.6215ta - 35.75v^0.16 + 0.4275tav^0.16
where ta is the outside temperature measured in degrees Fahrenheit and v is the speed measured in miles per hour, twc is
the wind-chill temperature. The formula cannot be used for wind speeds below 2 mph or temperatures below -58 oF or above
41F. Write a program that will receive a temperature between -58F and 41F and a wind speed greater than or equal to 2
and displays the wind-chill temperature. If the values received are not properly you will need to stop the program and
print that is not possible to use the formula Use Math.pow(a, b) to compute.                                         */
public class Exercise_12 {
    public static void findWindChillTemp(double temp, double wSpeed){
        if(temp >= -58 && temp <= 41 && wSpeed >= 2) {
            double twc = 35.74 + 0.6215 * temp - 35.75 * Math.pow(wSpeed, 0.16) + 0.4275 * Math.pow(temp * wSpeed, 0.16);
            System.out.println(twc);}
        else {
            System.out.println("The formula cannot be used!");}
    }

    public static void main(String[] args) {
        findWindChillTemp(31, 25);
    }
}