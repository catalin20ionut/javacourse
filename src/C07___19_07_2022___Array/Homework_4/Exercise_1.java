package C07___19_07_2022___Array.Homework_4;

/** Convert feet into meters - Write a program that will receive a number in feet, converts it to meters, and displays
the result. One foot is 0.305 meter.                                                                                 */
public class Exercise_1 {
    public static void convertIntoMeter(double feet){
        double meter= feet * 0.305;
        System.out.println(feet + " feet are " + meter + " meters.");
    }

    public static void main(String[] args) {
        convertIntoMeter(34);
    }
}