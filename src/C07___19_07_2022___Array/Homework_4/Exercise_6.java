package C07___19_07_2022___Array.Homework_4;

/** Physics: acceleration -- Average acceleration is defined as the change of velocity divided by the time taken to make
 the change, as shown in the following formula:
                                                     v1 - v0
                                                 a = --------
                                                        t
Write a program that will receive the starting velocity v0 in meters/second, the ending velocity v1 in meters/second,
and the time span t in seconds, and displays the average acceleration.                                               */

public class Exercise_6 {
    public static void getAcceleration(double v0, double v1, int i) {
        double a = (v1 - v0) / i;
        System.out.println("The average acceleration is " + a);
    }

    public static void main(String[] args) {
        getAcceleration(400,800,85);
    }
}