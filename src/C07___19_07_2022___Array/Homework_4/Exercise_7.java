package C07___19_07_2022___Array.Homework_4;

/** Science: calculating energy -------- Write a program that calculates the energy needed to heat water from an initial
temperature to a final temperature. Your program should receive the amount of water in kilograms and the initial and
final temperatures of the water. The formula to compute the energy is
                                                                  Q = M * (finalTemperature – initialTemperature) * 4184
where M is the weight of water in kilograms, temperatures are in degrees Celsius, and energy Q is measured in joules. */
public class Exercise_7 {
    public static void calculateEnergy(int w, int iTemp, int fTemp) {
        int Q = (w * (fTemp - iTemp) * 4184);
        System.out.println("Energy (Q) = " + Q + " joules");
    }

    public static void main(String[] args) {
        calculateEnergy(5, 25, 50);
    }
}