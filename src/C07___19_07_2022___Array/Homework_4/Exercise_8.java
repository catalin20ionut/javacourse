package C07___19_07_2022___Array.Homework_4;

/** Physics: finding runway length ------- Given an airplane’s acceleration a and take-off speed v, you can compute the
minimum runway length needed for an airplane to take off using the following formula:
                                                                                                                    v^2
                                                                                                           length = ---
                                                                                                                    2a
Write a program that will receive v in meters/second (m/s) and the acceleration a in meters/second squared (m/s2), and
displays the minimum runway length.                                                                                  */
public class Exercise_8 {
    public static void findRunwayLength (int v, int a) {
        double length=((Math.pow(v,2))/(2*a));
        System.out.println("Minimum runway length needed is " + length);
    }

    public static void main(String[] args) {
        findRunwayLength(25, 45);
    }
}
