package C07___19_07_2022___Array;

public class Ex2___Array_Examples {
    public static void main(String[] args) {
        String s1 = "Java este limbajul meu preferat";                   // declaring a string, how to declare a string
        String[] my_array = s1.split("\\s");                      //              changing a string into an array
        for (int i = 0; i < my_array.length; i++){
            System.out.println("1." + my_array[i]);}

        String[] fructe = {" mar", " para", "banana"};
        fructe[0] = "ananas";
        String[] legume = new String[6];
        legume[0] = "rosie";
        legume[2] = "varza";
        legume[5] = "castravete";
        System.out.println("2a." + legume[0]);
        System.out.println("2b." + legume[1]);
        System.out.println("2c." + legume[5]);

        int[] numbers = {1, 2, 3, 4, 10};
        for (int index = 0; index <= numbers.length -1; index++){
            System.out.println("3. number: " + (numbers[index]));}
            
        int[] numere = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("4. Print array before reverse");
        for (int i = 0; i <= numere.length - 1; i++) {
            System.out.println("5. >>> " + (numere[i] + " "));}
        // from the second index to the sixth index
        for (int i = 2; i <= numere.length - 4; i++) {
            System.out.println("5b. >>> " + (numere[i] + " "));}

        System.out.print("\n6. On the same row: ");
        for (int i = 0; i <= numere.length - 1; i++) {
            System.out.print(numere[i] + " ");}

        // parcurgere array pe jumătate pentru inversare valori --- important 1:57
        for (int i = 0; i < numere.length / 2; i++) {
            System.out.println("\n6.1: Am intrat în for.");
            System.out.println("6.2: Valoare numere[i]: " + numere[i]);
            int temp = numere[i];
            System.out.println("6.3 Temporar inițial este: " + temp);
            numere[i] = numere[numere.length -1 - i];
            System.out.println("6.4 Valoare numere[1] după inversare: " + numere[i]);
            numere[numere.length -1 -i] = temp;
            System.out.print("6.5 Temp după inversare este: " + temp + "\n");}

        System.out.print("\n6.6 Print array după inversare: ");
        for (int i = 0; i <= numere.length - 1; i++) {
            System.out.print(numere[i] + " ");}
    }
}

