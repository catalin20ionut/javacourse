package Project_3___School;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Admin admin = new Admin("Ioana", "Visinescu", "ioanavisinescu@gmail.com");
        admin.writingAdministrator();

        Accountant accountant = new Accountant("George", "Contabil", "georgecontabil@gmail.com");
        admin.generatingPasswordForUser(accountant);
        admin.writingUser(accountant);

        Parent parent = new Parent("Cristi", "Popescu", "cristipopescu@gmail.com");
        admin.generatingPasswordForUser(parent);
        admin.writingUser(parent);

        Parent parent2 = new Parent("Vasile", "Gica", "vasilegica@gmail.com@gmail.com");
        admin.generatingPasswordForUser(parent2);
        admin.writingUser(parent2);

        Pupil pupil1 = new Pupil("Darius", "Graur", "dariusgraur@gmail.com");
        admin.generatingPasswordForUser(pupil1);
        admin.writingUser(pupil1);
        pupil1.setPupilSchoolType(PupilSchoolType.PRIMARY_SCHOOL_PUPIL);
        pupil1.setHasScholarship(true);

        Pupil pupil2 = new Pupil("Vali", "Lucescu", "valilucescu@gmail.com");
        admin.generatingPasswordForUser(pupil2);
        admin.writingUser(pupil2);
        pupil2.setPupilSchoolType(PupilSchoolType.PRIMARY_SCHOOL_PUPIL);
        pupil2.setHasScholarship(true);

        Pupil pupil3 = new Pupil("Maria", "Ioana", "mariaioana@gmail.com");
        admin.generatingPasswordForUser(pupil3);
        admin.writingUser(pupil3);
        pupil3.setPupilSchoolType(PupilSchoolType.MIDDLE_SCHOOL_PUPIL);
        pupil3.setHasScholarship(true);

        Pupil pupil4 = new Pupil("Marius", "Ionel", "mariusionel@gmail.com");
        admin.generatingPasswordForUser(pupil4);
        admin.writingUser(pupil4);
        pupil4.setPupilSchoolType(PupilSchoolType.MIDDLE_SCHOOL_PUPIL);
        pupil4.setHasScholarship(true);

        Pupil pupil5 = new Pupil("Valentina", "Lucescu", "valentinabucescu@gmail.com");
        admin.generatingPasswordForUser(pupil5);
        admin.writingUser(pupil5);
        pupil5.setPupilSchoolType(PupilSchoolType.HIGH_SCHOOL_PUPIL);
        pupil5.setHasScholarship(true);

        Pupil pupil6 = new Pupil("Cătălin", "Grosu", "catalingrosu@gmail.com");
        admin.generatingPasswordForUser(pupil6);
        admin.writingUser(pupil6);
        pupil6.setPupilSchoolType(PupilSchoolType.HIGH_SCHOOL_PUPIL);
        pupil6.setHasScholarship(false);

        Pupil pupil7 = new Pupil("Oana", "Turcitu", "oanaturcitu@gmail.com");
        admin.generatingPasswordForUser(pupil7);
        admin.writingUser(pupil7);
        pupil7.setPupilSchoolType(PupilSchoolType.HIGH_SCHOOL_PUPIL);
        pupil7.setHasScholarship(true);

        Teacher teacher1 = new Teacher("Alin", "Roman", "alinroman@gmail.com");
        admin.generatingPasswordForUser(teacher1);
        admin.writingUser(teacher1);
        teacher1.setTeacherType(TeacherType.PRIMARY_SCHOOL);

        Teacher teacher2 = new Teacher("Dacian", "Ion", "dacianionescu@gmail.com");
        admin.generatingPasswordForUser(teacher2);
        admin.writingUser(teacher2);
        teacher1.setTeacherType(TeacherType.MIDDLE_SCHOOL);

        Teacher teacher3 = new Teacher("Alina", "Popescu", "alinapopescu@gmail.com");
        admin.generatingPasswordForUser(teacher3);
        admin.writingUser(teacher3);
        teacher3.setTeacherType(TeacherType.HIGH_SCHOOL);


        // cream o scola si adaugam membri in ea
        School school = new School("Ana Aslan");
        school.addUser(admin);
        school.addUser(teacher1);
        school.addUser(teacher2);
        school.addUser(teacher3);
        school.addUser(parent);
        school.addUser(parent2);
        school.addUser(pupil6);
        school.addUser(pupil7);
        school.addUser(pupil3);
        System.out.println(school.getSchoolMembers());
        admin.createMembersFolder(school);

        //facem o lista de useri
        List<User> userList = new ArrayList<>();
        userList.add(teacher1);
        userList.add(teacher2);
        userList.add(teacher3);
        userList.add(admin);
        userList.add(parent);
        userList.add(parent2);
        userList.add(pupil6);
        userList.add(pupil7);
        userList.add(pupil3);

        //stergem fisier si user din lista
        System.out.println("old" + userList);
        // admin.deleteFile(pupil6);
        admin.deletingUser(pupil6, userList);
        System.out.println("new " + userList);

        //resetam parola
//        System.out.println("old : " + admin.getPassword(parent));
//        admin.resettingPassword(parent);
//        System.out.println("new : " + admin.getPassword(parent));


        //creare fisier cu membri scolii
        admin.createMembersFolder(school);
        //declaram clasele
        Classes classes0 = Classes.GRADE_0;
        Classes classes1 = Classes.GRADE_1;
        Classes classes2 = Classes.GRADE_2;
        Classes classes3 = Classes.GRADE_3;
        Classes classes4 = Classes.GRADE_4;
        Classes classes5 = Classes.GRADE_5;
        Classes classes6 = Classes.GRADE_6;
        Classes classes7 = Classes.GRADE_7;
        Classes classes8 = Classes.GRADE_8;
        Classes classes9 = Classes.GRADE_9;
        Classes classes10 = Classes.GRADE_10;
        Classes classes11 = Classes.GRADE_11;
        Classes classes12 = Classes.GRADE_12;

        // am facut disciplinele
        Disciplines romanianLanguage = new Disciplines(TeachingSubjects.ROMANIAN_LANGUAGE_AND_LITERATURE);
        Disciplines matematics = new Disciplines(TeachingSubjects.MATHEMATICS);
        Disciplines biology = new Disciplines(TeachingSubjects.BIOLOGY);
        Disciplines chemestry = new Disciplines(TeachingSubjects.CHEMESTRY);
        Disciplines history = new Disciplines(TeachingSubjects.HISTORY);
        Disciplines informatics = new Disciplines(TeachingSubjects.INFORMATICS);
        Disciplines germanLanguage = new Disciplines(TeachingSubjects.GERMAN_LANGUAGE);
        Disciplines geography = new Disciplines(TeachingSubjects.GEOGRAPHY);

        //am adaugat clase  la care preda profesorul
        teacher1.addClasses(classes1);
        teacher1.addClasses(classes2);
        teacher1.addClasses(classes3);

        teacher2.addClasses(classes5);
        teacher2.addClasses(classes6);
        teacher2.addClasses(classes7);

        teacher3.addClasses(classes10);
        teacher3.addClasses(classes11);
        teacher3.addClasses(classes12);

        //setam o clasa la elev
        pupil6.setClasses(classes1);
        pupil7.setClasses(classes1);
        pupil3.setClasses(classes11);

        //  generam fisier cu elvi
        System.out.println("Password is : " + teacher1.getPassword());
        teacher1.generateStudentFiles(school, classes1);


        // facem o lista de discipline

        List<Disciplines> disciplinesList = new ArrayList<>();
        disciplinesList.add(romanianLanguage);
        disciplinesList.add(matematics);
        disciplinesList.add(germanLanguage);
        disciplinesList.add(chemestry);
        disciplinesList.add(geography);
        disciplinesList.add(history);
        disciplinesList.add(informatics);
        disciplinesList.add(biology);
        System.out.println(disciplinesList);

        //adaugam discipline la elev
        pupil6.addDisciplines(biology);
        pupil6.addDisciplines(history);
        pupil6.addDisciplines(geography);
        pupil6.addDisciplines(romanianLanguage);

        pupil7.addDisciplines(biology);
        pupil7.addDisciplines(history);
        pupil7.addDisciplines(geography);
        pupil7.addDisciplines(romanianLanguage);

        pupil3.addDisciplines(biology);
        pupil3.addDisciplines(history);
        pupil3.addDisciplines(geography);
        pupil3.addDisciplines(romanianLanguage);

        //setam didciplina la prof
        teacher1.setDisciplines(biology);
        teacher3.setDisciplines(history);
        teacher2.setDisciplines(geography);

        //prof adauga note la elev
        teacher1.addMarks(6, pupil6, teacher1, biology);

        //lista de note
        List<Integer> markList = new ArrayList<>();
        markList.add(8);
        markList.add(9);
        markList.add(10);
        markList.add(7);
        markList.add(3);
        System.out.println(pupil6);

        teacher1.addMarks(7, pupil6, teacher1, biology);
        teacher1.addMarks(9, pupil6, teacher1, biology);
        teacher1.addMarks(4, pupil6, teacher1, biology);
        System.out.println(pupil6.makeAverageDiscipline(markList, biology));


        // facem lista de note
        List<Integer> markList3 = new ArrayList<>();
        markList3.add(9);
        markList3.add(9);
        markList3.add(9);

        //adaugam note
        teacher3.addMarks(9, pupil6, teacher3, history);
        teacher3.addMarks(9, pupil6, teacher3, history);
        teacher3.addMarks(9, pupil6, teacher3, history);
        System.out.println(pupil6.makeAverageDiscipline(markList3, history));

        // facem lista de note noua
        List<Integer> markList2 = new ArrayList<>();
        markList2.add(7);
        markList2.add(8);
        markList2.add(9);

        //adaugam note
        teacher2.addMarks(7, pupil6, teacher2, geography);
        teacher2.addMarks(7, pupil6, teacher2, geography);
        teacher2.addMarks(7, pupil6, teacher2, geography);
        System.out.println(pupil6.makeAverageDiscipline(markList2, geography));
        pupil6.uploadHomework(pupil6, biology, "Am incarcat tema la bilogie inca o data");

        //facem o lista de medii
        List<Double> avgDouble = new ArrayList<>();
        avgDouble.add(8.9);
        avgDouble.add(7.4);
        avgDouble.add(5.8);
        avgDouble.add(8.8);
        //facem media anuala
        System.out.println("Anual avg : " + pupil6.makeAnualAverageForDisciplines(avgDouble, disciplinesList));


        pupil6.uploadHomework(pupil7, biology, "Am încărcat tema.");


        teacher1.setSalary(5000.50);
        teacher2.setSalary(4000.50);
        teacher3.setSalary(3000.50);

        //generare fisier salarii profesori
        List<Teacher> teacherSalaryList = new ArrayList<>();
        teacherSalaryList.add(teacher1);
        teacherSalaryList.add(teacher2);
        teacherSalaryList.add(teacher3);
        // generam lista salarii
        accountant.createSalaryList(teacherSalaryList);


        // facem o lista de elevi
        List<Pupil> pupilListScholarship = new ArrayList<>();
        pupilListScholarship.add(pupil1);
        pupilListScholarship.add(pupil2);
        pupilListScholarship.add(pupil3);
        pupilListScholarship.add(pupil4);
        pupilListScholarship.add(pupil5);
        pupilListScholarship.add(pupil6);
        pupilListScholarship.add(pupil7);
        // generam lista burse
        accountant.generateScholarshipList(pupilListScholarship);

    }

}



