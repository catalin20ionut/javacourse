package Project_3___School;

public enum PupilSchoolType {
    PRIMARY_SCHOOL_PUPIL("Clasa 0--Clasa 4"),
    MIDDLE_SCHOOL_PUPIL("Clasa 5--Clasa8"),
    HIGH_SCHOOL_PUPIL("Clasa 9--Clasa 12");
    private String description;

    PupilSchoolType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
