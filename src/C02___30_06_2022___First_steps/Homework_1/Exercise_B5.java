package C02___30_06_2022___First_steps.Homework_1;

/**
5. Declare two variables of String type and next please declare the boolean variable, which will store the result of
checking if two previously declared String values are equal (Hint: use equals() method from String class). Display
boolean value on the standard output (screen).
 */

public class Exercise_B5 {
    public static void main(String[] args) {
        String myString1;
        String myString2;
        myString1 = "Hello people! Welcome to Java!";
        myString2 = "Hello People! Welcome to Java!";

        boolean booleanVar;
        booleanVar = myString1.equals(myString2);
        System.out.println(booleanVar);                              // False, because people and People are different.
    }
}
