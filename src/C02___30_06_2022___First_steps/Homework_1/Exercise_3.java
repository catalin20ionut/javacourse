package C02___30_06_2022___First_steps.Homework_1;

/**
33. Please try to understand and run in a Java program the following lines:
                                                                                  ♦     System.out.println(2 + 12 / 4);
                                                                                  ♦         System.out.println(21 % 5);
                                                                                  ♦      System.out.println(3 - 5 % 7);
                                                                                  ♦       System.out.println(17.0 / 4);
                                                                                  ♦    System.out.println(8 - 5 * 2.0);
                                                                                  ♦ System.out.println(14 + 5 % 2 - 3);
                                                                                  ♦   System.out.println(15.0 + 3 / 2);
                                                                                  ♦         System.out.println(13 / 4);
*/

public class Exercise_3 {
    public static void main(String[] args) {
        System.out.println(2 + 12 / 4);                                  //                                       2 + 3
        System.out.println(21 % 5);                                     //                            21 : 5 = 4 rest 1
        System.out.println(3 - 5 % 7);                                 //                               3 - rest 5 = -2
        System.out.println(17.0 / 4);                                 //                            float 17 : 4 = 4.25
        System.out.println(8 - 5 * 2.0);                             //                                    8 - 10 = - 2
        System.out.println(14 + 5 % 2 - 3);                         //                             14 + rest 1 - 3 = 12
        System.out.println(15.0 + 3 / 2);                          // double 15 + integer 3 / integer 2 = 15d + 1 = 16d
        System.out.println(13 / 4);                               //         int 13: int 4 => integer de float 3.25 = 3
    }
}