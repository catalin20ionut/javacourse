package C02___30_06_2022___First_steps.Homework_1;

// 1. Write a java program to compare two strings, ignoring case differences.
import java.util.Scanner;

public class Exercise_C1_C2___with_Input {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);

        System.out.print("Name of the first book: ");
        String bookName1 = input.nextLine();

        System.out.print("Name of the second book: ");
        String bookName2 = input.nextLine();

        boolean nameComparison = bookName1.equalsIgnoreCase(bookName2);
        System.out.println(nameComparison);

        // 2. Write a Java program to concatenate a given string to the end of another string.
        System.out.println(bookName1.concat(" or " + bookName2 + "?"));
        System.out.println("\"" + bookName1.concat("\" or \"" + bookName2 + "\"?"));
    }
}