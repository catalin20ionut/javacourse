package C02___30_06_2022___First_steps.Homework_1;

// 2. Write a Java program to concatenate a given string to the end of another string.
public class Exercise_C2___concat {
    public static void main(String[] args) {
        String question = "Where are my";
        String word = "keys?";

        System.out.println(question.concat(" " + word));
    }
}