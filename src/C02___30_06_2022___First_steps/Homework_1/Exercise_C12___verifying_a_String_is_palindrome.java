package C02___30_06_2022___First_steps.Homework_1;

// 12. Verify if a strings is palindrome. You can also see the package "Palindrome_Anagrams"
public class Exercise_C12___verifying_a_String_is_palindrome {
    public static void verifyingMethod(String anyString){
        StringBuilder reversedString = new StringBuilder();

        int strLength = anyString.length();
        for (int i = (strLength - 1); i >= 0; i--) {
            reversedString.append(anyString.charAt(i));}

        if (anyString.equalsIgnoreCase(reversedString.toString())) {
            System.out.println(anyString + " is a palindrome.");}
        else {
            System.out.println(anyString + " isn't a palindrome.");}
    }

    public static void main(String[] args) {
        verifyingMethod("Radar");
        verifyingMethod("Hanna");
    }
}