package C02___30_06_2022___First_steps.Homework_1;

/**
   2. Write Java code to define two variables of type double. The value of these variables is hardcoded, the first is 5
and the second is 7. And the program must display the following lines on the screen.
                                                                         First value = <value stored in first variable>
                                                                       Second value = <value stored in second variable>
                                                                   Multiplication = <result of multiplying both values>
                                                                   Division = <result of dividing both values>       */
public class Exercise_2 {
    public static void main(String[] args) {
        double myDouble1 = 2345.12345;
        double myDouble2 = 25.1234567;
        System.out.println("First value = " + myDouble1);
        System.out.println("Second value = " + myDouble2);
        System.out.println("Multiplication = " + myDouble1 * myDouble2);
        System.out.println("Division = " + myDouble1 / myDouble2);
    }
}