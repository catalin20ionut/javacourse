package C02___30_06_2022___First_steps.Homework_1;

/**
4. Please, declare three variables of String type and assign it values. Then declare a fourth variable of type String
which will be a concatenation of previously declared variables and display its value on the screen. Please do it in two
ways:
a) Using ‘+’ operator
b) Using concat() method from String class (you will need to use it similar with length() that we used.              */

public class Exercise_B4___concat {
    public static void main(String[] args) {
        String myString1;
        myString1 = "This is";
        String myString2;
        myString2 = "an";
        String myString3;
        myString3 = "example.";

        String myString4;
        myString4 = myString1 + " " + myString2 + " " + myString3 ;
        System.out.println("a1) --> " + myString4);

        System.out.println("b1) --> " + myString1.concat(myString2.concat(myString3)));
        System.out.println("b2) --> " + myString1.concat(" " + myString2).concat(" " + myString3));
        System.out.println("b3) --> " + myString1.concat(" an").concat(" example."));
    }
}