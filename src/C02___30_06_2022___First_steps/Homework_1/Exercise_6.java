package C02___30_06_2022___First_steps.Homework_1;

/**
  6. Write equivalent statements using combined assignment (use the operators list and take into consideration that for
example z = z + 1 is equivalent to z += 1) for the following, if possible. .................. ▼ Equivalent statements ▼
a) x = 2 * x; ............................................................................................... x *= 2; ◄
b) x = x + y - 2; ....................................................................................... x += y - 2; ◄
c) sum = sum + num; ..................................................................................... sum += num; ◄
d) y = y / (x + 5); ................................................................................... y /= (x + 5); ◄
*/

public class Exercise_6 {
    public static void main(String[] args) {
        int x = 5;
        x *= 2;
        System.out.println(x);

        int y = 50;
        x += y - 2;
        System.out.println(x);                                                                 // x is 10 + 50 - 2 = 58

        y /= (x + 5);
        System.out.println(y);                                                               //       y = 50 : (58 + 5)
    }
}