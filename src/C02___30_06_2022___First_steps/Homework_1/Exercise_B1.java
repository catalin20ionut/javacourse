package C02___30_06_2022___First_steps.Homework_1;

/**
1. Basing on the example below, please implement the program, where:
a) Declare and initialize three variables of type int : a, b, c (initialize it to any values)
b) Store the result of a - b - c operation in the variable result1 and then display it on the standard output (screen)
c) Declare and initialize three variables of type long: d, e, f (initialize it to any values)
d) Store the result of the d * e / f operation in the variable result2 and then display it on the standard output
(screen) */

public class Exercise_B1 {
    public static void main(String[] args) {
        // a)
        int a;
        int b;
        int c;
        a = 12;
        b = 37;
        c = 23;

        // b)
        int result1 = a - b - c;
        System.out.println(result1);

        // c)
        long d;
        long e;
        long f;
        d = 9223372036854775807L;                                             // maximum value for a long type variable
        e = -9223372036854775808L;                                           //  minimum value for a long type variable
        f = 2147483648L;                                             // -2,147,483,648 to 2,147,483,647 is for integers

        // d)
        double result2 = (double) d * e / f;
        System.out.println("When the result is an double: " + result2);

        // comparison between double and float (for me)
        float result3 = (float) d * e / f;
        System.out.println("When the result is a float  : " + result3);
    }
}