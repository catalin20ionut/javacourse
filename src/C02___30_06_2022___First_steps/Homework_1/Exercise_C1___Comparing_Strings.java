package C02___30_06_2022___First_steps.Homework_1;

// 1. Write a java program to compare two strings, ignoring case differences.
public class Exercise_C1___Comparing_Strings {
    public static void main(String[] args) {
        String bookName1 = "the adventures of huckleberry finn";
        String bookName2 = "The Adventures of Huckleberry Finn";
        boolean nameComparison = bookName1.equalsIgnoreCase(bookName2);

        String modelKnown = "Rolls-Royce";
        String modelToBeInserted = "rolls-ROYcEDDD";
        boolean modelComparison = modelToBeInserted.equalsIgnoreCase(modelKnown);

        System.out.println("1. " + nameComparison);
        System.out.println("2. " + modelComparison);
    }
}