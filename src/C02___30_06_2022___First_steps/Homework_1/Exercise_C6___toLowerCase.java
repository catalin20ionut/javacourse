package C02___30_06_2022___First_steps.Homework_1;

// 6. Write a Java program to convert all the characters in a string to lowercase.
public class Exercise_C6___toLowerCase {
    public static void main(String[] args) {
        String bookName = "The Adventures of Huckleberry Finn";
        System.out.println(bookName.toLowerCase());
    }
}

/* Python =>
bookName = "The Adventures of Huckleberry Finn"
print(bookName.lower())
*/
