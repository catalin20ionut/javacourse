package C02___30_06_2022___First_steps.Homework_1;

// 7. Write a Java program to reverse a string.
public class Exercise_C7___reverse {
    public static String reverse(String str) {
        return new StringBuilder(str).reverse().toString();
    }

    public static void main(String[] args) {
        String bookName = "The Adventures of Huckleberry Finn";
        String finalString = reverse(bookName);
        System.out.println(finalString);
    }
}
/*
Python => print(book_name[::-1])

or

reversedString = ""
index = len(book_name)
while index > 0:
    reversedString += book_name[index - 1]
    index = index - 1
print(reversedString)
 */