package C02___30_06_2022___First_steps.Homework_1;

// 7. Write a Java program to reverse a string.
public class Exercise_C7_variant_2 {
    public static void main(String[] args) {
        // it creates a StringBuilder object with a String as parameter
        StringBuilder str = new StringBuilder("The Adventures of Huckleberry Finn");

        // it prints the initial string
        System.out.println("String         = " + str);

        // it reverses the string
        StringBuilder reverseStr = str.reverse();

        // it prints the reversed string
        System.out.println("Reverse String = " + reverseStr);
    }
}