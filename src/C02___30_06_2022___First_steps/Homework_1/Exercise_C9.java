package C02___30_06_2022___First_steps.Homework_1;

// 9. Write a Java program to convert integer to string.
public class Exercise_C9 {
    public static void main(String[] args) {
        int number = 2;
        String numberAsString1 = Integer.toString(number);
        String numberAsString2 = String.valueOf(number);
        String numberAsString3 = String.format("%d", number);
        System.out.println(numberAsString1);
        System.out.println(numberAsString2);
        System.out.println(numberAsString3);     // https://docs.oracle.com/javase/tutorial/java/data/numberformat.html
    }
}