package C02___30_06_2022___First_steps.Homework_1;

/**
    1. Write Java code to define two integer variables, name them how you want and also give them values. Calculate the
sum and print it in the console. If the number are for example 3 and 5, than in the console you will have the following
message: “Sum between 3 and 5 is 8”. In the same way, please calculate the difference between numbers, multiply the num-
bers, divide the numbers and the modulo operation. For all the operation please add a corresponding message in console.

From the trainer >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 --- cum poți să afișezi suma, diferența etc. și fără să declari o altă variabilă în care să ții valoarea rezultatului?
 --- la division nu trebuie să avem un rezultat de tipul float, ce s-ar întâmpla dacă lăsăm tot int? <<<<<<<<<<<<<<<<<<
*/

public class Exercise_1 {
    public static void main(String[] args) {
        int myVar1 = 23;
        int myVar2 = 5;

        int sumVar = myVar1 + myVar2;
        System.out.println("1a. Sum between " + myVar1 + " and " + myVar2 + " is " + sumVar +".");
        System.out.println("1b. Sum between " + myVar1 + " and " + myVar2 + " is " + (myVar1 + myVar2) +".");

        int difVar = myVar1 - myVar2;
        System.out.println("2a. Difference between " + myVar1 + " and " + myVar2 + " is " + difVar +".");
        System.out.println("2b. Difference between " + myVar1 + " and " + myVar2 + " is " + (myVar1 - myVar2) +".");

        int multiVar = myVar1 * myVar2;
        System.out.println("3a. Multiplication between " + myVar1 + " and " + myVar2 + " is " + multiVar +".");
        System.out.println("3b. Multiplication between " + myVar1 + " and " + myVar2 + " is " + (myVar1 * myVar2) +".");

        float divisionVar = (float) myVar1 / (float) myVar2;
        System.out.println("4aa. Division between " + myVar1 + " and " + myVar2 + " is " + divisionVar +".");
        System.out.println("4ab. Division between " + myVar1 + " and " + myVar2 + " is " + (myVar1 / myVar2) + ".");
        System.out.println("4ac. Division between " + myVar1 + " and " + myVar2 + " is "
                                                                               + (float) myVar1 / (float) myVar2 +".");

        /* or
        float myFloat1 = myVar1;
        float myFloat2 = myVar2;
        float divisionVar2 = myFloat1 / myFloat2;
        System.out.println("4b. Division between " + myVar1 + " and " + myVar2 + " is " + divisionVar2 +".");

        int divisionVar3 = myVar1 / myVar2;
        System.out.println("4c. Division between " + myVar1 + " and " + myVar2 + " is " + divisionVar3 +".");

        float myVar3 = 3f;
        float myVar4 = 5f;
        float divisionVar4 = myVar3 / myVar4;
        System.out.println("4d. Division between " + myVar3 + " and " + myVar4 + " is " + divisionVar4 +".");

        float divisionVar5 = (myVar1 / myVar2);
        System.out.println("4e. Division between " + myVar1 + " and " + myVar2 + " is " + divisionVar5 +".");
        */

        int moduloVar = myVar1 % myVar2;
        System.out.println("5a. Modulus between " + myVar1 + " and " + myVar2 + " is " + moduloVar +".");
        System.out.println("5b. Modulus between " + myVar1 + " and " + myVar2 + " is " + myVar1 % myVar2 +".");
    }
}