package C02___30_06_2022___First_steps.Homework_1;

/**
3. Implement the program, where you have to:
a) Declare and initialize two variables: intVar1, intVar2 of int type
b) Declare variable shortSum of short type and initialize it as the sum of previously declared variables
(intVar1 + intVar2)
c) Display on the screen the value stored in shortSum variable
d) Next, display on the screen the result of the incrementation: shortSum ++
e) Declare variable byteSum of byte type and initialize it as the sum of previously declared variables
(intVar1 + intVar2)
f) Display on the screen the value stored in byteSum variable
g) Next, display on the screen the result of the incrementation: ++ byteSum
*/
public class Exercise_B3 {
    public static void main(String[] args) {
        // a)
        int intVar1;
        int intVar2;
        intVar1 = 50;
        intVar2 = 23;

        // b)
        short shortSum;
        shortSum = (short) (intVar1 + intVar2);

        // c)
        System.out.println("Print 1: " + shortSum);

        // d)
        shortSum += 1;  // sau
        // shortSum = (short) (shortSum + 1);
        System.out.println("Print 2: " + shortSum);

        // e)
        byte byteSum;
        byteSum = (byte) (intVar1 + intVar2);

        // f)
        System.out.println("Print 3: " + byteSum);

        // g)
        byteSum += 1;
        System.out.println("Print 4: " + byteSum);
    }
}