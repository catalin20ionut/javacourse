package C02___30_06_2022___First_steps.Homework_1;

// 3. Write a java program to get the length of a given string
public class Exercise_C3___length {
    public static void main(String[] args) {
        String bookName = "The Adventures of Huckleberry Finn";
        System.out.println(bookName.length());
    }
}
// Python => print(f"The length is: {len(bookName)}")