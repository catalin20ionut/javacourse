package C02___30_06_2022___First_steps.Homework_1;

/**
4. Please try to run in a Java program the following lines and print in console the values for every variable:
int a, b, c;
double x, y;
a = 17;
b = 15;
a = a + b / 4;
c = a % 3 + 4;
x = 17 / 3 + 6.5;
y = a / 4.0 + 15 % 4 - 3.5;
*/

public class Exercise_4 {
    public static void main(String[] args) {
        int a, b, c;
        double x, y;
        a = 17;
        b = 15;
        c = a % 3 + 4;
        x = 17 / 3 + 6.5;
        y = a / 4.0 + 15 % 4 - 3.5; // 17/4 + 15 % 4 - 3.5 = 4.25 + 3 -3.5
        System.out.println("a = " + a);

        a = a + b / 4;
        System.out.println("a = " + a + "   <<< After the second assignment statement is made.");
        System.out.println("b = " + b);
        System.out.println("c = " + c + "    <<< a has the value 20.");                       // 20 % 3 + 4 = 2 + 4 = 6
        System.out.println("x = " + x);                                                      //  int 17 : int 3 = int 5
        System.out.println("y = " + y + " <<< a has the value 17 because y is before \" a = a + b / 4 \".");
    }
}