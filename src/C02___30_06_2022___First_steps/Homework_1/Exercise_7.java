package C02___30_06_2022___First_steps.Homework_1;

import java.util.Scanner;
import java.lang.Math;

/**
7. Write a program that inputs from the user the radius of a circle as an integer and prints the circle’s circumference
and area using the predefined variable called pi with value 3.14.         Use the following formulas (r is the radius):
circumference = 2*pi*r  and area = pi*r2                                                                             */

public class Exercise_7 {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        System.out.print("Input the radius: ");
        int r = input.nextInt();

        float pi = 3.14f;
        float circumference = 2 * pi * r;

        float area = pi * r * r;    // or ▼
        double area2 = pi * (Math.pow(r, 2));
        System.out.println("1a. Circumference of the circle with radius " + r + " is " + circumference + ".");
        System.out.println("2a. Area of the circle with radius " + r + " is " + area + ".");
        System.out.println("2b. Area of the circle with radius " + r + " is " + area2 + ".");
        System.out.println("2c. Area of the circle with radius " + r + " is " + pi * (Math.pow(r, 2)) + ".");

        /* Dacă folosim
        float area = pi * (float) (Math.pow(r, 2));
        Vom avea ca rezultat numai 4 cifre după punct --- pentru r = 25 vom avea 1962.5001 în loc de 1962.5000655651093
        pentru că "Math.pow(r, 2)" returnează un double.
         */
        float area3 = pi * (float) (Math.pow(r, 2));
        System.out.println("2d. Area of the circle with radius " + r + " is " + area3 +".");
    }
}