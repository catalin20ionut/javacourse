package C02___30_06_2022___First_steps.Homework_1;

// 10.Write a Java program to convert string to integer.
public class Exercise_C10 {
    public static void main(String[] args) {
        String numberAsString = "2";
        int number = Integer.parseInt(numberAsString);
        System.out.println(number);
    }
}