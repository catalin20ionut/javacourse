package C02___30_06_2022___First_steps.Homework_1;

/**       5. Suppose x, y, and sum are int variables and z is a double variable. What value is assigned to sum variable
after statement executes? Suppose x = 3, y = 5, and z = 14.1 =>  sum = x + y + (int) z;                              */
public class Exercise_5 {
    public static void main(String[] args) {
        int x = 3;
        int y = 5;
        double z = 14.1;

        int sum = x + y + (int) z;
        System.out.println("1. If we declare sum as an integer: " + sum);

        double sum2 = x + y + z;
        System.out.println("2. If we declare sum as a double  : " + sum2);
    }
}