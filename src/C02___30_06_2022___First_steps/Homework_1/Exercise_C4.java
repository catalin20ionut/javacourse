package C02___30_06_2022___First_steps.Homework_1;

// 4. Write a Java program to get a substring of a given string between two specified positions
public class Exercise_C4 {
    public static void main(String[] args) {
        String bookName = "The Adventures of Huckleberry Finn";
        System.out.println(bookName.substring(0, 18));
    }
}
/* Python
bookName = "The Adventures of Huckleberry Finn"
print(bookName[0:18])
*/