package C02___30_06_2022___First_steps.Homework_1;

/** 2. Display on the screen in the following lines the values of the logical expressions listed below:
2 * 2 >= 3 && 1 == 1
(12/4) != 3
12 % 4 != 0
3 != 4 || (2 + 3 > 5)
*/

// && - logical and
// || - logical or
public class Exercise_B2 {
    public static void main(String[] args) {
        boolean expression1 = 2 * 2 >= 3 && 1 == 1;
        System.out.println(expression1);
        System.out.println((12 / 4) != 3);
        System.out.println(12 % 4 != 0);
        System.out.println(3 != 4 || (2 + 3 > 5));
    }
}