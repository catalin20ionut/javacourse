package C02___30_06_2022___First_steps.Homework_1;

// 5. Write a Java program to convert all the characters in a string to uppercase.
public class Exercise_C5___toUpperCase {
    public static void main(String[] args) {
        String bookName = "The Adventures of Huckleberry Finn";
        System.out.println(bookName.toUpperCase());
    }
}
/* Python =>
bookName = "The Adventures of Huckleberry Finn"
print(bookName.upper())
*/