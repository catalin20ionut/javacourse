package C02___30_06_2022___First_steps;

public class StudentFacultate {
    boolean esteAngajat = true;
    boolean eBuget = false;
    char myChar = 'X';
    String nume = "Ana Popescu";
    String dadName = "I";
    byte[] nota;
    int age = 22;
    float mathMark = 9.5f;
    double notaRomana = 8.5;

    /** Exercise 1
    Define an integer variable called bankBalance. Initialize it to a value of 500.
    Then add 250 to it. Then subtract 100 from it. Finally, print the resulting value. */
    public static void main(String[] args) {
        int bankBalance = 500;
        System.out.println("Exercise 1 -> Result 1: " + bankBalance);
        bankBalance += 250;
        System.out.println("Exercise 1 -> Result 2: " + bankBalance);
        bankBalance -=150;
        System.out.println("Exercise 1 -> Result 3: " + bankBalance);


        /* Exercise 2
        Write Java code to define an integer variable called day and a String
        variable called month. Give month and day appropriate values for your birthday. */
        int day;
        String month;
        day = 12;
        month = "December";


        /* Exercise 3
        Write Java code to define a double variable called fahrenheit and set it to an initial value between 0 and 100.
        Then, create a double variable called celsius, and calculate its value based on the value of fahrenheit. To con-
        vert from Fahrenheit to Celsius, subtract 32, then multiply by 5, then divide by 9. Finally, print the final va-
        lue of celsius. */
        double fahrenheit = 60.0;
        double celcius;
        celcius = (fahrenheit - 32) * 5/9;
        System.out.println("Exercise 3 -> Celcius: " + celcius);


        /* Exercise 4
       Write Java code to create a String variable called firstName, define it to be your first name as a String. Then
       define a variable called lastName and define it to be your last name as a String. Then define a variable called
       fullName and set it to be your first name followed by a space followed by your last name. Use the existing varia-
       bles for your first and last name and String concatenation to define fullName. Finally, write code to print this
       text:
                 Hello, my name is [full name].
                 There are [number] letters in my name. */
        String firstName = "Grosu";
        String lastName = "Cătălin-Ionuț";
        String fullName = firstName + " " + lastName;
        System.out.println("Exercise 4 -> Hello, my name is " + fullName  + ".");
        System.out.println("Exercise 4 -> There are " + firstName.length() + " letters in my first name.");
    }
}