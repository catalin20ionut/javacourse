package C03___05_07_2022___Constructors.Ex_1___Burger;

/**
The purpose of the application is to help a company called Bills Burgers to manage their process of selling hamburgers.
Our application will help Bill to select types of burgers, some of additional items (additions) to be added to the
burgers and pricing. We want to create a base hamburger, but also two other types of hamburgers that are popular ones in
Bills store. The basic hamburger should have the following items.
- bread roll type, meat and up to 4 additional additions (things like lettuce, tomato, carrot etc.) that the customer
can select to be added to the burger. Each one of these items gets charged an additional price, so you need some way to
track how many items got added and to calculate the price (for the base burger and all the additions).
This burger has a base price and the additions are all separately priced.
Create a Hamburger class to deal with all the above.
The constructor should only include the roll type, meat and price.
Print the final price for the burger.
 */
public class Burger {

    // Step 1 --> we name the class or instance variables
    private String meat;
    private double price;
    private String rollType;

    private String extraIngredient1;
    private String extraIngredient2;
    private String extraIngredient3;
    private String extraIngredient4;
    private double extraPrice1;
    private double extraPrice2;
    private double extraPrice3;
    private double extraPrice4;


    // Step 2 --> basic Burger, constructor
    public Burger(String carne, double pret, String tipPaine) {
        System.out.println("For the standard Burger with " + tipPaine + " and "
                                                                              + carne + " the price is " + pret + ".");
        this.meat = carne;
        this.price = pret;
        this.rollType = tipPaine;
    }

    public void addExtraIngredient1 (String numeIngredient, double additionalPrice){
        this.extraIngredient1 = numeIngredient;
        this.extraPrice1 = additionalPrice;
    }

    public void addExtraIngredient2 (String numeIngredient, double additionalPrice){
        this.extraIngredient2 = numeIngredient;
        this.extraPrice2 = additionalPrice;
    }

    public void addExtraIngredient3 (String numeIngredient, double additionalPrice){
        this.extraIngredient3 = numeIngredient;
        this.extraPrice3 = additionalPrice;
    }

    public void addExtraIngredient4 (String numeIngredient, double additionalPrice){
        this.extraIngredient4 = numeIngredient;
        this.extraPrice4 = additionalPrice;
    }

    public void calculateFinalPrice () {
        double finalPrice = this.price;
        System.out.println("3. The final price is -- before adding ingredients: " + finalPrice);
        if (this.extraIngredient1 != null) {
            finalPrice += this.extraPrice1;
            System.out.println("4. I added the ingredient " + this.extraIngredient1
                    + " with the additional price " + this.extraPrice1);
            System.out.println("5. The final price is " + finalPrice);
        }
        if (this.extraIngredient2 != null) {
            finalPrice += this.extraPrice2 - this.extraPrice1;
            System.out.println("6. I added the ingredient " + this.extraIngredient2
                    + " with the additional price " + this.extraPrice2);
            System.out.println("7. The final price is " + finalPrice);
        }
        if (this.extraIngredient3 != null){
            finalPrice += this.extraPrice3;
            System.out.println("8. I added the ingredient " + this.extraIngredient3
                    + " with the additional price " + this.extraPrice3);
            System.out.println("9. The final price is " + finalPrice);
        }
        if (this.extraIngredient4 != null){
            finalPrice += this.extraPrice4;
            System.out.println("10. I added the ingredient " + this.extraIngredient4
                    + " with the additional price " + this.extraPrice4);
            System.out.println("11. The final price is " + finalPrice);
        }
    }
}