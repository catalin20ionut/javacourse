package C03___05_07_2022___Constructors.Ex_1___Burger;

public class Application {
    public static void main(String[] args) {
        Burger hamburger = new Burger( "pork",5.2, "chifla");
        System.out.println("--> " + hamburger);
        hamburger.calculateFinalPrice();
        hamburger.addExtraIngredient1("lettuce", 2.00);
        hamburger.calculateFinalPrice();
        hamburger.addExtraIngredient2("tomato", 3.00);
        hamburger.calculateFinalPrice();
        hamburger.addExtraIngredient3("ketchup", 1.5);
        hamburger.calculateFinalPrice();
        hamburger.addExtraIngredient4("mustard", 1.25);
        hamburger.calculateFinalPrice();
    }
}