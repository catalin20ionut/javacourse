package C03___05_07_2022___Constructors.Ex_2___Student;

public class Application {
    public static void main(String[] args) {
        Student studentMeu = new Student(); // new creez un obiect de tip student
        System.out.println(studentMeu);
        studentMeu.calculeazaPrietenii(6);

        System.out.println();
        Student myStudentAge = new Student(5, 7); // new creez un alt obiect de tip student
        System.out.println(myStudentAge);
        myStudentAge.calculeazaPrietenii(6);
    }
}