package C03___05_07_2022___Constructors.Ex_2___Student;

public class Student {
    public static int NUMBER = 100; // alocă din start memorie
    private String nume; // privat doar pentru student
    private int varsta;

    // Constructor
    public Student() {
        int a = 10;
        System.out.println("Sunt in constructor cu variabila " + a);
    }

    public Student(int age, int note) {
        age = 24;
        int a = 10;
        System.out.println("Sunt constructor age " + a);
    }

    public void calculeazaPrietenii(int cursuri){
        int nrDEPrieteni = cursuri * 2;
        System.out.println("Nr de prieteni " + nrDEPrieteni);
    }
}