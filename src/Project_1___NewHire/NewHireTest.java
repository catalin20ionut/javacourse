package Project_1___NewHire;


public class NewHireTest {
    public static void main(String[] args) {
        NewHire newHire = new NewHire("5040202573825");
        newHire.setFirstName("EMIl");
        newHire.setLastName("maRinesCu");
        newHire.displayFullName();
        newHire.setCompany("Google");
        newHire.setDepartment("sales");
        newHire.setEmail();
        newHire.displayEmail();
        newHire.displayDepartament();
        System.out.println(newHire.getPhoneNumber());
        System.out.println(newHire.getSerialNumber());
        newHire.generatePassword();
        System.out.println(newHire.getPassword());
        newHire.changePassword("qwerty25iophsdgf");
        newHire.displayMailBoxCapacity();
        newHire.setMailBoxCapacity(550);
        newHire.getMailBoxCapacity();
        newHire.setAlternativeEmail("emilmarinescu@gmail.com");
        newHire.displayAlternativeEmail();

        NewHire newHire2 = new NewHire("2940214573825");
        newHire2.setFirstName("andreea");
        newHire2.setLastName("popovici");
        newHire2.displayFullName();
        newHire2.setCompany("Saudi AraMCO");
        newHire2.setDepartment("accounting");
        newHire2.setEmail();
        newHire2.displayEmail();
        newHire2.displayDepartament();
        System.out.println(newHire2.getPhoneNumber());
        System.out.println(newHire2.getSerialNumber());
        newHire2.generatePassword();
        System.out.println(newHire2.getPassword());
        newHire2.changePassword("qwerty2iodg4f");
        newHire2.displayMailBoxCapacity();
        newHire2.setMailBoxCapacity(550);
        newHire2.getMailBoxCapacity();
        newHire2.setAlternativeEmail("andreea20p@gmail.com");
        newHire2.displayAlternativeEmail();

        NewHire newHire3 = new NewHire("6010214573825");
        newHire3.setFirstName("");
        newHire3.setLastName("munteaNU");
        newHire3.displayFullName();
        newHire3.setCompany("IBM");
        newHire3.setDepartment("development");
        newHire3.setEmail();
        newHire3.displayEmail();
        newHire3.displayDepartament();
        System.out.println(newHire3.getPhoneNumber());
        System.out.println(newHire3.getSerialNumber());
        newHire3.generatePassword();
        System.out.println(newHire3.getPassword());
        newHire3.changePassword("qwert34bskjd");
        newHire3.displayMailBoxCapacity();
        newHire3.setMailBoxCapacity(550);
        newHire3.getMailBoxCapacity();
        newHire3.setAlternativeEmail("silviamunteanu@gmail.com");
        newHire3.displayAlternativeEmail();

        NewHire newHire4 = new NewHire("1990814573628");
        newHire4.setFirstName("George");
        newHire4.setLastName("Petrescu");
        newHire4.displayFullName();
        newHire4.setCompany("IBM");
        newHire4.setDepartment("");
        newHire4.setEmail();
        newHire4.displayEmail();
        newHire4.displayDepartament();
        System.out.println(newHire4.getPhoneNumber());
        System.out.println(newHire4.getSerialNumber());
        newHire4.generatePassword();
        System.out.println(newHire4.getPassword());
        newHire4.changePassword("afasft3afasft35");
        newHire4.displayMailBoxCapacity();
        newHire4.setMailBoxCapacity(550);
        newHire4.getMailBoxCapacity();
        newHire4.setAlternativeEmail("p22george@gmail.com");
        newHire4.displayAlternativeEmail();
    }
}
