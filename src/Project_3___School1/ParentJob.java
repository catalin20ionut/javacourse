package Project_3___School1;

public enum ParentJob {
    DOCTOR("Medic"),
    DENTIST("Dentist"),
    MAILMAN("Postas"),
    TEACHER("Profesor"),
    JOURNALIST("Jurnalist"),
    WRITTER("Scriitor"),
    ACCOUNTANT("Contabil"),
    ADMINISTRATOR("Administrator");
    String description;
    ParentJob(String description){
        this.description= description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
