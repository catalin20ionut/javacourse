package Project_3___School1;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Pupil extends User {
    private char gender;
    private int age;
    private String birthDate;
    private String address;
    private boolean hasScholarship;
    private Classes classes;
    private List<Disciplines> disciplinesList = new ArrayList<>();
    private List<TeachingSubjects> teachingSubjects = new ArrayList<>();
    private List<Integer> markList = new ArrayList<>();
    private Disciplines discipline;
    private PupilSchoolType pupilSchoolType;

    public Pupil(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
    }

    public Disciplines getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Disciplines discipline) {
        this.discipline = discipline;
    }

    public PupilSchoolType getPupilSchoolType() {
        return pupilSchoolType;
    }

    public void setPupilSchoolType(PupilSchoolType pupilSchoolType) {
        this.pupilSchoolType = pupilSchoolType;
    }

    @Override
    public String getClassName() {
        return "Pupil";
    }

    @Override
    protected boolean checkPassword(User user) {
        if (getPassword() != null) {
            return true;
        }
        return false;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean HasScholarship() {
        return hasScholarship;
    }

    public void setHasScholarship(boolean hasScholarship) {
        this.hasScholarship = hasScholarship;
    }

    public boolean verifyingBirthDate(String birthDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        return sdf.parse(birthDate, new ParsePosition(0)) != null;
    }

    public List<Disciplines> getDisciplinesList() {
        return disciplinesList;
    }

    public void setDisciplinesList(List<Disciplines> disciplinesList) {
        this.disciplinesList = disciplinesList;
    }

    public List<Integer> getMarkList() {
        return markList;
    }

    public void setMarkList(List<Integer> markList) {
        this.markList = markList;
    }

    public List<TeachingSubjects> getTeachingSubjects() {
        return teachingSubjects;
    }

    public void setTeachingSubjects(List<TeachingSubjects> teachingSubjects) {
        this.teachingSubjects = teachingSubjects;
    }

    public int getAverageDiscipline() {
        return makeAverageDiscipline(markList, discipline);
    }

    public void addDisciplines(Disciplines discipline) {
        disciplinesList.add(discipline);
    }

    public int makeAverageDiscipline(List<Integer> markList, Disciplines disciplines) {
        double sum = 0;
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWORD IS NULL");
        } else {
            if (disciplinesList.contains(disciplines)) {
                for (int i = 0; i < markList.size(); i++) {
                    sum += markList.get(i);
                }
            }
        }
        int average = (int) (sum / markList.size());
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\ListAvg\\ListAverage.txt");
            file.createNewFile();
            FileWriter pw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(pw);
            bf.write(disciplines.getTeachingSubjects().toString() + " has the average : " + average);
            bf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return average;
    }

    public Double makeAverageList(List<Disciplines> disciplinesList) {
        int sum = 0;
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWORD IS NULL");
        } else {
            List<Integer> averageList = new ArrayList<>();

            for (Integer avg : averageList) {
                sum += avg;
            }
        }

        return (double) sum / disciplinesList.size();
    }

    public double makeAnualAverageForDisciplines(List<Double> average, List<Disciplines> list) {
        double sum = 0.00;
        if (this.getPassword() == null) {

            throw new RuntimeException("PASSWORD IS NULL");
        } else {
            for (int i = 0; i < markList.size(); i++) {
                sum += markList.get(i);
            }
        }

        return sum / markList.size();
    }

    public void uploadHomework(Pupil pupil, Disciplines discipline, String homework) {
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWORD IS NULL");
        } else {
            for (Disciplines currentDiscipline : disciplinesList) {
                if (currentDiscipline.getTeachingSubjects().equals(discipline)) {
                    currentDiscipline.getHomeworkList().add(homework);
                }
            }
        }
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\Homework\\" +
                    pupil.getFullName() + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter pw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(pw);
            bf.write(homework);
            bf.close();
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }

    public boolean hasDiscipline(Disciplines discipline) {
        for (Disciplines currentDiscipline : disciplinesList) {
            if (currentDiscipline.getTeachingSubjects().equals(discipline)) {
                return true;
            }
        }
        return false;
    }

}





