package Project_3___School1;

import java.util.ArrayList;
import java.util.List;

public class Disciplines {
    private String name;
    private TeachingSubjects teachingSubjects;
    private List<TeachingSubjects> teachingSubjectsList = new ArrayList<>();
    private List<Integer> marksList = new ArrayList<>();
    private List<String> homeworkList = new ArrayList<>();

    public Disciplines(TeachingSubjects teachingSubjects) {
        this.teachingSubjects = teachingSubjects;

    }

    public TeachingSubjects getTeachingSubjects() {
        return teachingSubjects;
    }

    public void setTeachingSubjects(TeachingSubjects teachingSubjects) {
        this.teachingSubjects = teachingSubjects;
    }

    public List<TeachingSubjects> getTeachingSubjectsList() {
        return teachingSubjectsList;
    }

    public void setTeachingSubjectsList(List<TeachingSubjects> teachingSubjectsList) {
        this.teachingSubjectsList = teachingSubjectsList;
    }


    public List<Integer> getMarksList() {
        return marksList;
    }

    public void setMarksList(List<Integer> marksList) {
        this.marksList = marksList;
    }

    public List<String> getHomeworkList() {
        return homeworkList;
    }

    public void setHomeworkList(List<String> homeworkList) {
        this.homeworkList = homeworkList;
    }

    @Override
    public String toString() {
        return teachingSubjects.toString();


    }
}
