package Project_3___School1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Random;

public abstract class User {
    private String userName;
    private String email;
    private String firstName;
    private String lastName;

    private String password;


    public User(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = getEmail();
        this.userName = generatingUserNameUser();
        if (this.email.contains("@gmail.com")) {
            System.out.println("The email is valid.");
        } else {
            throw new RuntimeException("Another type of email cannot be created!");
        }
    }
    public String getEmail() {
        return firstName.toLowerCase() + "-" + lastName.toLowerCase() + "@gmail.com";
    }
    public String getPassword() {
        return password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }



    public String getFirstNameUser() {
        return firstName;
    }

    public String getLastNameUser() {
        return lastName;
    }

    public String generatingUserNameUser() {
        Date date = new Date();
        Long unic = date.getTime();
        return this.getFirstNameUser().toLowerCase() + this.getLastNameUser().toUpperCase() + unic;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public abstract String getClassName();

    @Override
    public String toString() {
        return "Full name: " + getFullName();
    }

    protected abstract boolean checkPassword(User user);
}