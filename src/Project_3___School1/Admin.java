package Project_3___School1;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Admin extends User {

    public Admin(String firstNameAdmin, String lastNameAdmin, String emailAdmin) {
        super(firstNameAdmin, lastNameAdmin, emailAdmin);
        // this.password = generatingPasswordForAdmin();
    }

    @Override
    public String getClassName() {
        return "Admin";
    }

    public String generatingPasswordForAdmin() {
        Random random = new Random();
        String letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+_-*$#@";
        int passwordLength = random.nextInt(6) + 6;
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < passwordLength; i++) {
            int randomLetter = random.nextInt(letters.length());
            password.append(letters.charAt(randomLetter));
        }
        return password.toString();
    }

    public String generatingPasswordForUser(User user) {
        Random random = new Random();
        String digits = "0123456789";
        String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" + "!@#$%^&*()";
        int passwordLength = random.nextInt(6) + 6;
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < passwordLength; i++) {
            int digitOrLetter = random.nextInt(2);
            int randomDigit = random.nextInt(digits.length());
            int randomLetter = random.nextInt(letters.length());

            if (digitOrLetter == 0) {
                password.append(digits.charAt(randomDigit));
            } else {
                password.append(letters.charAt(randomLetter));
            }

        }
        if (user.getPassword() == null) {
            user.setPassword(password.toString());
        }
        return password.toString();
    }

    public String resettingPassword(User user) { //ok
        String newPassword = new String();
        if (getPassword() == null) {
            System.out.println("No password is generated.");
        } else {
            user.setPassword(newPassword);
        }
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\Users\\"
                    + user.getFullName() + "txt");
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(fw);
            if (file.exists()) {
                bf.write("Username is : " + user.getFullName());
                bf.write("\nNew password is :" + newPassword);
            }
            bf.close();
            deleteFile(user);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        this.setPassword(newPassword);
        return newPassword;
    }

    public void writingUser(User user) { //ok
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\Users\\" +
                    user.getFullName() + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(fw);
            bf.write("Username is: " + user.getUserName() + "\n");
            bf.write("Password is  : " + generatingPasswordForUser(user));

            bf.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void writingAdministrator() { //ok
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\Users\\FileAdministrator.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(fw);
            bf.write(generatingPasswordForAdmin());
            bf.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public boolean deletingUser(User user, List<User> schoolMembers) { //ok nu o folosiim inca
        boolean flag = false;
        if (schoolMembers.contains(user)) {
            schoolMembers.remove(user);
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    public void deleteFile(User user) { // ok
        File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\Users\\" +
                user.getFullName() + ".txt");
        if (file.delete()) {

            System.out.println("We deleted the file.");
        } else {
            System.out.println("Failed to delete the file.");
        }


    }

    public void createMembersFolder(School school) { // ok

        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\Project_3___School1\\SchoolMembers\\List.txt");
            file.createNewFile();
            FileWriter pw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(pw);
            StringBuilder text = new StringBuilder();
            int counter = 0;
            for (int i = 1; i <= school.getSchoolMembers().size(); i++) {
                counter++;
                text.append("Nr. " + counter + " " + school.getSchoolMembers().get(i - 1).getFullName() +
                        " " + school.getSchoolMembers().get(i - 1).getClassName() + " " + "\n");
            }
            bf.write(text.toString());
            bf.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean checkPassword(User user) { // nu merge
        if (this.generatingPasswordForUser(user) != null) {
            System.out.println("Password is generated.");
            return true;
        } else {
            throw new RuntimeException("User has no password");

        }
    }

}
