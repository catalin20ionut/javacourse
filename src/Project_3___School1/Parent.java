package Project_3___School1;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Parent extends User {
    private String numberPhone;
    private String address;
    private int ageOfTheParent;
    private String birthDate;
    private String job;
    private List<Pupil> kids = new ArrayList<>();
    private ParentJob parentJob;

    private List<Integer> note = new ArrayList<>();

    private Set<Teacher> teacherSet = new HashSet<>();

    public Parent(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
    }

    @Override
    public String getClassName() {
        return "Parent";
    }

    @Override
    protected boolean checkPassword(User user) {
        if (getPassword() != null) {
            return true;
        }
        return false;
    }


    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAgeOfTheParent() {
        return ageOfTheParent;
    }

    public void setAgeOfTheParent(int ageOfTheParent) {
        this.ageOfTheParent = ageOfTheParent;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setKids(List<Pupil> kids) {
        this.kids = kids;
    }
    // trebuie apelată din main

    public List<Pupil> getKids() {
        return kids;
    }

    public ParentJob getParentJob() {
        return parentJob;
    }

    public void setParentJob(ParentJob parentJob) {
        this.parentJob = parentJob;
    }

    public void addKidsForParentUser(Pupil pupil) {
        if (this.getPassword() == null) {
            throw new RuntimeException("PASSWROD IS NULL");
        } else {
            if (kids.contains(pupil)) {
                throw new RuntimeException("The user has already been registered.");
            } else {
                System.out.println("The user " + pupil.getUserName() + " is added to the school list.");
                kids.add(pupil);
            }
        }

    }

    public void showJob() {
        switch (parentJob) {
            case DOCTOR:
                System.out.println("Job is :" + ParentJob.DOCTOR);
                break;
            case DENTIST:
                System.out.println("Job is :" + ParentJob.DENTIST);
                break;
            case MAILMAN:
                System.out.println("job is :" + ParentJob.MAILMAN);
                break;
            case TEACHER:
                System.out.println("Job is :" + ParentJob.TEACHER);
                break;
            case WRITTER:
                System.out.println("Job is :" + ParentJob.WRITTER);
                break;
            case ACCOUNTANT:
                System.out.println("Job is:" + ParentJob.ACCOUNTANT);
                break;
            case JOURNALIST:
                System.out.println("Job is:" + ParentJob.JOURNALIST);
                break;
            case ADMINISTRATOR:
                System.out.println("Job is :" + ParentJob.ADMINISTRATOR);
                break;
            default:
                System.out.println("Please choose one of the jobs from the list.");
                break;
        }
    }
}
/*CLASS PARINTE
-lista de elevi (daca are mai mulati copii-in main declari elevi
 si asignam int-o lista)
-ocupatia
-pe baza parolei poate sa citesca mediile facem media pe baza mediilor
-refolosim medodele de la medii din elev
-avem o metoda cre returneaza mediile copiilor

-generare un fisier cu profesorii care preda la clasele copiilor lor
-facem un set de profesori--
-facem un stringbuilder si il facem in fisier sub src si le puneem pe toate

 */