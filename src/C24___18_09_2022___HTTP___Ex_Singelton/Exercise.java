package C24___18_09_2022___HTTP___Ex_Singelton;

public class Exercise {
    public static boolean isConsecutiveFour(int [] values) {
        int counter = 1;
        for (int i = 0; i < values.length-1; i++) {
            if (values[i] == values[i + 1]) {
                counter++;}
            if (counter >= 4) {
                return true;}
            if (values[i] != values[i + 1])
                counter = 1;}
        return false;
    }

    public static void main(String[] args) {
        int [] myVariable = {3, 4, 3, 4, 3, 33, 33, 33, 33};
        System.out.println(isConsecutiveFour(myVariable));
    }
}

