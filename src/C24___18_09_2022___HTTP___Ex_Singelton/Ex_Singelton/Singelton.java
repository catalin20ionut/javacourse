package C24___18_09_2022___HTTP___Ex_Singelton.Ex_Singelton;

public class Singelton {
    private static final Singelton first = new Singelton(1);
    private static final Singelton second = new Singelton(2);
    private final int id;
    private static boolean isEven = true;

    private Singelton (int id){
        this.id = id;
    }
    public static Singelton getInstance(){
        if(isEven){
            isEven = false;
            return second;
        }
        isEven = true;
        return first;
    }

    public int getId() {
        return id;
    }
}
