package C24___18_09_2022___HTTP___Ex_Singelton.Ex_Singelton;

public class Main {
    public static void main(String[] args) {
/*
Implement the singleton pattern with a twist. First, instead of storing one instance, store two instances. And in every
even call of getInstance(), return the first instance and in every odd call of getInstance(), return the second instance.
*/
        Singelton instance = Singelton.getInstance();
        System.out.println(Singelton.getInstance().getId());
        System.out.println(Singelton.getInstance().getId());
        System.out.println(Singelton.getInstance().getId());
        System.out.println(Singelton.getInstance().getId());

    }
}
