package C11___04_08_2022.Example_2;

public class TestStock {
    public static void main(String[] args) {
        Stock stock1 = new Stock("Ok", "Coke" );
        stock1.getChangePercent(34.5, 34.35);
        System.out.println("1: " + stock1.getChangePercent(34.5, 34.35));

        System.out.println("2: " + stock1.getCurrentPrice());

        stock1.setCurrentPrice(34.35);
        stock1.setPreviousClosingPrice(34.5);
        System.out.println("3: " + stock1.getChangePercent1());

        System.out.println("4: " + stock1.getCurrentPrice());

        System.out.println("5: " + stock1.getSomething());
        stock1.setSomething("Oana");
        System.out.println("6: " + stock1.getSomething());

    }

}
