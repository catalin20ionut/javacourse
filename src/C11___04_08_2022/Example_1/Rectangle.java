package C11___04_08_2022.Example_1;

/**
1. The Rectangle class ------------------- Create a class named Rectangle to represent a rectangle. The class contains:
■ Two double data fields named width and height that specify the width and height of the rectangle. The default values
are 1 for both width and height.
■ A no-arg constructor that creates a default rectangle.
■ A constructor that creates a rectangle with the specified width and height.
■ A method named getArea() that returns the area of this rectangle.
■ A method named getPerimeter() that returns the perimeter.
 */
public class Rectangle {
    private double width;
    private double height;

    public Rectangle() {
        this.width = 1.0;
        this.height = 1.0;
    }

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getArea () {
        return this.width * this.height;
    }

    public double getPerimeter () {
        return 2 * (this.width + this.height);
    }
}
