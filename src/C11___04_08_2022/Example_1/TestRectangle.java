package C11___04_08_2022.Example_1;


public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1= new Rectangle();
        Rectangle rectangle2= new Rectangle(2.4, 4.2);
        System.out.println(rectangle1.getArea());                                                        //        1 * 1
        System.out.println(rectangle1.getPerimeter());                                                  //     2 (1 + 1)

        System.out.println(rectangle2.getArea());                                                     //      2.4 * 4.2
        System.out.println(rectangle2.getPerimeter());                                               // 2 * (2.4 + 4.2)
    }
}
