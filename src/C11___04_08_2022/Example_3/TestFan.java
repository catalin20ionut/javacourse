package C11___04_08_2022.Example_3;

public class TestFan {
    public static void main(String[] args) {
        Fan fan1 = new Fan();
        Fan fan2 = new Fan();

        System.out.println("1a: " + fan1.getColour());
        System.out.println("1b: " + fan1.getRadius());
        System.out.println("1c: " + fan1.getSpeed());
        System.out.println("1d: " + fan1.isOn());
        System.out.println("1_: " + fan1.describeFan());

        fan1.setColour("yellow");
        fan1.setRadius(10);
        fan1.setOn(true);
        fan1.setSpeed(3);
        System.out.println("2a: " + fan1.getColour());
        System.out.println("2b: " + fan1.getRadius());
        System.out.println("2c: " + fan1.getSpeed());
        System.out.println("2d: " + fan1.isOn());
        System.out.println("2_: " + fan1.describeFan());

        fan1.setOn(false);
        System.out.println("3_: " + fan1.describeFan());
    }
}
