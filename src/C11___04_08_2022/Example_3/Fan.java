package C11___04_08_2022.Example_3;

/** 3. The Fan class --- Create a class named Fan to represent a fan. The class contains:
■ Three constants named SLOW, MEDIUM, and FAST with the values 1, 2, and 3 to denote the fan speed.
■ A private int data field named speed that specifies the speed of the fan; the default is SLOW.
■ A private boolean data field named on that specifies whether the fan is on; the default is false.
■ A private double data field named radius that specifies the radius of the fan; the default is 5.
■ A string data field named color that specifies the color of the fan, the default is blue.
■ The getters and setters methods for all four data fields.
■ A no-arg constructor that creates a default fan.
■ A method named describeFan() that returns a string description for the fan. If the fan is on, the method returns the
fan speed, color, and radius in one combined string. If the fan is not on, the method returns the fan color and radius
along with the string “fan is off” in one combined string.
Write a test program that creates two Fan objects. Assign maximum speed, radius 10, color yellow, and turn it on to the
first object. Assign medium speed, radius 5, color blue, and turn it off to the second object. Display the objects by
invoking their describeFan() method.                                                                                 */

public class Fan {
    private final int SLOW = 1;
    private final int MEDIUM = 2;
    private final int FAST = 3;
    private int speed;
    private double radius;
    private boolean on;
    private String colour;

    public Fan() {
        this.speed = SLOW;
        this.colour = "Blue";
        this.on = false;
        this.radius = 5;
    }

    public String getSpeed() {
        String result = "";
        switch (speed) {
            case SLOW:
                result = "Slow";
                break;
            case MEDIUM: result = "Medium"; break;
            case FAST: result = "Fast"; break;}
        return result;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String describeFan() {
        if (on){
            return "FAN speed: " + getSpeed() + " --- FAN colour: " + getColour() + " --- FAN radius: " + getRadius();}
        else {
             return "FAN colour: " + getColour() + " --- FAN radius: " + getRadius() + " --- FAN is off.";}
    }
}
