package C15___01_09_2022___Inheritance.Example_2___Vehicle;

public class Car extends Vehicle {
    private int noOfGears;
    private boolean isAutomatic;
    private int engineCapacity;
    private boolean hasSunRoof;
    private int currentSpeed;

    public Car(String name, int noOfWheels, String colour, boolean hasSteeringWheel, int weight, int noOfGears,
               boolean isAutomatic, int engineCapacity, boolean hasSunRoof) {
        super(name, noOfWheels, colour, hasSteeringWheel, weight);
        this.noOfGears = noOfGears;
        this.isAutomatic = isAutomatic;
        this.engineCapacity = engineCapacity;
        this.hasSunRoof = hasSunRoof;
        this.currentSpeed = 0;
    }

    public int getNoOfGears() {
        return noOfGears;
    }

    public void setNoOfGears(int noOfGears) {
        this.noOfGears = noOfGears;
    }

    public boolean isAutomatic() {
        return isAutomatic;
    }

    public void setAutomatic(boolean automatic) {
        isAutomatic = automatic;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public boolean isHasSunRoof() {
        return hasSunRoof;
    }

    public void setHasSunRoof(boolean hasSunRoof) {
        this.hasSunRoof = hasSunRoof;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public void changingGear(int speedGear) {
        if (isAutomatic){
            System.out.println("We don't need to change the speed gear!");
        } else {
            System.out.println("You changed into the speed gear: " + ANSI_BLUE + speedGear + ANSI_RESET);
        }
    }

    public void increasingTheSpeed (int milesPerHour) {
        this.currentSpeed = + milesPerHour;
        System.out.println("You increased the speed with " + ANSI_YELLOW + milesPerHour + ANSI_RESET
                + " and now you are driving at the speed of " + this.currentSpeed + ".");
        super.isMoving(currentSpeed);
    }


   public void decreasingTheSpeed (int milesPerHour) {
        if(this.currentSpeed < milesPerHour){
            System.out.println("You cannot decrease the speed by "
                    + ANSI_GREEN_BACKGROUND + ANSI_BLACK + milesPerHour + ANSI_RESET + ANSI_RESET
                    + "because the current speed is " + this.currentSpeed);
        } else {
            this.currentSpeed -= milesPerHour;
            System.out.println("You decreased the speed by "
                    + ANSI_GREEN_BACKGROUND + ANSI_BLACK + milesPerHour + ANSI_RESET + ANSI_RESET
                    + " miles and now the movement speed is " + this.currentSpeed + " miles per hours.");
            super.isMoving(currentSpeed);
        }
   }
}
