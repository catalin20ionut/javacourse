package C15___01_09_2022___Inheritance.Example_2___Vehicle;

public class Dacia extends Car {
    private boolean rattles;                                                                  // troncăne, zdrăngănește
    private boolean madeInRomania;
    private int manufacturingYear;

    public Dacia(String name, int noOfWheels, String colour, boolean hasSteeringWheel, int weight,
                 int noOfGears, int engineCapacity, boolean madeInRomania, int manufacturingYear) {
        super(name, noOfWheels, colour, hasSteeringWheel, weight,
                noOfGears,false, engineCapacity, false);
        this.rattles = true;
        this.madeInRomania = madeInRomania;
        this.manufacturingYear = manufacturingYear;
    }

    public boolean isRattles() {
        return rattles;
    }

    public void setRattles(boolean rattles) {
        this.rattles = rattles;
    }

    public boolean isMadeInRomania() {
        return madeInRomania;
    }

    public void setMadeInRomania(boolean madeInRomania) {
        this.madeInRomania = madeInRomania;
    }

    public int getManufacturingYear() {
        return manufacturingYear;
    }

    public void setManufacturingYear(int manufacturingYear) {
        this.manufacturingYear = manufacturingYear;
    }

    @Override
    public void increasingTheSpeed(int milesPerHour) {
        if(getCurrentSpeed() + milesPerHour > 100) {
            System.out.println("You gonna exceed the maximum speed of Dacia! -> 100 miles/hour");
            setCurrentSpeed(100);
        } else {
            setCurrentSpeed(getCurrentSpeed() + milesPerHour);
            System.out.println("Dacia is running not at the speed of" + getCurrentSpeed() + "miles per hour.");
        }
    }
}
