package C15___01_09_2022___Inheritance.Example_2___Vehicle;
/** Vehicle -> Car -> Dacia -> C22shi___19_10_2022.Main
Start with a base class of a Vehicle, then create a Car class that inherits from this base class.
Finally, create another class, a specific type of Car that inherits Car class.
You should be able to hand steering, changing gears, and moving (speed in other words).
You will want to decide where to put the appropriate state and behaviours (fields and methods).
As mentioned above, changing gears, increasing/decreasing speed should be included.
For you specific type of vehicle you will want to add something specific for that type of car.
*/

public class Main {
    public static void main(String[] args) {
    Car car1 = new Car ("Car", 4,"black", true, 1300,
            5,false, 2000, true);
    Dacia dacia1 = new Dacia("Sandero Stepway", 4, "Blue", true, 1000,
            5, 1500, true, 2019);
    car1.increasingTheSpeed(120);
    System.out.println("---------------------------------------------------------------------------------------------");
    System.out.println("2.->");                                              dacia1.increasingTheSpeed(120);
    System.out.println("3. -> " + dacia1.getCurrentSpeed());
    System.out.println("4.-> ");                                                         car1.changingGear(4);
    System.out.println("5.-> ");                                                car1.decreasingTheSpeed(70);
    System.out.println("6.-> ");                                              dacia1.decreasingTheSpeed(70);
    System.out.println("7.-> " + dacia1.getCurrentSpeed());
    System.out.println("8.-> ");                                                       dacia1.changingDirection("left");
    }
}

