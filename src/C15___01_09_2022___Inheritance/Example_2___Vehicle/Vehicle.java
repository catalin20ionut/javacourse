package C15___01_09_2022___Inheritance.Example_2___Vehicle;

public class Vehicle {
    private String name;
    private int noOfWheels;
    private String colour;
    private boolean hasSteeringWheel;
    private int weight;
    // output colouring, underlining, background
    // https://www.codegrepper.com/code-examples/java/add+color+to+java+console+output
    public static final String ANSI_RESET = "\u001B[0m";

    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static final String BLACK_UNDERLINED = "\033[4;30m";  // BLACK
    public static final String RED_UNDERLINED = "\033[4;31m";    // RED
    public static final String GREEN_UNDERLINED = "\033[4;32m";  // GREEN
    public static final String YELLOW_UNDERLINED = "\033[4;33m"; // YELLOW
    public static final String BLUE_UNDERLINED = "\033[4;34m";   // BLUE
    public static final String PURPLE_UNDERLINED = "\033[4;35m"; // PURPLE
    public static final String CYAN_UNDERLINED = "\033[4;36m";   // CYAN
    public static final String WHITE_UNDERLINED = "\033[4;37m";  // WHITE


    public Vehicle(String name, int noOfWheels, String colour, boolean hasSteeringWheel, int weight) {
        this.name = name;
        this.noOfWheels = noOfWheels;
        this.colour = colour;
        this.hasSteeringWheel = hasSteeringWheel;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoOfWheels() {
        return noOfWheels;
    }

    public void setNoOfWheels(int noOfWheels) {
        this.noOfWheels = noOfWheels;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isHasSteeringWheel() {
        return hasSteeringWheel;
    }

    public void setHasSteeringWheel(boolean hasSteeringWheel) {
        this.hasSteeringWheel = hasSteeringWheel;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void isMoving (int speed) {
        System.out.println("The vehicle is moving at speed of " + ANSI_RED + speed + ANSI_RESET + " miles per hour.");
    }

    public void changingDirection(String direction) {
        System.out.println("You changed the direction to the " +
                ANSI_GREEN + "\033[4;32m" + direction + ANSI_RESET +
                ".");
    }
}
