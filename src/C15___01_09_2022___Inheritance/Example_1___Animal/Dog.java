package C15___01_09_2022___Inheritance.Example_1___Animal;

public class Dog extends Animal{
    private int noOfTeeth;
    private String breed;
    private String fur;

    public Dog(String name, int age, char gender, String animalType,
               int weight, int noOfTeeth, String breed, String fur) {
        super(name, age, gender, "mammal", weight);
        this.noOfTeeth = noOfTeeth;
        this.breed = breed;
        this.fur = fur;
    }

    @Override
    public void eating () {
        System.out.println("The dog is eating now.");
        theDogIsEating();
    }

    public void theDogIsEating () {
        System.out.println("The dog is eating berries!");
        super.eating();
    }

    public void theDogRuns () {
        System.out.println("We are in the method theDogRuns().");
//        super.moving(10);
        moving(10);
    }
}
