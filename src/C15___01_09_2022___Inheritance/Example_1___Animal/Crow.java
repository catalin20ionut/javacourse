package C15___01_09_2022___Inheritance.Example_1___Animal;

public class Crow extends Animal{
    private String plumage;
    private String beakType;
    private String colourClaws;

    public Crow(String name, int age, char gender, String animalType,
                int weight, String plumage, String beakType, String colourClaws) {
        super(name, age, gender, "bird", weight);
        this.plumage = plumage;
        this.beakType = beakType;
        this.colourClaws = colourClaws;
    }

    public String getPlumage() {
        return plumage;
    }

    public void setPlumage(String plumage) {
        this.plumage = plumage;
    }

    public String getBeakType() {
        return beakType;
    }

    public void setBeakType(String beakType) {
        this.beakType = beakType;
    }

    public String getColourClaws() {
        return colourClaws;
    }

    public void setColourClaws(String colourClaws) {
        this.colourClaws = colourClaws;
    }

    private void flying () {
        System.out.println("The crow is flying.");
        moving(50);
    }

    @Override
    public void eating () {
        System.out.println("The crow eats corn.");
        flying();
    }

    @Override
    public void sleeping () {
        System.out.println("The crow sleeps on a rock.");
        eating();
        super.eating();
    }


}
