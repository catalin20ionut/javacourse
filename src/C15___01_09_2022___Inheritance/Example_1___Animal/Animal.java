package C15___01_09_2022___Inheritance.Example_1___Animal;

public class Animal {
    private String name;
    private int age;
    private char gender;
    private String animalType;
    private int weight;

    public Animal(String name, int age, char gender, String animalType, int weight) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.animalType = animalType;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    protected void eating (){
        System.out.println("Method eating() was called.");
    }

    public void sleeping (){
        System.out.println("Method sleeping() was called.");
    }
    public void moving (int speed) {
        System.out.println("The animal moves at the speed of "  + speed + " miles per hour.");
    }
}
