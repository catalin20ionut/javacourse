package C15___01_09_2022___Inheritance.Example_1___Animal;

public class Main {
    public static void main(String[] args) {
        Animal animal1 = new Animal("Animal", 3, 'F', "generic", 100);
        Dog dog = new Dog("Blackie", 3, 'F', "mammal", 20, 20,
                "from countryside", "black");
        Crow crow = new Crow("Cra", 3, 'F', "bird", 1,
                "black", "long","black");

        dog.theDogIsEating();
        dog.eating();
        dog.theDogRuns();
        System.out.println(dog.getName());
        dog.setName("Brownie");
        System.out.println(dog.getName());

        crow.eating();
        crow.moving(55);
        crow.sleeping();
   }
}
