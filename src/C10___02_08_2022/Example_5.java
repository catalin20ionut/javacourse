package C10___02_08_2022;

/** 5. Geometry: area of a pentagon
The area of a pentagon can be computed using the following formula:                        Area = (5*s^2)/4 * tan(PI/5)
Write a method that returns the area of a pentagon using the following header:   public static double area(double side)
Write a main method that allows you to enter the side of a pentagon and displays its area.                           */

public class Example_5 {
    public static double area(double side) {
        double pentagonArea;
        pentagonArea = 5 * Math.pow(side, 2) / 4 * Math.tan(Math.PI / 5);
        return pentagonArea;
    }

    public static void main(String[] args) {
        double side = 10.259;
        System.out.println("Area of a regular pentagon with the length of the side of " + side
                                                                                          + " is " + area(side) + ".");
    }
}
