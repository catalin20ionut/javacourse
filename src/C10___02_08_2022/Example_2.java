package C10___02_08_2022;

/**
2. Find the largest n such that n^3 < 12,000. Use a while loop to find the largest integer n such that n3 is less than
12,000.                                                                                                             */

public class Example_2 {
    public static int largestNumber () {
        int n = 0;
        while (Math.pow((n + 1), 3) < 12000){
            System.out.println((n + 1) + "^3 is: " + (int) Math.pow(n+1, 3));
            n++;}
            return n;
    }

    public static void main(String[] args) {
        System.out.println("The biggest integer number raised to the power of 3 is: " + largestNumber());
    }
}
