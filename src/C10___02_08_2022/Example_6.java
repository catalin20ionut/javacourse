package C10___02_08_2022;

/** 6. Geometry: area of a regular polygon                         A regular polygon is an n-sided polygon in which all
sides are of the same length and all angles have the same degree. I.e. the polygon is both equilateral and equiangular.
The formula for computing the area of a regular polygon is ....................... Area = (n * s^2) / (4 * tan(PI / n))

                           Write a method that returns the area of a regular polygon
                     using the following header: public static double area(int n, double side)
                                                                                                                Write a
main method that allows you to enter the number of sides and the side of a regular polygon and displays its area.    */

public class Example_6 {
    public static double area(int n, double side) {
        double regularPolygonArea;
        regularPolygonArea = n * Math.pow(side, 2) / (4 * Math.tan(Math.PI / n));
        return regularPolygonArea;
    }

    public static void main(String[] args) {
        int n = 6;
        double side = 10.2;
        System.out.println("Area of a regular polygon with the length of the side of " + side
                                                                                        + " is " + area(n, side) + ".");
    }
}
