package C10___02_08_2022;

/**   1. Write a program that allows you to enter a string and displays the characters at odd positions.             */
public class Example_1a {
    public static  String displayCharacters (String str) {
        String result = " ";
        for (int i = 0; i < str.length(); i += 2) {
            result += str.charAt(i);
            System.out.println(result);}
        return result;
    }

    public static void main(String[] args) {
        System.out.println(displayCharacters(" oana"));
    }
}
