package C10___02_08_2022;

/**   1. Write a program that allows you to enter a string and displays the characters at odd positions. */
public class Example_1b {
    public static String displayCharacters(String str) {
        String result = "";
        String trimmedString = str.trim();
        for (int i = 0; i < trimmedString.length(); i += 2) {
            result += trimmedString.charAt(i);
            System.out.println(result); }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(displayCharacters(" oana"));
    }
}
