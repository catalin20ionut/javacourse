package C10___02_08_2022;

/**
3. Check SSN ------------- Write a program that will allow  to enter a Social Security number in the format DDD-DD-DDDD,
where D is a digit. Your program should check whether the input is valid. Hint: use Character.isDigit to verify if D is
a digit, the length must be exactly 11 characters, and the ‘-‘ is always at position 3 and 6. If these conditions aren't
valid, then the number is an invalid number, and you should display “Invalid Social Security number”.
Hint: use Character.isDigit to find if is a number and chatAt to get the value.
 */
public class Example_3 {
    public static boolean checkCode (String socialNumber) {
        if (socialNumber.length() == 11                  && Character.isDigit(socialNumber.charAt(0))
            && Character.isDigit(socialNumber.charAt(1)) && Character.isDigit(socialNumber.charAt(2))
            && Character.isDigit(socialNumber.charAt(4)) && Character.isDigit(socialNumber.charAt(5))
            && Character.isDigit(socialNumber.charAt(7)) && Character.isDigit(socialNumber.charAt(8))
            && Character.isDigit(socialNumber.charAt(9)) && Character.isDigit(socialNumber.charAt(10))
            && socialNumber.charAt(3) == '-' && socialNumber.charAt(6) == '-'
             ) { return true; }
        else {
            return false; }
    }

    public static void main(String[] args) {
        System.out.println(checkCode("abc"));
        System.out.println(checkCode("123-45-6789"));
    }
}
