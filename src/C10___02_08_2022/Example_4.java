package C10___02_08_2022;

/**
4. Display leap years ----- Write a program that displays all the leap years, ten per line, from 101 to 2100, separated
by exactly one space. Also display the number of leap years in this period.                                          */

public class Example_4 {
    public static void displayingLeapYear (int minimumIntroducedYear, int maximumIntroducedYear) {
        int count = 0;
        for (int year = minimumIntroducedYear; year <= maximumIntroducedYear; year ++) {
            if (year % 4 == 0) {
                count++;
                if (count % 10 == 0) {
                    System.out.println(year);}
                else {
                    System.out.print(year + " "); }
            }
        }
        System.out.println("\nThe number of leap years in the interval " + minimumIntroducedYear
                                                               + " - "  + maximumIntroducedYear + " is " + count + ".");
    }

    public static void main(String[] args) {
        displayingLeapYear(101, 2100);
    }
}
