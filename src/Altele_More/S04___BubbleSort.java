package Altele_More;// https://www.javatpoint.com/bubble-sort-in-java
import java.util.Arrays;

public class S04___BubbleSort {
    static void bubbleSort(int[] bubbleArrayList) {
        int n = bubbleArrayList.length;
        for(int i = 1; i < n; i++) {
            for(int j = 0; j < (n-1); j++) {
                if(bubbleArrayList[j] > bubbleArrayList[j+1]) {
                    int temp;
                    temp = bubbleArrayList[j];
                    bubbleArrayList[j] = bubbleArrayList[j+1];
                    bubbleArrayList[j+1] = temp;
                }
            }
        }
    }
    public static void main(String[] args) {
        int[] myArrayList = {3, 60, 35, 2, 45, 320, 5};
        System.out.println("My list before the bubble sort: " + Arrays.toString(myArrayList));

        bubbleSort(myArrayList);
        System.out.println("My list after the bubble sort:  " + Arrays.toString(myArrayList));
    }
}