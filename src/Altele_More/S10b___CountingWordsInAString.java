package Altele_More;

public class S10b___CountingWordsInAString {
    public static void countingWords(String sentence) {
        int wordCount = 0;

        for(int i = 0; i < sentence.length()-1; i++) {
            if(sentence.charAt(i) == ' ' && Character.isLetter(sentence.charAt(i+1))) {
                wordCount++;}}
        wordCount++;
        System.out.println("Total number of words in the given string: " + wordCount);
    }

    public static void main(String[] args) {
        countingWords("Mike is a good player and he is so punctual!!");
    }
}