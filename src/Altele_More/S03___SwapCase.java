package Altele_More;// http://www.java2s.com/example/java-utility-method/string-case-swap/swapcase-string-str-56cc6.html

public class S03___SwapCase {

        public static String swapCase(String str) {
            int strLen;
            if (str == null || (strLen = str.length()) == 0) {
                return str;}
            StringBuilder anyString = new StringBuilder(strLen);

            char ch;
            for (int i = 0; i < strLen; i++) {
                ch = str.charAt(i);
                if (Character.isUpperCase(ch)) {
                    ch = Character.toLowerCase(ch);}
                else if (Character.isLowerCase(ch)) {
                    ch = Character.toUpperCase(ch);}
                anyString.append(ch);
            }
            return anyString.toString();
        }

    public static void main(String[] args) {
        System.out.println(swapCase(" ANa aR-E mERe@.c/O*m ."));
    }

}
