package Altele_More.Palindrome_Anagrams;


public class C1___in_a_map {
    public static void main(String[] args) {
        String word1 = "Ana";
        System.out.println(word1.equalsIgnoreCase(new StringBuilder(word1).reverse().toString()));
        System.out.println(word1.equalsIgnoreCase(new StringBuilder(word1).reverse().toString()) ?
                word1 + " is a palindrome." :word1 + " is not a palindrome.");
    }
}


