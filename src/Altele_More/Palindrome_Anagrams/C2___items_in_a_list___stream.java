package Altele_More.Palindrome_Anagrams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class C2___items_in_a_list___stream {
    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("abc");
        myArrayList.add("car");
        myArrayList.add("ada");
        myArrayList.add("anna");
        myArrayList.add("Hannah");
        myArrayList.add("racecar");

        myArrayList.forEach(word -> System.out.println(".forEach() -> " + word));

        // .reduce() is the first item that respects the rule
        myArrayList.stream().reduce((word, myArraylist) -> word.length() > 3 ? word : myArraylist)
                .ifPresent(System.out::println);

        List<String> bigWords = myArrayList.stream().filter(word -> word.length() > 5).collect(Collectors.toList());
        System.out.println("3. " + bigWords);

        List<String> arrayListWithPalindromes = myArrayList.stream()
                .filter(word -> word.equalsIgnoreCase(new StringBuilder(word).reverse().toString()))
                .collect(Collectors.toList());
        System.out.println("4. ArrayList with palindromes -> " + arrayListWithPalindromes);
    }
}