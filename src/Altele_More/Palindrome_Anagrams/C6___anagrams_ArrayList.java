package Altele_More.Palindrome_Anagrams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// Check whether two words are anagrams.                                          changing the string into an ArrayList
public class C6___anagrams_ArrayList {
    public static void areAnagrams(String anyString1, String anyString2) {
        String[] charactersOfTheString1 = anyString1.toLowerCase().split("");
        List<String> characterArrayListOfAnyString1 = Arrays.asList(charactersOfTheString1);
        System.out.println("1. unsorted ---> " + characterArrayListOfAnyString1);
        Collections.sort(characterArrayListOfAnyString1); // sorting an ArrayList
        System.out.println("1. sorted   ---> " + characterArrayListOfAnyString1);

        String[] charactersOfTheString2 = anyString2.toLowerCase().split("");
        List<String> characterArrayListOfAnyString2 = Arrays.asList(charactersOfTheString2);
        System.out.println("2. unsorted ---> " + characterArrayListOfAnyString2);
        Collections.sort(characterArrayListOfAnyString2);
        System.out.println("2. sorted   ---> " + characterArrayListOfAnyString2);

        if (characterArrayListOfAnyString1.equals(characterArrayListOfAnyString2)) {
            System.out.println(anyString1 + " and " + anyString2 + " are anagrams.");}
        else {
            System.out.println(anyString1 + " and " + anyString2 + " are not anagrams.");}
    }

    public static void main(String[] args) {
        areAnagrams("care", "Race");
        areAnagrams("carr", "crar");
    }
}