package Altele_More.Palindrome_Anagrams;

class C1__number {
    public static void checkingNumberIsPalindrome(int anyNumber) {
        int temporalAnyNumber = anyNumber;
        int reversedAnyNumber = 0;
        int remainder;

        while (anyNumber > 0) {
            remainder = anyNumber % 10;
            reversedAnyNumber = reversedAnyNumber * 10 + remainder;
            anyNumber /= 10;}

        if (temporalAnyNumber == reversedAnyNumber) {
            System.out.println("The number " + temporalAnyNumber + " is palindrome.");}
        else {
            System.out.println("The number " + temporalAnyNumber + " is not palindrome.");}
    }

    public static void main(String[] args) {
        checkingNumberIsPalindrome(121);
    }
}