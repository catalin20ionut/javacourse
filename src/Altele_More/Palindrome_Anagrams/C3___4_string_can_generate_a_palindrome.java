package Altele_More.Palindrome_Anagrams;

import java.util.stream.IntStream;

public class C3___4_string_can_generate_a_palindrome {
    public static boolean checkingIfAStringCanFormAPalindrome(String anyString) {        
    return IntStream.range(0, anyString.length() / 2).noneMatch(i -> anyString.toLowerCase().charAt(i)
                != anyString.toLowerCase().charAt(anyString.length() - i - 1));
    }

    public static void main(String[] args) {
        System.out.println(checkingIfAStringCanFormAPalindrome("Ana ana"));
    }
}
