package Altele_More.Palindrome_Anagrams;

import java.util.ArrayList;

public class C4___an_ArrayList {
    public static void checkingArrayListIsPalindrome (ArrayList<String> anyArrayList) {
        System.out.println("1. myArrayList --> " + anyArrayList);
        StringBuilder theStringMadeFromTheItemsOfAnyArrayList = new StringBuilder();
        for (String word : anyArrayList) {
            theStringMadeFromTheItemsOfAnyArrayList.append(word.toLowerCase()
                    .replace(",", "").replace("'",""));}
        System.out.println("2. The string        : " + theStringMadeFromTheItemsOfAnyArrayList);

        StringBuilder reversedString = new StringBuilder();
        for (int i = (theStringMadeFromTheItemsOfAnyArrayList.length() - 1); i >= 0; i--) {
            reversedString.append(theStringMadeFromTheItemsOfAnyArrayList.charAt(i));}
        System.out.println("3. The reverse string: " + reversedString);

        if (theStringMadeFromTheItemsOfAnyArrayList.toString().equals(reversedString.toString())) {
            System.out.println("3. The ArrayList is palindrome.");}
        else {
            System.out.println("3. The ArrayList is not palindrome.");}
    }

    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>();
        myArrayList.add("Ana");
        myArrayList.add("are");
        myArrayList.add("mere.");
        myArrayList.add("erem");
        myArrayList.add("era");
        myArrayList.add("Ana");
        checkingArrayListIsPalindrome(myArrayList);

        ArrayList<String> anEnglishPalindromeSentence = new ArrayList<>();
        anEnglishPalindromeSentence.add("Madam,");
        anEnglishPalindromeSentence.add("I'm");
        anEnglishPalindromeSentence.add("Adam");
        checkingArrayListIsPalindrome(anEnglishPalindromeSentence);

        ArrayList<String> anGermanPalindromeSentence = new ArrayList<>();
        anGermanPalindromeSentence.add("Ein");
        anGermanPalindromeSentence.add("Neger");
        anGermanPalindromeSentence.add("mit");
        anGermanPalindromeSentence.add("Gazelle");
        anGermanPalindromeSentence.add("zagt");
        anGermanPalindromeSentence.add("im");
        anGermanPalindromeSentence.add("Regen");
        anGermanPalindromeSentence.add("nie");
        checkingArrayListIsPalindrome(anGermanPalindromeSentence);
    }
}