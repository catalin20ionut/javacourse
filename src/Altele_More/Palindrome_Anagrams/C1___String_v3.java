package Altele_More.Palindrome_Anagrams;

public class C1___String_v3 {
    public static boolean verifyingWhetherWordIsPalindrome(String anyString){
        if(anyString.length() == 0 || anyString.length() == 1){
            return true;}
        if(anyString.toLowerCase().charAt(0) == anyString.toLowerCase().charAt(anyString.length()-1)){
            return verifyingWhetherWordIsPalindrome(anyString.substring(1, anyString.length()-1));}
        return false ;
    }

    public static void display (String anyString){
        if(verifyingWhetherWordIsPalindrome(anyString)){
            System.out.println(anyString + " is a palindrome.");}
        else {
            System.out.println(anyString + " is not a palindrome.");}
    }

    public static void main(String[]args){
        display("Anna");
        display("121");
        display("Mary");
        display("She is si ehs");
    }
}
