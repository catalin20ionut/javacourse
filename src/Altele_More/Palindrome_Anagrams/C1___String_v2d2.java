package Altele_More.Palindrome_Anagrams;


public class C1___String_v2d2 {
    public static void verify (String anyString) {
        System.out.println(anyString.equalsIgnoreCase(new StringBuilder(anyString).reverse().toString()) ?
                anyString + " is a palindrome." :anyString + " is not a palindrome.");
    }

    public static void main(String[] args) {
        verify("Hannah");
    }
}
