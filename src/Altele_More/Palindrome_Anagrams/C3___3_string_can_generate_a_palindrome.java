package Altele_More.Palindrome_Anagrams;

import java.util.TreeMap;

class C3___3_string_can_generate_a_palindrome {
    public static boolean canPermutePalindrome(String anyString) {
        TreeMap<Character, Integer> map = new TreeMap<>();
        for (int i = 0; i < anyString.length(); i++) {
            map.put(anyString.charAt(i), map.getOrDefault(anyString.charAt(i), 0) + 1);}

        int count = 0;
        for (char key : map.keySet()) {
            count += map.get(key) % 2;}

        System.out.print("\u001B[32m" + anyString + "\u001B[0m -->  ");
        return count <= 1;
    }

    public static void main(String[] args) {
        System.out.println(canPermutePalindrome(""));
        System.out.println(canPermutePalindrome("a"));
        System.out.println(canPermutePalindrome("aa"));
        System.out.println(canPermutePalindrome("racecar"));
        System.out.println(canPermutePalindrome("racacar"));
        System.out.println(canPermutePalindrome("daily"));
    }
}