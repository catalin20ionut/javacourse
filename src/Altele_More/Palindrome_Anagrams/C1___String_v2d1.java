package Altele_More.Palindrome_Anagrams;


public class C1___String_v2d1 {
    public static void verify1 (String anyString) {
        System.out.println(anyString.equalsIgnoreCase(new StringBuilder(anyString).reverse().toString()));
    }

    public static boolean verify2 (String anyString) {
        return anyString.equalsIgnoreCase(new StringBuilder(anyString).reverse().toString());
    }

    public static void main(String[] args) {
        verify1("Hannah");
        System.out.println(verify2("Hannah"));
    }
}
