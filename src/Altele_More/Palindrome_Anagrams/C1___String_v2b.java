package Altele_More.Palindrome_Anagrams;


public class C1___String_v2b {
    public static void verifyingMethod(String anyString){
        StringBuilder reverseString = new StringBuilder(new StringBuilder(anyString).reverse().toString());
        if (anyString.equalsIgnoreCase(reverseString.toString())) {
            System.out.println(anyString + " is a palindrome.");}
        else {
            System.out.println(anyString + " isn't a palindrome.");}
    }
    /* or
    public static void verifyingMethod(String anyString){
        if (anyString.equalsIgnoreCase(new StringBuilder(anyString).reverse().toString())) {
            System.out.println(anyString + " is a palindrome.");}
        else {
            System.out.println(anyString + " isn't a palindrome.");}
    }
       */

    public static void main(String[] args) {
        verifyingMethod("a");
        verifyingMethod("aa");
        verifyingMethod("Abba");
        verifyingMethod("Anna");
        verifyingMethod("Hannah");
        verifyingMethod("Child");
    }
}