package Altele_More.Palindrome_Anagrams;

import java.util.Objects;

public class C1___String_v2a {
    public static String reverseString (String anyString) {
        return new StringBuilder(anyString).reverse().toString();
    }
    public static void verifyingMethod(String anyString){
        if (Objects.equals(anyString.toLowerCase(), reverseString(anyString).toLowerCase())) {
            System.out.println(anyString + " is a palindrome.");}
        else {
            System.out.println(anyString + " isn't a palindrome.");}
    }
    public static void main(String[] args) {
        verifyingMethod("a");
        verifyingMethod("aa");
        verifyingMethod("Abba");
        verifyingMethod("Anna");
        verifyingMethod("Hannah");
        verifyingMethod("Child");
    }
}