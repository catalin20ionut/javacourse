package Altele_More.Palindrome_Anagrams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class C5___map {
    public static void main(String[] args) {
        Map<Integer, String> my_map = new HashMap<>();
        my_map.put(0, "Racecar");
        my_map.put(1, "anA");
        my_map.put(2, "Mary");
        my_map.put(5, "hannah");

        for (int key : my_map.keySet()) {
            String value = my_map.get(key);
            System.out.println(key + " : " + value);
        }

        System.out.println("\n->.forEach()");
        my_map.forEach((key, value) -> System.out.println((key + " : " + value)));

        System.out.println("\n->.stream()");
        my_map.entrySet().stream()
                .forEach(item -> System.out.println(item.getKey() + " : " + item.getValue()));

        System.out.println(my_map.entrySet().stream()
                .filter(item -> item.getValue().length() < 4).collect(Collectors.toList()));

        System.out.println(my_map.entrySet().stream()
                .filter(item -> item.getValue().equalsIgnoreCase
                        (new StringBuilder(item.getValue()).reverse().toString()))
                .collect(Collectors.toList()));

        Map<Boolean, List<Map.Entry<Integer, String>>> newMap = my_map.entrySet().stream()
                .collect(Collectors.partitioningBy(item -> item.getValue().length() < 4));
        System.out.println(newMap);

//        Map.Entry<Integer, String> newMap = my_map.entrySet().stream()
//                .filter(item -> item.getValue().equalsIgnoreCase
//                        (new StringBuilder(item.getValue()).reverse().toString()))
//                .collect(Collectors.toCollection(newMap <java.lang.Integer,java.lang.String>)));
//        System.out.println(newMap);
    }
}