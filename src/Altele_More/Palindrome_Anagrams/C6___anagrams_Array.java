package Altele_More.Palindrome_Anagrams;

import java.util.Arrays;
// Check whether two word are anagrams.                                               changing the string into an Array
public class C6___anagrams_Array {
    public static void areAnagrams(String anyString1, String anyString2) {
        char[] charArray1 = anyString1.toLowerCase().toCharArray();
        char[] charArray2 = anyString2.toLowerCase().toCharArray();
        System.out.println("1. (Tuple) Array1 unsorted --> " + Arrays.toString(charArray1));
        System.out.println("1. (Tuple) Array2 unsorted --> " + Arrays.toString(charArray2));
        Arrays.sort(charArray1); // sorting an Array
        Arrays.sort(charArray2); // sorting an array
        System.out.println("2. (Tuple) Array1 sorted   --> " + Arrays.toString(charArray1));
        System.out.println("2. (Tuple) Array2 sorted   --> " + Arrays.toString(charArray2));

        if (Arrays.equals(charArray1, charArray2)) {
            System.out.println(anyString1 + " and " + anyString2 + " are anagrams.");}
        else {
            System.out.println(anyString1 + " and " + anyString2 + " are not anagrams.");}
    }

    public static void main(String[] args) {
        areAnagrams("caRe", "race");
    }
}