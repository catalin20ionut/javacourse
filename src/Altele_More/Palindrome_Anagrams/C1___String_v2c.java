package Altele_More.Palindrome_Anagrams;

public class C1___String_v2c {
    public static boolean verify (String word){
        return word.equalsIgnoreCase(new StringBuilder(word).reverse().toString());
    }

    public static void main(String[] args) {
        System.out.println(verify("ana"));
        System.out.println(verify("Hannah"));
        System.out.println(verify("Car"));
    }
}
