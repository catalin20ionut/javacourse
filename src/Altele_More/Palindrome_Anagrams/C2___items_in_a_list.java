package Altele_More.Palindrome_Anagrams;

import java.util.ArrayList;

public class C2___items_in_a_list {
    public static boolean verifyingWord(String anyString) {
        StringBuilder reversedString = new StringBuilder();
        for (int i = (anyString.length() - 1); i >= 0; --i) {
            reversedString.append(anyString.charAt(i));}
        return anyString.equalsIgnoreCase(reversedString.toString());
    }

    public static ArrayList<String> findingPalindromes(ArrayList<String> anyArrayList) {
        ArrayList<String> myPalindromesArrayList = new ArrayList<>();
        for (String word : anyArrayList) {
            if (verifyingWord(word)) {
                myPalindromesArrayList.add(word);}
        }
        return myPalindromesArrayList;
    }

    public static void displayPalindromes(ArrayList<String> myArrayList) {
        ArrayList<String> myPalindromesArrayList = findingPalindromes(myArrayList);
        if (myPalindromesArrayList.size() == 0){
            System.out.println("No palindrome");}
        for (String word : myPalindromesArrayList){
            System.out.println("Palindrome in myArrayList --> " + word + " ");}
        System.out.println("PalindromeArrayList --------> " + myPalindromesArrayList);
    }

    public static void main(String[] args) {
        ArrayList <String> myArrayList = new ArrayList<>();
        myArrayList.add("abc");
        myArrayList.add("car");
        myArrayList.add("ada");
        myArrayList.add("Anna");
        myArrayList.add("Hannah");
        myArrayList.add("racecar");
        displayPalindromes(myArrayList);
    }
}