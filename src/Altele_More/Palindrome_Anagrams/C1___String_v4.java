package Altele_More.Palindrome_Anagrams;

import java.util.stream.IntStream;

// I don't like this.
public class C1___String_v4 {
    public static boolean isPalindrome(String originalString) {
        String tempString = originalString.toLowerCase();
        return IntStream.range(0, tempString.length() / 2)
                .noneMatch(i -> tempString.charAt(i) != tempString.charAt(tempString.length() - i - 1));
    }

    public static void main(String[] args) {
        String word1 = "Ana";
        String word2 = "has";
        System.out.println("Is \"" + word1 + "\" a palindrome? -> " + isPalindrome(word1));
        System.out.println("Is \"" + word2 + "\" a palindrome? -> " + isPalindrome(word2));
    }
}