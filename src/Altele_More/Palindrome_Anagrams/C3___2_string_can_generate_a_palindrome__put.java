package Altele_More.Palindrome_Anagrams;

import java.util.TreeMap;

class C3___2_string_can_generate_a_palindrome__put {
    public static void canPermutePalindrome(String anyString) {
        TreeMap<Character, Integer> map = new TreeMap<>();
        char[] anyStringArray = anyString.toCharArray();
        for (char character : anyStringArray) {
            if (map.containsKey(character)) {
                map.put(character, map.get(character) + 1);
            } else {
                map.put(character, 1);
            }
        }

        int count = 0;
        for (char key: map.keySet()) {
            count += map.get(key) % 2;
            System.out.println(count + " - " + map.get(key));
        }

        if (count <= 1) {
            System.out.println("The word \"\u001B[32m" + anyString + "\u001B[0m\" can generate a palindrome.");
        } else {
            System.out.println("The word \"\u001B[32m" + anyString + "\u001B[0m\" cannot generate a palindrome.");
        }
    }

    public static void main(String[] args) {
        canPermutePalindrome("");
        canPermutePalindrome("a");
        canPermutePalindrome("aa");
        canPermutePalindrome("racecar");
        canPermutePalindrome("racacar");
        canPermutePalindrome("daily");
    }
}
