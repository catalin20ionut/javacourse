package Altele_More.Palindrome_Anagrams;

/*
# C1 --- if a string is a palindrome
# C2 --- if the items in a list are palindromes
# C3 --- if a string can generate a palindrome
# C4 --- if a list is palindrome
# C5 --- if the items in a dictionary are palindromes
# C6 --- if two strings are anagrams
*/

public class C1___String {
    public static void verifyingMethod(String anyString){
        StringBuilder reversedString = new StringBuilder();
        for (int i = (anyString.length() - 1); i >= 0; i--) {
            reversedString.append(anyString.charAt(i));}

        if (anyString.equalsIgnoreCase(reversedString.toString())) {
            System.out.println(anyString + " is a palindrome.");}
        else {
            System.out.println(anyString + " isn't a palindrome.");}
    }

    public static void main(String[] args) {
        verifyingMethod("a");
        verifyingMethod("aa");
        verifyingMethod("Abba");
        verifyingMethod("Anna");
        verifyingMethod("Hannah");
        verifyingMethod("Child");
    }
}