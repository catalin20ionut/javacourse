package Altele_More.Palindrome_Anagrams;

import java.util.stream.IntStream;

public class C3___5_string_can_generate_a_palindrome {
    public static boolean checkingIfAStringCanFormAPalindrome(String anyString) {
        String stringWithoutSpace = anyString.replaceAll("\\s+", "").toLowerCase();
    return IntStream.range(0, stringWithoutSpace.length() / 2).noneMatch(i -> stringWithoutSpace.charAt(i)
                != stringWithoutSpace.charAt(stringWithoutSpace.length() - i - 1));
    }


    public static void main(String[] args) {
        System.out.println(checkingIfAStringCanFormAPalindrome(""));
        System.out.println(checkingIfAStringCanFormAPalindrome("a"));
        System.out.println(checkingIfAStringCanFormAPalindrome("Ana"));
        System.out.println(checkingIfAStringCanFormAPalindrome("Ana ana"));
        System.out.println(checkingIfAStringCanFormAPalindrome("Ana are"));
    }
}
