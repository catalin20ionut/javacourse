package Altele_More.Palindrome_Anagrams;

import java.util.LinkedHashMap;
import java.util.Map;

public class C3___2_string_can_generate_a_palindrome__replace {
    public static boolean canPermutePalindrome(String word) {
        Map<Character, Integer> chars = new LinkedHashMap<>();
        for(int i=0; i<word.length(); i++) {
            char currentChar = word.charAt(i);
            if(chars.containsKey(currentChar)) {
                chars.replace(currentChar, chars.get(currentChar) + 1);
            } else {
                chars.put(currentChar, 1);
            }
        }

        int oddChars = 0;
        for(Map.Entry<Character, Integer> entry : chars.entrySet()) {
            if(entry.getValue() % 2 != 0) {
                oddChars++;
            }
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        System.out.println("Odd chars: " + oddChars);
        return oddChars <= 1;
    }
    public static void main(String[] args) {
        System.out.println(canPermutePalindrome(""));
        System.out.println(canPermutePalindrome("a"));
        System.out.println(canPermutePalindrome("aa"));
        System.out.println(canPermutePalindrome("racecar"));
        System.out.println(canPermutePalindrome("racacar"));
        System.out.println(canPermutePalindrome("daily"));
    }
}

