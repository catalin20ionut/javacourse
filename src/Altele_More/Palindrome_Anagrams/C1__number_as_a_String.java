package Altele_More.Palindrome_Anagrams;

public class C1__number_as_a_String {
    public static void isPalindrome (String anyNumber) {
        StringBuilder reverseNumber = new StringBuilder();
        for (int i = anyNumber.length() -1; i >= 0; i--){
            reverseNumber.append(anyNumber.charAt(i));}

        if(anyNumber.equals(reverseNumber.toString())){
            System.out.println("The number " + anyNumber + " is a palindrome.");}
        else {
            System.out.println("The number " + anyNumber + " is not a palindrome.");}
    }

    public static void main(String[] args) {
        isPalindrome("121");
    }
}