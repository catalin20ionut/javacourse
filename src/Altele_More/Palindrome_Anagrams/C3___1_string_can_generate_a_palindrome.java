package Altele_More.Palindrome_Anagrams;

import java.util.TreeMap;

class C3___1_string_can_generate_a_palindrome {
    public static void canPermutePalindrome(String anyString) {
        // it changed the string into a map
        TreeMap<Character, Integer> map = new TreeMap<>();
        for (int i = 0; i < anyString.length(); i++) {
            map.put(anyString.charAt(i), map.getOrDefault(anyString.charAt(i), 0) + 1);}
        System.out.println(map);

        int count = 0;
        for (char key: map.keySet()) {
            count += map.get(key) % 2;
//            System.out.println(count + " - " + map.get(key));
        }

        if (count <= 1) {
            System.out.println("The word \"\u001B[32m" + anyString + "\u001B[0m\" can generate a palindrome.");}
        else {
            System.out.println("The word \"\u001B[32m" + anyString + "\u001B[0m\" cannot generate a palindrome.");}
    }

    public static void main(String[] args) {
        canPermutePalindrome("");
        canPermutePalindrome("a");
        canPermutePalindrome("aa");
        canPermutePalindrome("racecar");
        canPermutePalindrome("racacar");
        canPermutePalindrome("daily");
    }
}