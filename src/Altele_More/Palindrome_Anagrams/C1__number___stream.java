package Altele_More.Palindrome_Anagrams;

import java.util.stream.IntStream;

public class C1__number___stream {
    public static boolean isPalindrome(int number) {
        return number == IntStream.iterate(number, i -> i / 10)
                .map(n -> n % 10)
                .limit(String.valueOf(number).length())
                .reduce(0, (a, b) -> a = a * 10 + b);
    }
    public static void main(String[] args) {
        System.out.println(isPalindrome(121));
    }
}