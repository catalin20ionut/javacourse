package Altele_More.Palindrome_Anagrams;

import java.util.*;
import java.util.stream.Collectors;

// cu set de posibile cuvinte
public class C3___6_string_can_generate_a_palindrome {
    static List<String> palindromeList = new ArrayList<>();
    /* displaying all the possible words from a list of strings
    public static List<Character> convertStringIntoCharList(String anyString) {
        List<Character> characterList = new ArrayList<>();        //                Creating an empty List of character
        for (char character : anyString.toCharArray()) {
            characterList.add(character);}                       //  For each character in the String add it to the List
        return characterList;                                  //                                       return the List
    }

    // the words are as a list, but I don't have a list of generated words
    public static void findingAllPossibleWords (List<Character> characterList, int k) {
        for(int i = k; i < characterList.size(); i++) {
            Collections.swap(characterList, i, k);
            findingAllPossibleWords(characterList, k+1);
            Collections.swap(characterList, k, i);
        }

        if (k == characterList.size() - 1) {
            StringBuilder concatenatedLettersToFormWords = new StringBuilder();
            for (Character letter : characterList) {
                concatenatedLettersToFormWords.append(letter);}
            System.out.println("A. findingAllPossibleWords --> " + concatenatedLettersToFormWords);
        }
    }
    */
    // It finds all the words. The method charInsert is called.
    public static Set<String> possibleWordSet(String anyString) {
        Set<String> permutation = new HashSet<>();
        if (anyString == null) {
            return null;}
        else if (anyString.length() == 0) {
            permutation.add("");
            return permutation;}

        char initial = anyString.charAt(0); // first character
        String remained = anyString.substring(1); // Full string without first character
        Set<String> words = possibleWordSet(remained);
        for (String strNew : words) {
            for (int i = 0;i<=strNew.length();i++){
                permutation.add(charInsert(strNew, initial, i));}
        }
        return permutation;
    }

    public static String charInsert(String anyString, char c, int j) {
        String begin = anyString.substring(0, j);
        String end = anyString.substring(j);
        return begin + c + end;
    }


    public static void checkingInAllPossibleWordList(String anyString){
        List<String> words = new ArrayList<>(possibleWordSet(anyString));
        System.out.println("1. The set of possible words: " + words);

        System.out.println("2. ------------------------------------------");
        List<String> palindromeList = words.stream().filter(i -> Objects.equals(i, reversedString(i))).
                collect(Collectors.toList());
        System.out.println("2. The palindrome list is:              " + palindromeList);
        /* if the possibleWordSet() would be possibleWordList(); changing an Arraylist into a HashSet
        System.out.println("2. -----------------------------------");
        Set<String> palindromeSet = new HashSet<>(palindromeList);
        System.out.println("3. The palindrome(s) formed from the word \u001B[34m" + anyString + "\u001B[0m --> "
                + palindromeSet);
         */
    }

    public static Object reversedString (String word) {
        StringBuilder reversedStr = new StringBuilder();
        for (int i = word.length() - 1; i >= 0; i--) {
            reversedStr.append(word.charAt(i));}
        System.out.println("String reversed: " + reversedStr + " ---------- from word " + word);
        if (word.equals(reversedStr.toString())) {
            System.out.println("Yes - Palindrome -----------------> " + word + ": " + palindromeList.add(word));
            return word;}
        else {
            System.out.println("Not palindrome ..............................") ;
            return null;}
    }


    public static void displayPalindrom (String anyString) {
        /* This is for findingAllPossibleWords().
        System.out.println("1. convertStringIntoCharList --> " +  convertStringIntoCharList(anyString));
        findingAllPossibleWords(convertStringIntoCharList(anyString), 0);
         */
        checkingInAllPossibleWordList(anyString);
    }

    public static void main(String[] args) {
        displayPalindrom("aba");
//        displayPalindrom("carrace");
    }
}