package Altele_More.Palindrome_Anagrams;


public class C1___String_v2d3_in_main {
    public static void main(String[] args) {
        String word1 = "Ana";
        System.out.println(word1.equalsIgnoreCase(new StringBuilder(word1).reverse().toString()));
        System.out.println(word1.equalsIgnoreCase(new StringBuilder(word1).reverse().toString()) ?
                word1 + " is a palindrome." :word1 + " is not a palindrome.");
    }
}
