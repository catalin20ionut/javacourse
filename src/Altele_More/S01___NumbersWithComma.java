package Altele_More;

public class S01___NumbersWithComma {
    public static void number(){
        long x = 4_000_000_000_000L;
        System.out.format("%,8d%n%n", x);
//        System.out.format("%_8d%n%n", x);
    }

    public static void main(String[] args) {
        number();
    }
}
