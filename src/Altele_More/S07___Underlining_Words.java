package Altele_More;

// How to underline words.
public class S07___Underlining_Words {
    public static void greeting (String greeting) {
        System.out.println("31. " + String.join("\u0331", greeting.split("", 0)));
        System.out.println("32. " + String.join("\u0332", greeting.split("", 0)));
        System.out.println("33. " + String.join("\u0333", greeting.split("", 0)));
        System.out.println("34. " + String.join("\u0334", greeting.split("", 0)));
        System.out.println("35. " + String.join("\u0335", greeting.split("", 0)));
        System.out.println("36. " + String.join("\u0336", greeting.split("", 0)));
        System.out.println("37. " + String.join("\u0337", greeting.split("", 0)));
        System.out.println("38. " + String.join("\u0338", greeting.split("", 0)));
        System.out.println("39. " + String.join("\u0339", greeting.split("", 0)));
        System.out.println("40." + String.join("\u0340", greeting.split("", 0)));
        System.out.print("60. " + String.join("\u0360", greeting.split("", 0)));
        System.out.println(" ... and so on");
    }

    public static void isMoving (String speed) {
        String s1 = "The vehicle is moving at speed of\u001B[32m" +
                String.join("\u0332", speed.split("", 0)) + "\u001B[0m miles per hour.";
        System.out.println(s1);
    }

    public static void main(String[] args) {
        greeting("Welcome Back Jack!");
        isMoving("125");
        isMoving(" 125");
    }
}
