package Altele_More;

public class S09___Combinations {
    public static void displayCombinations(String anyString){
        for(int i = 0; i < anyString.length(); i++)
            combinationLevel(anyString,0,0,"",i+1);
    }
    public static void combinationLevel(String anyString, int beginningFromTheIndex,int level, String s, int K){
        if (level == K) {
            System.out.println(s);
        } else{
            for(int i = beginningFromTheIndex; i < anyString.length(); i++)
                combinationLevel( anyString, i+1,level+1,s + anyString.charAt(i), K);
        }
    }

    public static void main(String[] args) {
        displayCombinations("1234");
    }
}