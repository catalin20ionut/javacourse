package Altele_More;// https://www.javatpoint.com/bubble-sort-in-java
import java.util.Arrays;

public class S04___BubbleSort_WithDouble {
    static void bubbleSort(double[] bubbleArrayList) {
        int n = bubbleArrayList.length;
        for(int i = 1; i < n; i++) {
            for(int j = 0; j < (n-1); j++) {
                if(bubbleArrayList[j] > bubbleArrayList[j+1]) {
                    double temp;
                    temp = bubbleArrayList[j];
                    bubbleArrayList[j] = bubbleArrayList[j+1];
                    bubbleArrayList[j+1] = temp;
                }
            }
        }
    }
    public static void main(String[] args) {
        double[] myArrayList = {3, 60, 35, 2.3, 45, 320, 5};
        System.out.println("My list before the bubble sort: " + Arrays.toString(myArrayList));

        bubbleSort(myArrayList);
        System.out.println("My list after the bubble sort:  " + Arrays.toString(myArrayList));
    }
}