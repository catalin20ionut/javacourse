package Altele_More;

public class S10a___CountingWordsInAString {
    public static void main(String[] args) {
        String sentence = "Mike is a good player and he is so punctual!!";
        int wordCount = 0;

        for(int i = 0; i < sentence.length()-1; i++) {
            if(sentence.charAt(i) == ' ' && Character.isLetter(sentence.charAt(i+1))) {
                wordCount++;}
        }
        wordCount++;
        System.out.println("Total number of words in the given string: " + wordCount);
    }

}