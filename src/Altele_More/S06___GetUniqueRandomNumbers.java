package Altele_More;

import java.util.ArrayList;
import java.util.Collections;

public class S06___GetUniqueRandomNumbers {
    public static void displayNumbers(int maxvalue) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= maxvalue; i++) {
            list.add(i);}

        Collections.shuffle(list);
        System.out.print("1. --> ");
        for (int i = 0; i < maxvalue; i++) {
            System.out.format("%-5s", list.get(i));}                                                 // on the same row

        System.out.println();
        for (int i = 0; i < maxvalue; i++) {
            System.out.println(list.get(i));}                                                      // on different rows
    }

    public static void main(String[] args) {
        displayNumbers(25);
    }
}
