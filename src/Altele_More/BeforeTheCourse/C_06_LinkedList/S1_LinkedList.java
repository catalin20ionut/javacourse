package Altele_More.BeforeTheCourse.C_06_LinkedList;
import java.util.LinkedList;

public class S1_LinkedList {
    public static void main(String[] args) {
        LinkedList<String> cars = new LinkedList<>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");

        System.out.println("1. A linked list                 : " + cars);

        cars.addFirst("Bugatti");
        System.out.println("2. Linked list after adding first: " + cars);

        cars.addLast("Ferrari");
        System.out.println("3. Linked list after adding last : " + cars);

        cars.removeFirst();
        System.out.println("4. Linked list after removing first --- Bugatti: " + cars);

        cars.removeLast();
        System.out.println("5. Linked list after removing last ---- Ferrari: " + cars);

        cars.getFirst();
        System.out.println("6. The first item of the linked list is: " + cars.getFirst());

        cars.getLast();
        System.out.println("7. The first item of the linked list is: " + cars.getLast());
    }
}