package Altele_More.BeforeTheCourse.C_08_Classes__using_multiple_classes;

import java.util.Arrays;

public class Second {
    public static void main(String[] args) {
        Main myObj = new Main();
        Main myObjFloat = new Main();
        Main myObjGreeting = new Main();
        Main myObjArray = new Main();
        System.out.println(myObj.x);
        System.out.println(myObjFloat.y);
        System.out.println(myObjGreeting.greeting);
        System.out.println("An array: " + Arrays.toString(myObjArray.cars));
        System.out.println("He's got in his garage a " + Arrays.toString(new String[]{myObjArray.cars[0]})
                + " and a " + Arrays.toString(new String[]{myObjArray.cars[1]}) + ".");
        System.out.println("An arrayList: " + Arrays.toString(myObjArray.autos));
    }
}


