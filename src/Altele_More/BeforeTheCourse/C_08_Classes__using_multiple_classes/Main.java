package Altele_More.BeforeTheCourse.C_08_Classes__using_multiple_classes;


public class Main {
    int x = 5;
    float y = 3.5f;
    String greeting = "Hello World";
    String [] cars = {"Ferrari", "Porsche"};
    String[] autos = new String[]{"Volvo", "Mitsubishi"};
}
