package Altele_More.BeforeTheCourse.C_05_ArrayList___inPy_List;
import java.util.ArrayList;  // used for creating arraylists
import java.util.Collections; // this is used for sorting an arraylist

public class S3_an_arrayList_with_numbers {
    public static void main(String[] args) {
        ArrayList<Integer> myNumbers = new ArrayList<>();
        myNumbers.add(10);
        myNumbers.add(75);
        myNumbers.add(30);
        myNumbers.add(25);
        System.out.println("1. Creating an arrayList with integers: " + myNumbers);

        Collections.sort(myNumbers);
        System.out.println("2. Option 1 >>> Sorting the items of an arrayList with integers: " + myNumbers);
        for (int i : myNumbers) {
            System.out.println("2. Option 2 >>> Sorting the items of an arrayList" +
                    " with integers using \"for loop\": " + i);
        }
    }
}