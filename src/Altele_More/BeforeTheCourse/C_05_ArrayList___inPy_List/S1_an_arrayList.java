package Altele_More.BeforeTheCourse.C_05_ArrayList___inPy_List;
import java.util.ArrayList;

public class S1_an_arrayList {
    public static void main(String[] args) {
        ArrayList<String> cars = new ArrayList<>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");

        System.out.println("1. Creating an arrayList: " + cars);

        System.out.println("•  Using a for loop --- Option 1 >>>>>>>>>>>>>>>>>");
        for (String car: cars) {
            System.out.println("2. The items in the arrayList are:           " + car);
        }
        System.out.println("•  Using a for loop --- Option 2 >>>>>>>>>>>>>>>>>");
        for (int i = 0; i < cars.size(); i++) {
            System.out.println("2. The items in the arrayList are:           " + cars.get(i));
        }

        System.out.println("3. The first item in the arrayList is ...... " + cars.get(0) + "!");
        System.out.println("4. This arrayList has " + cars.size() + " items.");
        System.out.println("•  In an array, \"cars.length\" is used to find out how many items are.");

        cars.set(0, "Opel");
        System.out.println("5. Changing the first item in the arrayList: " + cars);

        cars.remove(0);
        System.out.println("6. Removing the first item in the arrayList:       " + cars);

        cars.clear();
        System.out.println("7. All the items in the arrayList are deleted. Now the arrayList is empty: "
                + cars);
    }
}