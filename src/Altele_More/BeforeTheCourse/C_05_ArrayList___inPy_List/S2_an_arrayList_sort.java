package Altele_More.BeforeTheCourse.C_05_ArrayList___inPy_List;
import java.util.ArrayList;
import java.util.Collections;

public class S2_an_arrayList_sort {
    public static void main(String[] args) {
        ArrayList<String> cars = new ArrayList<>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");
        cars.add("bugatti");
        cars.add("253RK");

        Collections.sort(cars);
        System.out.println("1. Creating an arrayList which we sorted it: " + cars);

        for (String i : cars) {
            System.out.println("2. The items which we sorted them: " + i);
        }
    }
}