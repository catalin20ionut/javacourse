package Altele_More.BeforeTheCourse.C_03_myMethod;

public class Situation_8__if {
    static void checkAge(int age) {
        if (age < 16) {
            System.out.println("Access denied - You are not old enough!");}
        else if (age < 18) {
            System.out.println("Access granted - You are old enough, but you won't be able to see all the files!");}
        else {
            System.out.println("Access granted - You are old enough!");}
    }
   public static void main(String[] args) {
       checkAge(27);
   }
}

/*
age = 27
if age < 16:
    print("Access denied - You are not old enough!")
elif age < 18:
    print("Access granted - You are old enough, but you won't be able to see all the files!")
else:
    print("Access granted - You are old enough!")
*/