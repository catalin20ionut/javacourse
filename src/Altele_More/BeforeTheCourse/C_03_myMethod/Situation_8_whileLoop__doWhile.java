package Altele_More.BeforeTheCourse.C_03_myMethod;

public class Situation_8_whileLoop__doWhile {
        public static void main(String[] args) {
        int i = 0;
        while (i < 5) {
            System.out.println("1.While loop: " + i);
            i++;
        }

        int x = 0;
        do {
            System.out.println("2.Do ... while: " + x);
            x++;
        }
        while (x < 5);

        int y = 0;
        while (y < 10) {
            System.out.println("3. While with (if...) break: " + y);
            y++;
            if (y == 4) {
                break;
            }
        }

        int z = 0;
        while (z < 10) {
            if (z == 4) {
                z++;
                continue;
            }
            System.out.println("4. While with (if...) continue: " + z);
            z++;
        }
    }
}

/* Python
i = 1
while i < 5:
  print(i)
  i += 1
 */