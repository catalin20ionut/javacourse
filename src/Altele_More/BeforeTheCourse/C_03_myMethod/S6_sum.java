package Altele_More.BeforeTheCourse.C_03_myMethod;

public class S6_sum {
    static int myMethod(int x, int y) {
     return y + x;
   }
    public static void main(String[] args) {
     System.out.println(myMethod(3, 6));
   }
 }

/* or
public class S6_sum {
   static int myMethod(int x, int y) {
     return x + y;
   }

   public static void main(String[] args) {
     int z = myMethod(5, 3);
     System.out.println(z);
   }
 }
*/