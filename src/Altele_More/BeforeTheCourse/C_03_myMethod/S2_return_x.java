package Altele_More.BeforeTheCourse.C_03_myMethod;

public class S2_return_x {
    static int myMethod(int x) {
            return x;
        }
    public static void main(String[] args) {
        System.out.println(myMethod(100));
    }
}

/*
public class Situation_4b_myMethod {
   public static void main(String[] args) {
     int x = 100;
     System.out.println(x);
   }
}

- Python ---->
def my_function(x):
  return x

print(my_function(100))

or

def my_function(x):
  print(x)

my_function(100)
 */

