package Altele_More.BeforeTheCourse.C_03_myMethod;

public class S4_sum {
    static int myMethod(int x) {
        return 5 + x;
    }
    public static void main(String[] args) {
        System.out.println(myMethod(9));
        System.out.println(myMethod(12));
    }
}

/*
def my_func(x):
    return 5 + x

print(my_func(9))
print(my_func(12))
*/


