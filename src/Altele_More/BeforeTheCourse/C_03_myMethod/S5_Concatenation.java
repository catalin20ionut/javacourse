package Altele_More.BeforeTheCourse.C_03_myMethod;

public class S5_Concatenation {
    static void myMethod(String name, int age) {
        System.out.println(name + " is " + age + " years old.");
    }

    public static void main(String[] args) {
        myMethod("Liam", 5);
        myMethod("Jenny", 8);
        myMethod("Anja", 31);
    }
}

/*
def sentence_func(x, y):
    print("My name is " + x + " and I am " + y + " years old.")
sentence_func("Andrei", "7")
sentence_func("Sabine", "6")
*/


