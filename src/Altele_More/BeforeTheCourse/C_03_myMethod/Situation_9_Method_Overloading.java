package Altele_More.BeforeTheCourse.C_03_myMethod;

public class Situation_9_Method_Overloading {
    static int myFirstMethod(int x) {
        return 5 + x;
    }
    static float mySecondMethod(float x) {
        return 5 + x;
    }

    public static void main(String[] args) {
        int myNum1 = myFirstMethod(9);
        float myNum2 = mySecondMethod(10.259f);
        System.out.println("myFirstMethod: " + myNum1);
        System.out.println("mySecondMethod: " + myNum2);
    }
}