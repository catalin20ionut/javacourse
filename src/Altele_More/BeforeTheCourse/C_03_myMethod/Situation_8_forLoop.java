package Altele_More.BeforeTheCourse.C_03_myMethod;

public class Situation_8_forLoop {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println("1.Pasul unu: " + i);
        }
        for (int x = 0; x <= 10; x += 2) {
            System.out.println("2.Pasul doi: " + x);
        }
        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
        for (String car : cars) {
            System.out.println("3.Java forEach Loop: " + car);
        }
        for (int y = 0; y < 10; y++) {
            if (y == 4) {
                break;
            }
            System.out.println("4.For loop with (if ...) break: " + y);
        }
        for (int z = 0; z < 10; z++) {
            if (z == 4) {
                continue;                                                     // 4 will not appear in the output
            }
            System.out.println("5.For loop with (if ...) continue: " + z);
        }
    }
}

/* Python
1.
for i in range(5):
    print(i)

2.
for x in range(0, 11, 2):
    print(x)


3.
cars = ["Volvo", "BMW", "Ford", "Mazda"]
for car in cars:
	print(car)

4.
for y in range(10):
	if y == 4:
		break
	print(y)

5.
for y in range(10):
	if y == 4:
		continue
	print(y)
 */