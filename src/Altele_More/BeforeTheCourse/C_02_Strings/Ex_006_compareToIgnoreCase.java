// 6. Write a Java program to compare two strings lexicographically, ignoring case differences.
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_006_compareToIgnoreCase {
    public static void main(String[] args) {
        String str1 = "This is exercise 1";
        String str2 = "This is Exercise 1";
        int result = str1.compareToIgnoreCase(str2);

        if (result < 0) {
            System.out.println("\"" + str1 + "\"" + " is less than " + "\"" + str2 + "\".");
        }
        else if (result == 0) {
            System.out.println("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".");
        }
        else {
            System.out.println("\"" + str1 + "\"" + " is greater than " + "\"" + str2 + "\".");
        }
    }
}