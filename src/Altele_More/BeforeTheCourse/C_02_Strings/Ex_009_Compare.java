// 9. Write a Java program to compare a given string to the specified character sequence.
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_009_Compare {
    public static void main(String[] args) {
        String str1 = "example.com", str2 = "Example.com";
        CharSequence cs = "example.com";
        System.out.println("Comparing "+str1+" and "+cs+": " + str1.contentEquals(cs));
        System.out.println("Comparing "+str2+" and "+cs+": " + str2.contentEquals(cs));
    }
}

/*
Python
if cs == str1:
	print(f"Comparing {str1} and {cs}: True")
else:
	print(f"Comparing {str1} and {cs}: False")

if cs == str2:
	print(f"Comparing {str2} and {cs}: True")
else:
	print(f"Comparing {str2} and {cs}: False")
 */