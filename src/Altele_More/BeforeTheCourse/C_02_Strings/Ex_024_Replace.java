package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_024_Replace {
    public static void main(String[] args) {
        String str = "The quick brown fox jumps over the lazy dog.";
        String new_str = str.replace('d', 'f');
        System.out.println("New String: " + new_str);
    }
}