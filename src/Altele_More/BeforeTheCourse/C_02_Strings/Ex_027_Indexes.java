package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_027_Indexes {
    public static void main(String[] args) {
        String str = "The quick brown fox jumps over the lazy dog.";
        String new_str = str.substring(10, 26);
        System.out.println("new = " + new_str);
    }
}

//Python: print(str[10:26])