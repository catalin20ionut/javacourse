// 11. Write a Java program to create a new String object with the contents of a character array.
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_011_Numbers_as_strings {
    public static void main(String[] args) {
        char[] arr_num = new char[] { '1', '2', '3', '4' };

        // Create a String containing the contents of arr_num
        String str = String.copyValueOf(arr_num, 1, 3);

        // Display the results of the new String.
        System.out.println("The book contains " + str +" pages.");
    }
}

/*
given_numbers = ["1", "2", "3", "4"]
my_numbers = given_numbers[1:4]
number = ''.join(my_numbers)
print(f"The book contains {number} pages.")
 */