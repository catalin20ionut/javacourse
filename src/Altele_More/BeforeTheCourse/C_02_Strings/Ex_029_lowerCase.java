package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_029_lowerCase {
    public static void main(String[] args) {
        String str = "The Quick BroWn FoX!";
        String lowerStr = str.toLowerCase();
        System.out.println("String in lowercase: " + lowerStr);
    }
}
