/* Vezi Ex_9_Compare
5. Write a Java program to compare two strings lexicographically. Two strings are lexicographically equal if they are
the same length and contain the same characters in the same positions.                              Vezi Ex_9_Compare
*/

package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_005_Compare {
    public static void main(String[] args) {
        String str1 = "This is Exercise 1";
        String str2 = "This is Exercise 2";
        int result = str1.compareTo(str2); // Compare the two strings.

        if (result < 0) {
            System.out.println("\"" + str1 + "\"" + " is less than " + "\"" + str2 + "\".");}
        else if (result == 0) {
            System.out.println("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".");}
        else {
            System.out.println("\"" + str1 + "\"" + " is greater than " + "\"" + str2 + "\".");}

        if (result == 0) {
            System.out.println("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".");}
        else {
            System.out.println("\"" + str1 + "\"" + " is different from " + "\"" + str2 + "\".");
        }

        String strEx9 = "This is Exercise 1";
        CharSequence cs = "This is Exercise 2";
        System.out.println("2. Comparing " + "\"" + strEx9 + "\"" + " and " + "\"" + cs + "\": "
                + strEx9.contentEquals(cs));
    }
}

/* https://www.educative.io/answers/how-to-compare-two-strings-in-python
Python:
str1 = "This is Exercise 1"
str2 = "This is Exercise 2"

if str1 < str2:
    print("\"" + str1 + "\"" + " is less than " + "\"" + str2 + "\".")
elif str1 == str2:
    print("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".")
else:
    print("\"" + str1 + "\"" + " is greater than " + "\"" + str2 + "\".")

if str1 != str2:
    print("\"" + str1 + "\"" + " is different from " + "\"" + str2 + "\".")
else:
    print("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".")
*/