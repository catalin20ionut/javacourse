package Altele_More.BeforeTheCourse.C_02_Strings;

public class _Strings {
    public static void main(String[] args) {
        // for "//" it has the JDK 15+
        String a = "Hello World!";
        String txt = "We're the so-called \"Vikings\" from the north.";
        String a2 = "            Hello World!                         ";
        String a3 = ",,,,,rrttgg.....banana....rrr";
        System.out.println("print(len(txt)) --- The length of the txt string is " + txt.length() + ".");
        System.out.println("a. print(a + txt) --- Concatenating two strings: " + a.concat(txt));
        System.out.println("b. Concatenating two strings: " + a + " " + txt);
        System.out.println(a2.trim());
//        System.out.println("c. strip -> " + a2.strip());
//        System.out.println("c. Concatenating three strings: " + a + " " + txt + " " + a2.strip());
//        System.out.println("d. Concatenating three strings: " + a.concat(txt.concat(a2.strip())));
        System.out.println("1. print(txt.upper())" + " >>> " + txt.toUpperCase());
        System.out.println("2. print(txt.lower())" + " >>> " + txt.toLowerCase());
        System.out.println("3a. print(txt.find('t')" + " >>> " + txt.indexOf("t"));
        System.out.println("3b. print(txt.find('from')" + " >>> " + txt.indexOf("from"));
        System.out.println("4a-v1. print(txt[1])" + " >>> " +txt.charAt(1));

        int index1 = txt.charAt(1);
        System.out.println("4b-v1. print(txt[1])" + " >>> " +(char)index1);

        System.out.println("5. print('ice' in txt)" + " >>> " +txt.contains("ice"));
        System.out.println("6-v2. print(txt[2:5])" + " >>> " +txt.substring(2, 5));
        System.out.println("7-v4. print(txt[:5])" + " >>> " +txt.substring(0, 5));
        System.out.println("8-v4. print(txt[2:])" + " >>> " +txt.substring(2));


        System.out.println("1. print(a[1])" + " >>> " + a.charAt(1));
        System.out.println("2. print(a[2:5])" + " >>> " + a.substring(2, 5));
        System.out.println("3. print(a[2:])" + " >>> " + a.substring(2));
        System.out.println("4. print(a[:5])" + " >>> " + a.substring(0,5));
        System.out.println("5. print(a[-1])" + " >>> " + a.charAt(a.length()-1));
        System.out.println("6. print(a[-5:-2])" + " >>> " + a.substring(a.length()-5,a.length()-2));

//        System.out.println("1a. print(a2.strip())" + " >>> " + a2.strip());
        System.out.println("1b. print(a2.strip())" + " >>> " + a2.trim());
//        System.out.println("2a. print(a2.lstrip())" + " >>> " + a2.stripLeading());
//        System.out.println("3a. print(a2.rstrip())" + " >>> " + a2.stripTrailing());
//        System.out.println("2b. print(a2.lstrip())" + " >>> " + a2.replaceAll("^\s+", ""));
//        System.out.println("3b. print(a2.rstrip())" + " >>> " + a2.replaceAll("\s+$", ""));
//        System.out.println("1c. print(a2.rstrip())" + " >>> " + a2.replaceAll("^\s+|\s+$", ""));
        System.out.println("1d. print(a2.rstrip())" + " >>> " + a2.replace
                ("            Hello World!                         ", "Hello World!"));

        System.out.println("4a. print(a3.strip(',.grt'))" + " >>> " + a3.replaceAll("[,rtg.]", ""));
        System.out.println("4b. print(a3.lstrip(',.grt'))" + " >>> " + a3.replaceAll("", "@@@@"));
        System.out.println("4c. print(a3.rstrip(',.grt'))" + " >>> " + a3.replaceAll("", "@@@@"));
        System.out.println("4b. print(a3.lstrip())" + " >>> " + a3.replace(",,,,,rrttgg.....", ""));
        System.out.println("4c. print(a3.rstrip())" + " >>> " + a3.replace("....rrr", ""));


        System.out.println("1. print(a.replace('H,'J'))" + " >>> " + a.replace("H", "X"));
        String x = a.replace("H", "X");
        String y = x.replace("ll", "j");
        System.out.println("2." + " >>> " + y);
        System.out.println("1. print(c1.title())" + " >>> " + "See Altele_More/S02___TitleCase");
        System.out.println("1. print(c1.swapcase())" + " >>> " + "See Altele_More/S02___SwapCase");
    }
}

/* Python
a = "ana are mere"
b = a.title()
c = b.swapcase()

print(a)  ............................................. ana are mere
print(b)  ................................................. Ana Are Mere
print(c) .............................................. aNA aRE mERE

 */