// 8. Write a Java program to test if a given string contains the specified sequence of char values. See _Strings
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_008_Contains {
    public static void main(String[] args) {
        String str = "PHP Exercises and Python Exercises";
        System.out.println(str.contains("and"));
    }
}

/*
1. Python
str = "PHP Exercises and Python Exercises"
if "and" in str:
	print("True")
else:
	print("False")

2. Python
str = "PHP Exercises and Python Exercises"
try:
	"PHP Exercises and Python Exercises".index("and")
except:
	print("False")
else:
	print("True")

3. Python
str = "PHP Exercises and Python Exercises"
try:
	"PHP Exercises and Python Exercises".find("and")
except:
	print("False")
else:
	print("True")

4. Python
str = "PHP Exercises and Python Exercises"

if "PHP Exercises and Python Exercises".find("and") != -1:
    print("True")
else:
    print("False")
 */