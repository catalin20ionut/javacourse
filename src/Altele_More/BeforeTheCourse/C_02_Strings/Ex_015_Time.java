package Altele_More.BeforeTheCourse.C_02_Strings;
import java.util.Calendar;

public class Ex_015_Time {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current Date and Time:");
        System.out.format("%tB %te, %tY%n", c, c, c);
        System.out.format("%tl:%tM %tp%n", c, c, c);
    }
}

// Cum fac să apară într-un singur rând
// https://www.w3schools.com/java/java_date.asp
// https://www.w3schools.com/python/python_datetime.asp

/*
import datetime

x = datetime.datetime.now()

print("Current Date and Time:", x.strftime("%B %d, %Y --- %I:%M %p"))
print("Current Date and Time:", x.strftime("%B"), f"{x.day}, {x.year} ---",
x.strftime("%I"), ":", x.strftime("%M"), x.strftime("%p"))
 */