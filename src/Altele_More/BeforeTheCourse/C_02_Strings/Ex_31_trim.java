package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_31_trim {
    public static void main(String[] args)    {
        String str = "           Java Exercises                      ";
        String new_str = str.trim();
        System.out.println("New String: " + new_str);
    }
}
