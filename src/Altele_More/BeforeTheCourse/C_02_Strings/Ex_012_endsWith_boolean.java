// 12. Write a Java program to check whether a given string ends with the contents of another string.
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_012_endsWith_boolean {
    public static void main(String[] args) {
        String str1 = "Python Exercises";
        String str2 = "Python Exercise";
        String end_str = "se";

        // Check first two Strings end with end_str
        boolean ends1 = str1.endsWith(end_str);
        boolean ends2 = str2.endsWith(end_str);

        // Display the results of the endsWith calls.
        System.out.println("Does \"" + str1 + "\" end with " + "\"" + end_str + "\"? - " + ends1);
        System.out.println("Does \"" + str2 + "\" end with " + "\"" + end_str + "\"? - " + ends2);
    }
}

/*
Python

str1 = "Python Exercises"
str2 = "Python Exercise"
end_str = "se"
ends1 = str1.endswith("se")
ends2 = str2.endswith(end_str)
print(f"Does \"{str1}\" end with \"{end_str}\" ? - {ends1}")
print(f"Does \"{str2}\" end with \"{end_str}\" ? - {ends2}")
 */