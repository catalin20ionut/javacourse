package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_030_upperCase {
    public static void main(String[] args) {
        String str = "The Quick BroWn FoX!";
        String upperStr = str.toUpperCase();
        System.out.println("String in uppercase: " + upperStr);
    }
}
