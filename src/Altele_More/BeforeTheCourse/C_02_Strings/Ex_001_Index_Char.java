// 0. Write a Java program to print 'Hello' on screen and then print your name on a separate line.
// 1. Write a Java program to get the character at the given index within the String.
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_001_Index_Char {
    public static void main(String[] args) {
        System.out.println("0 ▼▼▼");
        System.out.println("Hello\nCătălin!");
        System.out.println("0 ▲▲▲▲▲▲");
        System.out.println();


        System.out.println("1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
        String myString = "Java Exercises!";

        int index1 = myString.charAt(0);
        int index2 = myString.charAt(10);

        System.out.println("•• myString = \"" + myString + "\" ••");

        System.out.println("-------- with variables ---------- ");
        System.out.println("The character at position  0 is: " + (char)index1);
        System.out.println("The character at position 10 is: " + (char)index2);

        System.out.println("------ with built-in method ------ ");
        System.out.println("The character at position  0 is: " + myString.charAt(0));
        System.out.println("The character at position 10 is: " + myString.charAt(10));
        System.out.println("1b <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");
    }
}
/*
my_string = "Java Exercises!"
print(f"The character at position  0 is: {my_string[0]}")
print("The character at position 10 is: " + my_string[10])
 */