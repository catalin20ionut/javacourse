package Altele_More.BeforeTheCourse.C_02_Strings;
import java.time.LocalDate;  // import the LocalDate class

public class Ex_015_var2_Time {
    public static void main(String[] args) {
        LocalDate myObj = LocalDate.now();  // Create a date object
        System.out.println(myObj);  // Display the current date
    }
}

// https://www.w3schools.com/java/java_date.asp