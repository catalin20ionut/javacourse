package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_035_Repetition {
    public static void main(String[] args) { permutationWithRepetition("PQR");}
    private static void permutationWithRepetition(String str1) {
        System.out.println("The given string is: PQR");
        System.out.println("The permuted strings are:");
        showPermutation(str1, "");
    }
    private static void showPermutation(String str1, String NewStringToPrint) {
        if (NewStringToPrint.length() == str1.length()) {
            System.out.println(NewStringToPrint);
            return;
        }
        for (int i = 0; i < str1.length(); i++) {
            showPermutation(str1, NewStringToPrint + str1.charAt(i));
        }
    }
}