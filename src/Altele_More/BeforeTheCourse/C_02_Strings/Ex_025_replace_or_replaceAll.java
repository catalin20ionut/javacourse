package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_025_replace_or_replaceAll {
    public static void main(String[] args) {
        String str = "The quick brown fox jumps over the lazy dog.";
        String new_str = str.replace("fox", "cat");
        // or String new_str = str.replaceAll("fox", "cat");
        System.out.println("New String: " + new_str);
    }
}
