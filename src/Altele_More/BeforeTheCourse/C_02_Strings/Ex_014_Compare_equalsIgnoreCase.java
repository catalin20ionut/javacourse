package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_014_Compare_equalsIgnoreCase {
    public static void main(String[] args) {
        String column1 = "Stephen Edwin King";
        String column2 = "Walter Winchell";
        String column3 = "stephen edwin king";

        // Test any of the above Strings equal to one another
        boolean equals2 = column1.equalsIgnoreCase(column3);
        boolean equals1 = column2.equalsIgnoreCase(column3);

        // Display the results of the equality checks.
        System.out.println("Does \"" + column1 + "\" equal \"" + column3 + "\"? - " + equals2);
        System.out.println("Does \"" + column2 + "\" equal \"" + column3 + "\"? - " + equals1);
    }
}

/* Python
if column3.lower() == column1.lower():
	print(f"Does \"{column1}\" equal \"{column3}\"? - True")
else:
	print(f"Does \"{column1}\" equal \"{column3}\"? - False")

if column3.lower() == column2.lower():
	print(f"Does \"{column2}\" equal \"{column3}\"? - True")
else:
	print(f"Does \"{column2}\" equal \"{column3}\"? - False")
 */