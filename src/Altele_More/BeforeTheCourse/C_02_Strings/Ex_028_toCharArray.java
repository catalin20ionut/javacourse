package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_028_toCharArray {
    public static void main(String[] args) {
        String str = "Java Exercises.";

        // Convert the above string to a char array.
        char[] arr = str.toCharArray();

        // Display the contents of the char array.
        System.out.println(arr);
    }
}
