package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_026_startsWith {
    public static void main(String[] args) {
        String str1 = "Red is favorite color.";
        String str2 = "Orange is also my favorite color.";
        String startStr = "Red";

        boolean starts1 = str1.startsWith(startStr);
        boolean starts2 = str2.startsWith(startStr);

        // Display the results of the startsWith calls.
        System.out.println("\"" + str1 + "\" starts with " + startStr + "? " + starts1);
        System.out.println("Does the sentence \"" + str2 + "\" start with " +  startStr + "? -> " + starts2);
    }
}

/* Python

str1 = "Red is favorite color."
str2 = "Orange is also my favorite color."
start_str = "Red";

print(str1.startswith("Red"))
print(str2.startswith("Red"))
 */