package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_022_length {
    public static void main(String[] args) {
        String str = "example.com";
        // Get the length of str.
        int len = str.length();
        System.out.println("The string length of '" + str + "' is: " + len);
    }
}