// 7. Write a Java program to concatenate a given string to the end of another string. See _Strings
package Altele_More.BeforeTheCourse.C_02_Strings;

public class Ex_007_Concatenation {
    public static void main(String[] args) {
        String str1 = "PHP Exercises and ";
        String str2 = "Python Exercises";
        String str3 = str1.concat(str2);
        System.out.println("1. The concatenated string: " + str3);
        System.out.println("2. The concatenated string: " + str1 + str2);
    }
}

/* Python
str1 = "PHP Exercises and "
str2 = "Python Exercises"
str3 = str1.join(str2);
print("1. The concatenated string: " + str3);
 */

