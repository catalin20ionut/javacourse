package Altele_More.BeforeTheCourse.C_04_Arrays___inPy_Tuples;

public class S3_multidimensional_array {
    public static void main(String[] args) {
        int[][] myNumbers = {{1, 2, 3, 4, 5}, {6, 7, 8, 9}, {10, 11, 12, 13, 14, 15}, {16, 17, 18, 19}};
        for (int[] myNumber : myNumbers) {
            for (int i : myNumber) {
                System.out.println("1. Option 1 >>> All the items of the main dimension array:" + i);
            }
        }

        for (int i = 0; i < myNumbers.length; i += 1) {
            for (int j = 0; j < myNumbers[i].length; j += 1) {
                System.out.println("1. Option 2 >>> All the items of the main dimension array:"
                        + myNumbers[i][j]);
            }
        }

        for (int i = 0; i < myNumbers.length; i += 2) {
            for (int j = 0; j < myNumbers[i].length; j += 1) {
                System.out.println("2. Step 2 --- Step 1: " + myNumbers[i][j]);

            }
        }

        for (int i = 0; i < myNumbers.length; i += 1) {
            for (int j = 0; j < myNumbers[i].length; j += 2) {
                System.out.println("2. Step 1 --- Step 2: " + myNumbers[i][j]);

            }
        }

        for (int i = 0; i < myNumbers.length; i += 2) {
            for (int j = 0; j < myNumbers[i].length; j += 2) {
                System.out.println("2. Step 2 --- Step 2: " + myNumbers[i][j]);

            }
        }
    }
}