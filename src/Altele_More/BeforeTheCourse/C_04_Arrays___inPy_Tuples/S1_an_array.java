package Altele_More.BeforeTheCourse.C_04_Arrays___inPy_Tuples;

import java.util.Arrays;                                                // Se folosește pentru a printa lista cars
public class S1_an_array {
    public static void main(String[] args) {
        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
        System.out.println("1. Printing an array in a list: " + Arrays.toString(cars));
        System.out.println(("2. Access the first item of the array: ") + cars[0]);

        cars[0] = "Opel";
        System.out.println("3. Changing the first element of the array: " + Arrays.toString(cars));

        System.out.println("4. Finding out the length of the array: " + cars.length);

        for (String car : cars) {
            System.out.println("5. Option 1 >>> Printing an array using an for loop: " + car);
        }
        for (int car = 0; car < cars.length; car++) {
            System.out.println("5. Option 2 -- Step 1 >>> Printing an array using an for loop: " + cars[car]);
            // Dacă folosesc car+=2 for nu va mai avea umbra galbenă (yellow shading). Vezi mai jos!
            // Se folosește pentru un pas mai mare sau egal cu doi.
        }
        for (int car = 0; car < cars.length; car+=2) {
            System.out.println("5. Option 2 -- Step 2 >>> Printing an array using an for loop: " + cars[car]);
        }
        for (int car = 1; car < cars.length; car+=2) {
            System.out.println("5. Option 2 -- Step 2 -- from the second item >>> Printing an array using an" +
                    "for loop: " + cars[car]);
        }
    }
}