package Altele_More.BeforeTheCourse.C_04_Arrays___inPy_Tuples;

import java.util.Arrays;
public class S2_an_array_with_integers {
    public static void main(String[] args) {
        int[] myNum = {10, 20, 30, 40};
        System.out.println("1. Printing an array: " + Arrays.toString(myNum));
    }
}
