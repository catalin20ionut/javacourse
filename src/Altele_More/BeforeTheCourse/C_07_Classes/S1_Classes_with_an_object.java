package Altele_More.BeforeTheCourse.C_07_Classes;

public class S1_Classes_with_an_object {
    int x = 5;

    public static void main(String[] args) {
        S1_Classes_with_an_object myObj = new S1_Classes_with_an_object();
        System.out.println("How to create a class with an object: " + myObj.x);
    }
}

/*
How to create a class with more objects
>>>>>>>>>>>>>>>>>>>>>>>>>
package Chapter_5_Classes;

public class S2_Classes_with_more_objects {
    int x = 5;

    public static void main(String[] args) {
        S2_Classes_with_more_objects myObj1 = new S2_Classes_with_more_objects();
        S2_Classes_with_more_objects myObj2 = new S2_Classes_with_more_objects();
        System.out.println("Creating the first object of the class: " + myObj1.x);
        System.out.println("Creating the second object of the class: " + myObj2.x);
    }
}
 */