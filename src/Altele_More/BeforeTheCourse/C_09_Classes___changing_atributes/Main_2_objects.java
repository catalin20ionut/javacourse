package Altele_More.BeforeTheCourse.C_09_Classes___changing_atributes;

public class Main_2_objects {
    int x = 5;

    public static void main(String[] args) {
        Main_2_objects myObj1 = new Main_2_objects();
        Main_2_objects myObj2 = new Main_2_objects();
        myObj2.x = 25;
        System.out.println(myObj1.x);
        System.out.println(myObj2.x);
    }
}
