package Altele_More.BeforeTheCourse.C_09_Classes___changing_atributes;

public class Main {
    int x = 10;                                                                // final int = 10;
    // If you don't want the ability to override existing values, declare the attribute as final:

    public static void main(String[] args) {
        Main myObj = new Main();
        myObj.x = 25; // x is now 25
        System.out.println(myObj.x);
    }
}
