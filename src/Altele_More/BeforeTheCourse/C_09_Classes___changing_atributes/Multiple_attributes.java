package Altele_More.BeforeTheCourse.C_09_Classes___changing_atributes;

public class Multiple_attributes {
    String firstName = "John";
    String lastName = "Doe";
    int age = 24;

    public static void main(String[] args) {
        Multiple_attributes myObj = new Multiple_attributes();
        System.out.println("His full name is " + myObj.firstName + " "
                + myObj.lastName + " and he's " + myObj.age + ".");
    }
}
