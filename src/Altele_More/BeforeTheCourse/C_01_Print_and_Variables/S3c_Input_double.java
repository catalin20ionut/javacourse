package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;

import java.util.Scanner;

public class S3c_Input_double {
    public static void main (String [] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input the first number: ");
        double num1 = input.nextDouble();
        System.out.print("Input the second number: ");
        double num2 = input.nextDouble();
        double sum = num1 + num2;
        System.out.println();              // printează un rând gol
        System.out.println("The sum of the two numbers is: " + sum);
    }
}

/*
Python the upper example >>>>>>>>>>>>>>>>>>>>>>>
number_1 = float(input("Input the first number: "))
number_2 = float(input("Input the second number: "))
print("The sum of the two numbers is:", number_1 + number_2)
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
*/