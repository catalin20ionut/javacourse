package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;

public class S1a_Math_and_random {
    public static void main(String[] args) {
        System.out.println("Maximum value: " + Math.max(5, 10));                  //               print(max(5, 10))
        System.out.println("Minimum value: " + Math.min(5, 10));                 //                print(min(5, 10))
        System.out.println("Radical din 64: " + Math.sqrt(64));                 // import math print(math. sqrt(64))
        System.out.println("Valoarea absolută: " + Math.abs(-64.6));           //                  print(abs(-64.6))

        int randomNum = (int)(Math.random() * 101);               // 101 reprezintă intervalul de la 0 la 100 inclusiv
        System.out.println("Număr aleatoriu, random integer: " + randomNum);
        float randomNumFloat = (float)(Math.random() * 101);
        System.out.println("Număr aleatoriu, random float: " + randomNumFloat);
    }
}
/* Python
import random

print(random.randrange(0, 101))                                                 # it generates an integer
print(random.randint(0, 25))                                                    # it generates an integer
print(random.uniform(0, 25))                                                    # it generates a float
print(round(random.uniform(0, 25), 4))                                          # it generates a float with 4 decimals
 */