package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;
import java.util.Scanner;           // pentru varianta 2

public class S1c_Math_sum_of_2_numbers {
    static int myMethod (int x, int y) {
        return x + y;
    }
    public static void main (String [] args) {
        System.out.println(myMethod(4, 5));

        // or
        Scanner input = new Scanner(System.in);
        System.out.print("Input the first number: ");
        int num1 = input.nextInt();
        System.out.print("Input the second number: ");
        int num2 = input.nextInt();
        int sum = num1 + num2;
        System.out.println();
        System.out.println("The sum of the two numbers is: " + sum);
    }
}

/*
def my_sum(a,b):
    return a + b
print(my_sum(4, 5))
 */