package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;

import java.util.Scanner;

public class S3a_Input_integer {
    public static void main (String [] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input the first number: ");
        int num1 = input.nextInt();
        System.out.print("Input the second number: ");
        int num2 = input.nextInt();
        int sum = num1 + num2;
        System.out.println();              // printează un rând gol
        System.out.println("The sum of the two numbers is: " + sum);
    }
}

/*
for input >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
import java.util.Scanner;

Scanner input = new Scanner(System.in);
        System.out.print("Input the first number: ");
        int num1 = input.nextInt();
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

Python for input >>>>>>>>>>>>>>>>>>>>
int(input("Exam result: "))
exam_result = int(input("Exam result: "))
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

Python the upper example >>>>>>>>>>>>>>>>>>>>>>>
number_1 = int(input("Input the first number: "))
number_2 = int(input("Input the second number: "))
print("The sum of the two numbers is:", number_1 + number_2)
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */