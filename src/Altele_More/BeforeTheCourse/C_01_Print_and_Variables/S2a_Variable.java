package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;

public class S2a_Variable {
    public static void main(String[] args) {
        String greeting = "Hello World!";
        System.out.println("The variable is a string: " + greeting);
    }
}

/* Python
greeting = "Hello World!"
print("The variable is a string:", greeting)
*/