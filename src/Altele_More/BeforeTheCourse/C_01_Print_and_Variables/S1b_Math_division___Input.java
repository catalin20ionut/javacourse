package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;
import java.util.Scanner;

public class S1b_Math_division___Input {
    public static void main(String[] args) {
        System.out.println("Împărțire, rezultat un întreg: " + 50/3);

        System.out.println();
        Scanner input = new Scanner (System.in);                            // for the examples with integer and float
        System.out.print("Input the first number: ");
        int a = input.nextInt();
        System.out.print("Input the second number: ");
        int b = input.nextInt();
        int d = (a/b);
        System.out.println("The division of a and b is the integer: " + d);

        System.out.println();
        System.out.print("Input the first number: ");
        float aFloat = input.nextFloat();
        System.out.print("Input the second number: ");
        float bFloat = input.nextFloat();
        float dFloat = (aFloat/bFloat);
        System.out.println("The division of a and b is the float: " + dFloat);
    }
}