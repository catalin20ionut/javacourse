package Altele_More.BeforeTheCourse.C_01_Print_and_Variables;

import java.util.Scanner;

public class S2b_More_Variables {
    public static void main(String[] args) {
        String firstName = "John";
        String lastName = "Doe";
        int age = 25;
        System.out.println("1. My full name is " + firstName + " " + lastName + " and I'm " + age + " years old.");

        Scanner input = new Scanner(System.in);
        System.out.print("First name: ");
        String firstName2 = input.nextLine();
        System.out.print("Last name: ");
        String lastName2 = input.nextLine();
        System.out.print("Last name: ");
        int age2 = input.nextInt();
        System.out.println("2. My full name is " + firstName2 + " " + lastName2 + " and I'm " + age2 + " years old.");
    }
}
/*
first_name = "John"
last_name = "Doe"
age = 25
print(f"Option 1: My full name is {first_name} {last_name} and I'm {age} years old.")
print("Option 2: My full name is {} {} amd I'm {} years old.".format("John", "Doe", 25))
print("Option 3: My full name is {0} {1} amd I'm {2} years old.".format("John", "Doe", 25))


first_name2 = input("First name: ")
last_name2 = input("Last name: ")
age2 = input("Age: ")
print("Option 4: My full name is " + first_name2 + " " + last_name2 + " and I'm " + age2 + " years old.")
*/