package Altele_More;

// https://www.baeldung.com/java-string-title-case
public class S02___TitleCase {
    public static String convertToTitleCase(String text) {
        if (text == null || text.isEmpty()) {
            return text;}

        StringBuilder converted = new StringBuilder();

        boolean convertNext = true;
        for (char ch : text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true;}
            else if (ch == '-' || ch == '@') {
                convertNext = true;}
            else if (convertNext) {
                ch = Character.toTitleCase(ch);
                convertNext = false;}
            else {
                ch = Character.toLowerCase(ch);}
            converted.append(ch);
        }
        return converted.toString();
    }

    public static void main(String[] args) {
        System.out.println(convertToTitleCase("anA aRe mERe m-seT meg@sAL"));
    }
}