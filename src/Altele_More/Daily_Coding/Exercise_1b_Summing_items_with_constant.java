package Altele_More.Daily_Coding;

import java.util.ArrayList;
import java.util.List;

// summing the items in an ArrayList with a constant
public class Exercise_1b_Summing_items_with_constant {
    public static void summingNumbersWithK(ArrayList<Integer> numberAnyArrayList, int k){
       for(int number : numberAnyArrayList) {
           System.out.println(number + k);
       }
    }
    public static void summingNumbersWithKAndDisplayAsAnArrayList(ArrayList<Integer> numberAnyArrayList, int k){
        List<Integer> finalList = new ArrayList<>();
        for(int number : numberAnyArrayList) {
            finalList.add(number + k);
        }
        System.out.println(finalList);
    }

    public static void main(String[] args) {
        ArrayList <Integer> numberMyArrayList = new ArrayList<>();
        numberMyArrayList.add(10);
        numberMyArrayList.add(15);
        numberMyArrayList.add(3);
        numberMyArrayList.add(7);
        summingNumbersWithK(numberMyArrayList, 7);
        summingNumbersWithKAndDisplayAsAnArrayList(numberMyArrayList, 7);
    }
}
