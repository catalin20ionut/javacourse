package Altele_More.Daily_Coding;

/** checking if the sum between two items in an array is equal with a constant
1. Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.  Bonus: Can you do this in one pass?  */
class Exercise_1 {
    public static boolean checkingSum(int[] anyArray, int k) {
        for (int i = 0; i < anyArray.length; i++) {
            for (int j = i + 1; j < anyArray.length; j++) {
                System.out.println("The sum of the items " + anyArray[i] + " and " + anyArray[j] + ": "
                        + (anyArray[i] + anyArray[j]));
                if (anyArray[i] + anyArray[j] == k) {
                    return true;}
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] myArray = {10, 15, 3, 7};
        System.out.println(checkingSum(myArray, 10));
    }
}
