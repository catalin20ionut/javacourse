package Altele_More.Daily_Coding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** calculating the product between the items in an array excepting the item at position i
2. Given an array of integers, return a new array such that each element at index i of the new array is the product of
all the numbers in the original array except the one at i. For example, if our input was [1, 2, 3, 4, 5], the expected
output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].          */

public class Exercise_2 {
    public static Integer[] productArray (int [] anyArray) {
        List<Integer> productList = new ArrayList<>();
        if (anyArray.length == 1) {
            System.out.print("The productArray cannot be formed! So it'll be empty. --> ");}
        else {
            int product = 1;
            for (int number: anyArray) {
                product *= number;}
            for (int number : anyArray) {
                productList.add(product / number);}
        }
        /* changing an arrayList into an array
        See also: public static >>> Integer[] <<< productArray (int [] anyArray) { */
        return productList.toArray(new Integer[0]);
    }

    public static void main(String[] args) {
        int[] myArray1 = {5};
        int[] myArray2 = {1, 2, 3, 4, 5};
        int[] myArray3 = {5, 4, 3, 2, 1};
        int[] myArray4 = {3, 2, 1};
        System.out.println(Arrays.toString(productArray(myArray1)));
        System.out.println("For myArray2 --> " + Arrays.toString(productArray(myArray2)));
        System.out.println("For myArray3 --> " + Arrays.toString(productArray(myArray3)));
        System.out.println("For myArray4 --> " + Arrays.toString(productArray(myArray4)));
    }
}
