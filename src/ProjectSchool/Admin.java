package ProjectSchool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import static ProjectSchool.Utile.generatingPassword;

public class Admin extends User {
    public Admin(String firstNameAdmin, String lastNameAdmin, String emailAdmin) {
        super(firstNameAdmin, lastNameAdmin, emailAdmin);
    }

    public String generatingPasswordForAdmin() {
        return generatingPassword();
    }
    public void writingAdministrator() {
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\ProjectSchool\\Users\\FileAdministrator.txt");
            if (!file.exists()) {
                file.createNewFile();}
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(fw);

            bf.write("Username is: " + getUserName() + "\n");
            bf.write("Password: " + generatingPasswordForAdmin());
            bf.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

//    public String generatingPasswordForUser(User user) {
//        return generatingPassword();
//    }

    public String generatingPasswordForUser(User user) {
        Random random = new Random();
        String digits = "0123456789";
        String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" + "!@#$%^&*()";
        int passwordLength = random.nextInt(6) + 6;
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < passwordLength; i++) {
            int digitOrLetter = random.nextInt(2);
            int randomDigit = random.nextInt(digits.length());
            int randomLetter = random.nextInt(letters.length());

            if (digitOrLetter == 0) {
                password.append(digits.charAt(randomDigit));
            } else {
                password.append(letters.charAt(randomLetter));
            }

        }
        if (user.getPassword() == null) {
            user.setPassword(password.toString());
        }
        return password.toString();
    }

    public void writingUser(User user) { //ok
        try {
            File file = new File("E:\\Java\\IntelliJ\\JavaCourse\\src\\ProjectSchool\\Users\\"
                    + user.generatingUserNameUser() + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bf = new BufferedWriter(fw);
            bf.write("Username is: " + user.getUserName() + "\n");
            bf.write("Password is: " + generatingPasswordForUser(user));

            bf.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}