package ProjectSchool;


import java.util.Date;

public abstract class User {
    private String email;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;

    public User(String email, String firstName, String lastName) {
//        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = generatingUserNameUser();
    }

    public String getEmail() {
        return firstName.toLowerCase() + lastName.toLowerCase() + "@gmail.com";
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstNameUser() {
        return firstName;
    }
    public String getLastNameUser() {
        return lastName;
    }
    public String getFullName() {
        return firstName + " " + lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
    public String generatingUserNameUser() {
        Date date = new Date();
        long unic = date.getTime();
        return this.firstName.toLowerCase() + this.lastName.toUpperCase() + unic;
    }
}