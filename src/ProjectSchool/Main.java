package ProjectSchool;


public class Main {
    public static void main(String[] args) {
        Admin admin = new Admin("Ioana", "Visinescu", "ioanavisinescu@gmail.com");
        admin.writingAdministrator();


        Accountant accountant = new Accountant("George", "Marin", "georgemarin@gmail.com");
        admin.generatingPasswordForUser(accountant);
        admin.writingUser(accountant);
//        System.out.println(accountant.getFirstName() + " " + accountant.getLastName() + " --> accountant password: " + admin.generatingPasswordForUser(accountant));

        Parent parent = new Parent("Cristi", "Popescu", "cristipopescu@gmail.com");
        admin.generatingPasswordForUser(parent);
        admin.writingUser(parent);

        Pupil pupil1 = new Pupil("Darius", "Graur", "dariusgraur@gmail.com");
        admin.generatingPasswordForUser(pupil1);
        admin.writingUser(pupil1);

        Teacher teacher1 = new Teacher("Alin", "Roman", "alinroman@gmail.com");
        admin.generatingPasswordForUser(teacher1);
        admin.writingUser(teacher1);
    }
}
