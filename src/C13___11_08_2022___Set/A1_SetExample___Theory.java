package C13___11_08_2022___Set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class A1_SetExample___Theory {
    public static void main(String[] args) {
//        Set<String> set1 = new HashSet<>();                         //                     The items are nor ordered.
//        Set<String> set1 = new LinkedHashSet<>();                  //       Elements remain in order they were added.
        Set<String> set1 = new TreeSet<>();                         // It arranges the items in the alphabetical order.
        if (set1.isEmpty()) {
            System.out.println("1. The set has no element.");}
        set1.add("dog");             // The eleventh row is "declaring a set". The 12th row is adding items to the set.
        set1.add("cat");
        set1.add("bear");
        set1.add("chicken");
        set1.add("lion");
        if (!set1.isEmpty()) {
            System.out.println("2. The set contains elements: " + set1);}
        set1.add("lion");
        System.out.println("3. The items in a set doesn't repeat: " + set1);
        set1.add("tiger");
        System.out.println("4. After adding tiger: " + set1);

        for(String currentElement: set1) {
            System.out.println("5. Item in set1: " + currentElement);}

        if(set1.contains("bear")){
            System.out.println("6.The sought item exits in this set.");}
        if(!set1.contains("marmot")){
            System.out.println("7. The sought item doesn't exit in this set.");}

        Set<String> set2 = new HashSet<>();
        set2.add("ficus");
        set2.add("rose");
        set2.add("tulip");
        set2.add("orchid");
        set2.add("cactus");
        set2.add("dog");
        set2.add("tiger");

        Set<String> intersection = new HashSet<>(set1);
        intersection.retainAll(set2);
        System.out.println("8. Common items: " + intersection);

        Set<String>  difference = new HashSet<>(set1);
        difference.removeAll(set2);
        System.out.println("9.Items in set1 that don't exist in set2: " + difference);
    }
}