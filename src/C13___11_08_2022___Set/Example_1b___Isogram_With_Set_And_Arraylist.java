package C13___11_08_2022___Set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**    Determine if a word or phrase is an isogram. An isogram, also known as a "non-pattern word", is a word or phrase
without a repeating letter, however spaces and hyphens are allowed to appear multiple times.
Examples of isograms: lumberjacks, background, downstream, six-year-old. The word isograms is not an isogram, because
the letter "s" repeats.                                                                                              */

public class Example_1b___Isogram_With_Set_And_Arraylist {
    public static boolean isIsogram2(String s) {
        if (s.isEmpty()) {
            return true;}
        String isogram = s.toLowerCase().replace(" ", "").replace("-", "");
        List<Character> characterList = new ArrayList<>();
        for (char element:isogram.toCharArray()) {
            characterList.add(element);}
        Set<Character> characterSet = new HashSet<>(characterList);     // (characterList) we changed the list into set
        if(characterSet.size() == characterList.size()){
            return true;}
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isIsogram2("Alex"));
        System.out.println(isIsogram2("America"));
        System.out.println(isIsogram2("Al ex"));
        System.out.println(isIsogram2("Al-ex"));
        System.out.println(isIsogram2("Al  ex"));
        System.out.println(isIsogram2("  "));
        System.out.println(isIsogram2(""));
    }
}
