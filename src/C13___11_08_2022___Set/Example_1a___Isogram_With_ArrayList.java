package C13___11_08_2022___Set;

import java.util.ArrayList;
import java.util.List;

/**    Determine if a word or phrase is an isogram. An isogram, also known as a "non-pattern word", is a word or phrase
without a repeating letter, however spaces and hyphens are allowed to appear multiple times.
Examples of isograms: lumberjacks, background, downstream, six-year-old. The word isograms is not an isogram, because
the letter "s" repeats.                                                                                              */

public class Example_1a___Isogram_With_ArrayList {
    public  static boolean isIsogram(String s) {
        /* The code in the 16th, 17th and 18th rows doesn't work for empty spaces. For an empty space it works. This or
        the 19th row. That works for all kinds of empty spaces.
        if (s.isEmpty()) {
            return true;}
        String isogram = s.toLowerCase(); */
        String isogram = s.toLowerCase().replace(" ","").replace("-", "");
        List<Character> myCharList = new ArrayList<>();
        for (char element:isogram.toCharArray()) {          // .toCharArray - changes an array of a character into char
            if(myCharList.contains(element)) {
                return false;}
            else {
                myCharList.add(element);}
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isIsogram("Alex"));
        System.out.println(isIsogram("America"));
        System.out.println(isIsogram("Al ex"));
        System.out.println(isIsogram("Al-ex"));
        System.out.println(isIsogram("Al  ex"));
        System.out.println(isIsogram("  "));
        System.out.println(isIsogram(""));
    }
}
