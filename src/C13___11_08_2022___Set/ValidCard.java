package C13___11_08_2022___Set;

import java.util.ArrayList;
import java.util.List;

/** Given a number, determine whether it is valid or not per the Luhn formula. The Luhn algorithm is a simple checksum
formula used to validate a variety of identification numbers, such as credit card numbers and Canadian Social Insurance
Numbers. The task is to check if a given string is valid.

Validating a Number:
Strings of length 1 or less are not valid. Spaces are allowed in the input, but they should be stripped before checking.
All other non-digit characters are disallowed.

Example 1: valid credit card number 4539 1488 0343 6467. The first step of the Luhn algorithm is to double every second
digit, starting from the right. We will be doubling 43 18 04 66. If doubling the number results in a number greater than
9 then subtract 9 from the product. The results of our doubling: 8569 2478 0383 3437. Then sum all the digits:
8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80. If the sum is evenly divisible by 10, then the number is valid. This number is
valid!

Example 2:            invalid credit card number 8273 1232 7352 0569. Double the second digits, starting from the right
7253 2262 5312 0539. Sum the digits     7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57. 57 is not evenly divisible by 10, so this
number is not valid.                                                                                                 */
public class ValidCard {
    private static boolean isContainOnlyDigitAndSpaceAndIsAValidNumber(String candidate) {
        // Aici verificăm dacă există numărul de caractere necesar, în caz că nu este, atunci returnăm false
        if (candidate.length() <= 1) {
            return false;}

        // eliminăm spațiile pentru că sunt caractere acceptate
        String digitsWithoutSpace = candidate.replace(" ", "");
        for (int i = 0; i < digitsWithoutSpace.length(); i++) {
            // În timp ce parcurgem caracterele rămase după eliminarea spațiilor, ne focusăm să vedem dacă măcar un
            // singur caracter este diferit de cifră. În cazul în care găsim caracter diferit atunci returnăm false.
            if (!(Character.isDigit(digitsWithoutSpace.charAt(i)))) {
                return false;}
        }

        // În cazul în care ajunge să fie îndeplinite toate condițiile anterioare mergem să verificăm dacă putem să
        // facem suma și modulo, plus dublarea caracterelor necesare
        return isValidNumberNumberAfterSumAndModulo(digitsWithoutSpace);
    }

    private static boolean isValidNumberNumberAfterSumAndModulo(String digitsWithoutSpace) {
        // Declarăm o colecție în care ținem fiecare caracter numeric din stringul fără spații
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i <= digitsWithoutSpace.length() - 1; i++) {  //înseram în colecția de tip List fiecare cifră
            integers.add(Character.getNumericValue(digitsWithoutSpace.charAt(i)));}
        // Pasăm ca și parametru noua noastră colecție de cifre, ca sa putem face calculele matematice
        int sum = getSumOfDigits(integers);
        return sum % 10 == 0;    // După ce am calculat suma necesară mai facem verificarea împărțirii exacte la 10.
    }

    private static int getSumOfDigits(List<Integer> integers) {
        int sum = 0;
        // Parcurgem lista de cifre
        for (int i = 1; i <= integers.size(); i++) { //luăm elementul current din listă, dar de la stânga la dreapta
            int elem = integers.get(integers.size() - i);
            // dacă elementul curent, dar luat tot al doilea element de la stânga la dreapta îndeplinește condiția
            // să fie pe poziție pară, atunci facem dublarea cifrei; adaugăm valoarea elementului la suma finală
            if (i % 2 == 0) {
                sum += doubleDigit(elem);}
            else {
                sum += elem;}
        }
        return sum;
    }

    private static int doubleDigit(int digit) {
        int doubled = digit * 2;                                             //dublam elementul
        // Dacă după ce am dublat elementul suma este mai mare decât 9 atunci scădem valoarea 9.
        // În caz că nu depășim valoarea 9, returnăm valoarea dublată
        return doubled > 9 ? doubled - 9 : doubled;
    }

    public static void main(String[] args) {
        System.out.println(isContainOnlyDigitAndSpaceAndIsAValidNumber("4539 1488 0343 6467"));
        System.out.println(isContainOnlyDigitAndSpaceAndIsAValidNumber("8273 1232 7352 0569"));
    }
}

