package C13___11_08_2022___Set;

import java.util.*;

public class X1___StringToSet_SetToString {
    public static void changingStringIntoSetAndBack (String anyString) {
        // converting a string to set; changing a string into a set
        String[] charactersOfTheString = anyString.split("");
        List<String> characterArrayListOfAnyString = Arrays.asList(charactersOfTheString);
        System.out.println("1. ArrayList: " + characterArrayListOfAnyString);
        Set<String> anyStringAsSet = new TreeSet<>(characterArrayListOfAnyString);
        System.out.println("2. Set      : " + anyStringAsSet);
        // changing a set into a string; converting a set to a string
        StringBuilder uniqueLettersAsString = new StringBuilder(anyStringAsSet.toString().replaceAll(",|\\[|]|\\s", ""));
        System.out.println("3. String   : " + uniqueLettersAsString);
        // the letters in a string
        for (int i = 0; i < uniqueLettersAsString.length(); i++) {
            System.out.println("4. The letters in the string: " + uniqueLettersAsString.charAt(i));
        }
    }

    public static void main(String[] args) {
        String myString = "race";
        changingStringIntoSetAndBack(myString);
    }
}