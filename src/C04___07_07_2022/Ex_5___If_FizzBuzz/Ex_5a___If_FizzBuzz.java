package C04___07_07_2022.Ex_5___If_FizzBuzz;

public class Ex_5a___If_FizzBuzz {
    public void If___FizzBuzz (int number){
        String result = "";
        if(number % 3 == 0){
            result += "Fizz";
            System.out.println("S-a executat verificarea la 3: " + result);}

        if(number % 5 == 0){
            result += "Buzz";
            System.out.println("S-a executat verificarea la 5: " + result);}

        if(result.equals("")){
            System.out.println("Nu este divizibil nici cu 3 nici cu 5.");}
    }

    public static void main(String[] args) {
        Ex_5a___If_FizzBuzz fB = new Ex_5a___If_FizzBuzz();
        fB.If___FizzBuzz (3);
        fB.If___FizzBuzz (5);
        fB.If___FizzBuzz (30);
        fB.If___FizzBuzz (43);
    }
}