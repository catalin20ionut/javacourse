package C04___07_07_2022.Ex_4___BankAccount;


public class Ex_4___BankAccount {
    private String nr;
    private double balance;
    private String name;
    private String email;
    private String phoneNumber;

    public Ex_4___BankAccount(){
        this("ro1234", 100.20, "Gabriel", "gabi@gmail.com", "+040751148");
        System.out.println("1. The first constructor: ");
    }

    public Ex_4___BankAccount(String iban, double balance, String name, String email, String phoneNumber){
        this.nr = iban;
        this.balance = balance;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        System.out.println("2. The second constructor: ");
    }

    public void DepositMoney (double amountDeposited){
        this.balance += amountDeposited;
        System.out.println("3. You deposited the amount of " + amountDeposited + " euro.");
        System.out.println("4. The total amount is " + this.balance + " euros.");
    }

    public void WithdrawMoney (double withdrawnAmount){
        if (withdrawnAmount <= this.balance){
            this.balance -= withdrawnAmount;
            System.out.println("5. You withdrew the amount of " + withdrawnAmount + " euro.");
            System.out.println("6. After withdrawal the total amount is " + this.balance + " euro.");
        }
        else {
            System.out.println("7. You don't have enough money in your account!");
        }
    }

    public static void main(String[] args) {
       Ex_4___BankAccount contulMeu = new Ex_4___BankAccount();
       contulMeu.DepositMoney(10.12);
       contulMeu.DepositMoney(25);
       contulMeu.DepositMoney(55);
       contulMeu.WithdrawMoney(25.32);
       contulMeu.WithdrawMoney(65);
       contulMeu.WithdrawMoney(100);
       contulMeu.DepositMoney(55);
       contulMeu.WithdrawMoney(100);
    }
}
