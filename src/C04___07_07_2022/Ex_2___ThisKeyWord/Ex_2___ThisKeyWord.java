package C04___07_07_2022.Ex_2___ThisKeyWord;

public class Ex_2___ThisKeyWord {
    int a;
    int b;

    public Ex_2___ThisKeyWord(int a, int b){
        this.a = a;
        this.b = b;
    }

    void calculateSum(int a, int b){
        int c = 10;
        this.a = a;
        this.b = b;
        System.out.println("1.1 The calculated sum is --> " + (a + b + c));
    }

    void show(){
        System.out.println("2.1 --> " + a);
        System.out.println("2.2 --> " + b);
        System.out.println("2.3 The showed sum sum is --> " + (a + b));
    }

    public static void main(String[] args) {
        Ex_2___ThisKeyWord key1 = new Ex_2___ThisKeyWord(3, 4);
        key1.show();
        key1.calculateSum(5, 5);
    }
}
