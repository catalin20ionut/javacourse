package C04___07_07_2022.Homework_2.Exercise_3;

/**
3. Define three int numbers, give them 3 values, check and print in console which one is the largest. If a is the
largest print “a is the largest number”, if b is the largest print “b is the largest number” otherwise print “c is the
largest”
 */
public class Exercise_3 {
    static void Comparator (int a, int b, int c){
        if (a > b  && a > c) {
            System.out.println("a is the largest number");
        }
        else if (b > a && b > c) {
            System.out.println("b is the largest number");
        }
        else if (c > a && c > b) {
            System.out.println("c is the largest number");
        }
        else {
            System.out.println("The numbers are equal.");
        }
    }
    public static void main(String[] args) {
        Comparator(6, 7, 14);
        Comparator(25, 17, 24);
        Comparator(25, 47, 25);
        Comparator(25, 25,25);
    }
}
