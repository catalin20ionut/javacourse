package C04___07_07_2022.Homework_2.Exercise_2;

/**
2. Create a new class for a C15___01_09_2022.Example_2___Vehicle.Car, create fields like model, engine, colour, productionYear, carPrice
- when you want to buy a car, allow to buy only from your top five favorite models (you'll choose your top five models)
- before buying the model make sure that it doesn't matter how the model is entered when you use the C15___01_09_2022.Example_2___Vehicle.Car object, won't
be case-sensitive; basically if you try to buy a FIAT/Fiat/fiat you will be allowed to buy it if this is a favorite car
- if the model selected is not a favorite one, print that the model is unknown, and you can not buy that car
- restrict not to be able to buy if the productionYear is strict before 2000, if the year is before print that you can't
buy the car
- define a maximum price that you can use to buy a car, in case that you will exceed that maximum price, print that you
don’t have enough money for the car.
 */
public class Car {
    private final String model;
    private final int productionYear;
    private final int carPrice;

    public Car(String model, int productionYear, int carPrice){
        this.model = model;
        this.productionYear = productionYear;
        this.carPrice = carPrice;

        String model1 = "Rolls-Royce";
        String model2 = "Bugatti";
        String model3 = "Pagani";
        String model4 = "Mercedes";
        String model5 = "Ferrari";

        if (!model.equalsIgnoreCase(model1)
            && !model.equalsIgnoreCase(model2)
            && !model.equalsIgnoreCase(model3)
            && !model.equalsIgnoreCase(model4)
            && !model.equalsIgnoreCase(model5)){
            System.out.println("Write correctly!");}
        else if (productionYear < 2000 && carPrice > -1) {
            System.out.println("You are not allowed to buy that car!");}
        else if (productionYear > 1999 && carPrice > 3500000) {
            System.out.println("You don't have enough money!");}
        else {
            System.out.println("Congratulation!");}
    }

    public static void main(String[] args) {
        System.out.println("For car1 >>>> ");
        Car car1 = new Car("roLLs-RoYCE", 2000, 120000);
        int i = car1.model.indexOf("-");
        System.out.println("Congratulation! You found what you were looking for!\nThis is a "
                + car1.model.substring(0,1).toUpperCase()
                + car1.model.substring(1,i).toLowerCase() + "-"
                + car1.model.substring(i+1, i+2).toUpperCase()
                + car1.model.substring(i+2, car1.model.length()-1).toLowerCase()
                + Character.toLowerCase(car1.model.charAt(car1.model.length()-1))
                + " from " + car1.productionYear + ". You have to pay for it " + car1.carPrice + " euro.");
                // Rows 49 - 53 change "roLLs-RoYCE" into "Rolls-Royce"; it can also do it with "Mercedes-Benz" aso.

        System.out.println("For car2 >>>> ");
        Car car2 = new Car("bugATTI", 2005, 1500000);
        System.out.println("Congratulation! You found what you were looking for!\nThis is a "
                + Character.toUpperCase(car2.model.charAt(0))
                + car2.model.substring(1).toLowerCase()
                + " from " + car2.productionYear + ". You have to pay for it " + car2.carPrice + " euro.");
    }
}
