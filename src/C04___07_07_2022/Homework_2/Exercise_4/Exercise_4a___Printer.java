package C04___07_07_2022.Homework_2.Exercise_4;
import java.util.Objects;

/**
4. Create a class called Printer. It will simulate a real Computer Printer. It should have fields for the toner level,
number of pages printed, and also whether it is a duplex printer (capable of printing on both sides of the paper). Add
methods to fill up the toner (up to a maximum of 100%), another method to simulate printing a page which should increa-
se the number of pages printed. Decide on the scope, whether to use constructors, and anything else you think is needed.
*/
public class Exercise_4a___Printer {
    private final int numberOFPagesPrinted;
    private final int tonerLevel;


    public Exercise_4a___Printer(String duplexPrinter, int tonerLevel, int numberOFPagesPrinted){
        this.numberOFPagesPrinted = numberOFPagesPrinted;
        this.tonerLevel = tonerLevel;

        if(Objects.equals(duplexPrinter, "Yes")) {
            System.out.println("The printer can print on the both sides of the sheet.");}
        else if (Objects.equals(duplexPrinter, "No")) {
            System.out.println("The printer can print on only a side of the sheet.");}

        // Supposing a page consumes 1% of the toner's level
        if (tonerLevel == 0){
            System.out.println("Fill up the toner!");}
        /* If the level of the toner is bigger than the number of the pages or equal the printer can print the pages
        this.numberOFPagesPrinted represents the numbers of pages that will be printed; the number that will be entered
        at object "Exercise_4___Printer" (41-st row numberOfPagesPrinted) */
        else if (tonerLevel > 0 && tonerLevel < 101 && tonerLevel >= this.numberOFPagesPrinted) {
            numberOFPagesPrinted = 0;
            // We will receive the message with the number of printed pages
            while (tonerLevel > numberOFPagesPrinted) {
                if (this.numberOFPagesPrinted > numberOFPagesPrinted){
                    System.out.println("Numbers of pages printed: " + (numberOFPagesPrinted + 1)) ;}
                numberOFPagesPrinted ++;}
                }
        // In case the number of the pages is bigger than the level of the toner
        else if (this.numberOFPagesPrinted > tonerLevel){
            System.out.println("Look out! You will be able to print only " + tonerLevel + " pages of "
                                                                                  + this.numberOFPagesPrinted + "." );}
        else {
            System.out.println("Error!");}
    }
    public static void main (String[] args) {
        Exercise_4a___Printer printer1 = new Exercise_4a___Printer("Yes",10, 10);
        if (printer1.tonerLevel == 0)
            System.out.println("The printer cannot print the " + printer1.numberOFPagesPrinted + " pages.");
        else if (printer1.tonerLevel < printer1.numberOFPagesPrinted)
            System.out.println();
        else
            System.out.println("The printer printed the " + printer1.numberOFPagesPrinted + " pages.");


        // The following cases are commented in order to be avoided the too big result.

//        Exercise_4a___Printer printer2 = new Exercise_4a___Printer("No",0, 10);
//        if (printer2.tonerLevel == 0)
//            System.out.println("The printer cannot print the " + printer2.numberOFPagesPrinted + " pages.");
//        else if (printer2.tonerLevel < printer2.numberOFPagesPrinted)
//            System.out.println("");
//        else
//            System.out.println("The printer printed the " + printer2.numberOFPagesPrinted + " pages.");
//
//
//        Exercise_4a___Printer printer3 = new Exercise_4a___Printer("Yes",25, 14);
//        if (printer3.tonerLevel == 0)
//            System.out.println("The printer cannot print the " + printer3.numberOFPagesPrinted + " pages.");
//        else if (printer3.tonerLevel < printer3.numberOFPagesPrinted)
//            System.out.println();
//        else
//            System.out.println("The printer printed the " + printer3.numberOFPagesPrinted + " pages.");
//
//
//        Exercise_4a___Printer printer4 = new Exercise_4a___Printer("No",30, 40);
//        if (printer4.tonerLevel == 0)
//            System.out.println("The printer cannot print the " + printer4.numberOFPagesPrinted + " pages.");
//        else if (printer4.tonerLevel < printer4.numberOFPagesPrinted)
//            System.out.println("");
//        else
//            System.out.println("The printer printed the " + printer4.numberOFPagesPrinted + " pages.");

    }
}
