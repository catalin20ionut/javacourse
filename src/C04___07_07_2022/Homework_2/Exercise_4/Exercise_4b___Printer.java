package C04___07_07_2022.Homework_2.Exercise_4;

/**
4. Create a class called Printer. It will simulate a real Computer Printer. It should have fields for the toner level,
number of pages printed, and also whether it is a duplex printer (capable of printing on both sides of the paper). Add
methods to fill up the toner (up to a maximum of 100%), another method to simulate printing a page which should increa-
se the number of pages printed. Decide on the scope, whether to use constructors, and anything else you think is needed.
*/

/* Here the count of the pages increases when we give the command
    hp.print(12); --> Pages printed pages: 12
    hp.print(4);  --> Pages printed pages: 16                                                                        */


public class Exercise_4b___Printer {
    private int tonerLevel;
    private int pagesPrinted;

    public Exercise_4b___Printer(int tonerLevel, int pagesPrinted) {
        this.tonerLevel = tonerLevel;
        this.pagesPrinted = pagesPrinted;
    }

    public void fillToner(int tonerQuantity) {
        if (this.tonerLevel == 100) {
            System.out.println("The toner level is already full");}
        else if (this.tonerLevel + tonerQuantity > 100) {
            this.tonerLevel = 100;
            System.out.println("Toner in printer filled up. Extra toner left: " + (tonerQuantity - this.tonerLevel));}
        else {
            this.tonerLevel += tonerQuantity;
            System.out.println("Toner added. The toner level is now: " + this.tonerLevel);}
    }

    public void print(int pages) {
        if (this.tonerLevel > 0) {
            System.out.println("Printing....");
            this.pagesPrinted += pages;
            System.out.println("Pages printed pages: " + this.pagesPrinted);}
        else {
            System.out.println("Toner level too low.");}
    }

    public static void main(String[] args) {
        Exercise_4b___Printer hp = new Exercise_4b___Printer(56, 0);
        hp.print(12);
        hp.print(4);
        hp.fillToner(20);
    }
}
