package C04___07_07_2022.Homework_2.Exercise_5;

/**
5. Create a method called isEvenNumber that takes a parameter of type int. Its purpose is to determine if the argument
passed to the method is an even number or not, print true if an even number, otherwise print false;
*/
public class Exercise_5 {
    static void isEvenNumber (int myNumber) {
        if (myNumber % 2 == 0) {
            System.out.println("true");
        }
        else {
            System.out.println("false");
        }
    }

    public static void main(String[] args) {
        isEvenNumber(8);
        isEvenNumber(9);
    }
}
