package C04___07_07_2022.Homework_2.Exercise_1;

/**
1. Convert a number to a string, the contents of which depend on the number's factors. If the number has 3 as a factor,
output 'Pling'. If the number has 5 as a factor, output 'Plang'. If the number has 7 as a factor, output 'Plong'.
If the number does not have 3, 5, or 7 as a factor,just pass the number's digits straight through.
Examples:
28's factors are 1, 2, 4, 7, 14, 28. In raindrop-speak, this would be a simple "Plong".
30's factors are 1, 2, 3, 5, 6, 10, 15, 30. In raindrop-speak, this would be a "PlingPlang".
34 has four factors: 1, 2, 17, and 34. In raindrop-speak, this would be "34".
*/
public class Exercise_1a {
    static void raindropSpeak(int number){
        if (number % 105 == 0){
            System.out.println("PlingPlangPlong");}
        else if (number % 15 == 0){
            System.out.println("PlingPlang");}
        else if (number % 21 == 0){
            System.out.println("PlingPlong");}
        else if (number % 35 == 0){
            System.out.println("PlangPlong");}
        else if (number % 3 == 0){
            System.out.println("Pling");}
        else if (number % 5 == 0){
            System.out.println("Plang");}
        else if (number % 7 == 0){
            System.out.println("Plong");}
        else {
            System.out.println(number);}
    }
    public static void main(String[] args) {
        raindropSpeak(210);
        raindropSpeak(30);
        raindropSpeak(42);
        raindropSpeak(70);
        raindropSpeak(6);
        raindropSpeak(10);
        raindropSpeak(14);
        raindropSpeak(28);
        raindropSpeak(34);
    }
}
