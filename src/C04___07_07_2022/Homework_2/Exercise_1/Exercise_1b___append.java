package C04___07_07_2022.Homework_2.Exercise_1;

/**
1. Convert a number to a string, the contents of which depend on the number's factors. If the number has 3 as a factor,
output 'Pling'. If the number has 5 as a factor, output 'Plang'. If the number has 7 as a factor, output 'Plong'.
If the number does not have 3, 5, or 7 as a factor,just pass the number's digits straight through.
Examples:
28's factors are 1, 2, 4, 7, 14, 28. In raindrop-speak, this would be a simple "Plong".
30's factors are 1, 2, 3, 5, 6, 10, 15, 30. In raindrop-speak, this would be a "PlingPlang".
34 has four factors: 1, 2, 17, and 34. In raindrop-speak, this would be "34".
*/
public class Exercise_1b___append {
    public static void raindropSpeak(int number) {
        StringBuilder output = new StringBuilder();
        if (number % 3 == 0) {
            output.append("Pling");}
        if (number % 5 == 0) {
            output.append("Plang");}
        if (number % 7 == 0) {
            output.append("Plong");}

        if (number % 3 != 0 && number % 5 != 0 && number % 7 != 0 ) {
            System.out.println(number);}
        else {
            System.out.println(output); }
    }

    public static void main(String[] args) { raindropSpeak(11); }
}
