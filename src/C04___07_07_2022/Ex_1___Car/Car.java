package C04___07_07_2022.Ex_1___Car;

public class Car {
    private String name;
    private String model;
    private int yearFabrication;


    public Car (){
        this("Dacia", 2000);
        System.out.println("The first constructor.");
    }

    public Car(String nameCar, int year){
        this("Fiat", 2001, "Punto");
        this.name= nameCar;
        this.yearFabrication = year;
        System.out.println("The second constructor.");
    }

    public Car(String name, int year, String model){
        System.out.println("The third constructor.");
        this.name= name;
        this.yearFabrication= year;
        this.model = model;
    }
}