package C04___07_07_2022.Ex_1___Car;

public class AppCar {
    public static void main(String[] args) {
        Car car1= new Car();                   // The third constructor. • The second constructor. • First constructor.
        Car car2= new Car("Matiz", 2000);              // The third constructor. • The second constructor.
        Car car3= new Car("BMW", 2003, "x5");                                // The third constructor.
    }
}
