public class Writing {
    public static int sumD(int n) {
       if (n == 0) {
          return n;}
       else {
          return (n % 10) + sumD( n / 10);}
    }

    public static void main(String[] args) {
        System.out.println("The sum is " + sumD(123456789));
    }
}