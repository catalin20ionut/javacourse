package C18___13_09_2022__Design_Patterns.behavioral.visitorDesignPattern;

public interface ComputerPartVisitor {
    void visit(Computer computer);
    void visit(Mouse mouse);
    void visit(Keyboard keyboard);
    void visit(Monitor monitor);
}
