package C18___13_09_2022__Design_Patterns.behavioral.visitorDesignPattern;

public interface ComputerPart {
    void accept(ComputerPartVisitor computerPartVisitor);
}