package C18___13_09_2022__Design_Patterns.behavioral.visitorDesignPattern;

public class Monitor implements ComputerPart {

    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        computerPartVisitor.visit(this);
    }
}
