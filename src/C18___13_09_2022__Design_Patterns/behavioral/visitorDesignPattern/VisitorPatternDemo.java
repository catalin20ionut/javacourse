package C18___13_09_2022__Design_Patterns.behavioral.visitorDesignPattern;

public class VisitorPatternDemo {
    public static void main(String[] args) {

        ComputerPart computer = new Computer();
        computer.accept(new ComputerPartDisplayVisitor());
    }
}
