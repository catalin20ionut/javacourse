package C18___13_09_2022__Design_Patterns.behavioral.stateDesignPattern;

public class StopState implements State {

    public void doAction(Context context) {
        System.out.println("Player is in stop state");
        context.setState(this);
    }

    public String toString(){
        return "Stop State";
    }
}
