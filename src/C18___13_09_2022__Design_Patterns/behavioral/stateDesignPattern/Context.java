package C18___13_09_2022__Design_Patterns.behavioral.stateDesignPattern;

public class Context {
    private State state;

    public Context(){
        state = null;
    }

    public void setState(State state){
        this.state = state;
    }

    public State getState(){
        return state;
    }
}
