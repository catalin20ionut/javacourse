package C18___13_09_2022__Design_Patterns.behavioral.stateDesignPattern;

public interface State {
    void doAction(Context context);
}
