package C18___13_09_2022__Design_Patterns.behavioral.stateDesignPattern;

public class StartState implements State {

    public void doAction(Context context) {
        System.out.println("Player is in start state");
        context.setState(this);
    }

    public String toString(){
        return "Start State";
    }
}
