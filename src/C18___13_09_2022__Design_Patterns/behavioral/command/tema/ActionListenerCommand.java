package C18___13_09_2022__Design_Patterns.behavioral.command.tema;

public interface ActionListenerCommand {
    void execute();
}
