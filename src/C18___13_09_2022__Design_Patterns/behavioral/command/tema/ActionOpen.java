package C18___13_09_2022__Design_Patterns.behavioral.command.tema;

public class ActionOpen implements ActionListenerCommand{
    private final Document doc;
    public ActionOpen(Document doc) {
        this.doc = doc;
    }
    @Override
    public void execute() {
        doc.open();
    }
}
