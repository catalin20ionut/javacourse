package C18___13_09_2022__Design_Patterns.behavioral.command.tema;

public class Document {
    public void open(){
        System.out.println("Document Opened");
    }
    public void save(){
        System.out.println("Document Saved");
    }
}
