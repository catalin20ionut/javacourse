package C18___13_09_2022__Design_Patterns.behavioral.command;

public class SellStock implements Order {
    private final Stock abcStock;

    public SellStock(Stock abcStock){
        this.abcStock = abcStock;
    }

    public void execute() {
        abcStock.sell();
    }
}
