package C18___13_09_2022__Design_Patterns.behavioral.command;

public interface Order {
    void execute();
}
