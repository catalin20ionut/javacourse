package C18___13_09_2022__Design_Patterns.behavioral.command;

public class BuyStock implements Order {
    private final Stock abcStock;

    public BuyStock(Stock abcStock){
        this.abcStock = abcStock;
    }

    public void execute() {
        abcStock.buy();
    }
}
