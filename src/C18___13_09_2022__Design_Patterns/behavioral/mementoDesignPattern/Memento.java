package C18___13_09_2022__Design_Patterns.behavioral.mementoDesignPattern;

public class Memento {
    private String state;

    public Memento(String state){
        this.state = state;
    }

    public String getState(){
        return state;
    }
}

