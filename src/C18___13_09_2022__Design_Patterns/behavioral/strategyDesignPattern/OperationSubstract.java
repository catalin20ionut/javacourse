package C18___13_09_2022__Design_Patterns.behavioral.strategyDesignPattern;

public class OperationSubstract implements Strategy{
    @Override
    public int doOperation(int num1, int num2) {
        return num1 - num2;
    }
}
