package C18___13_09_2022__Design_Patterns.behavioral.strategyDesignPattern;

public interface Strategy {
    int doOperation(int num1, int num2);
}
