package C18___13_09_2022__Design_Patterns.behavioral.iteratorDesignPattern;

public interface Iterator {
    boolean hasNext();
    Object next();
}
