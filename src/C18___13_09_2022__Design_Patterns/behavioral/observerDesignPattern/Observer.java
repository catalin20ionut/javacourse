package C18___13_09_2022__Design_Patterns.behavioral.observerDesignPattern;

public abstract class Observer {
    protected Subject subject;
    public abstract void update();
}
