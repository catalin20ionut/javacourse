package C18___13_09_2022__Design_Patterns.behavioral.interpreterDesignPattern;

public interface Expression {
    boolean interpret(String context);
}
