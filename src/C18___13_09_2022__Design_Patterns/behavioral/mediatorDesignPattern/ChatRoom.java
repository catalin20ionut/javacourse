package C18___13_09_2022__Design_Patterns.behavioral.mediatorDesignPattern;

import java.util.Date;

public class ChatRoom {
    public static void showMessage(User user, String message){
        System.out.println(new Date() + " ["
                + user.getName() + "] : " + message);
    }
}

