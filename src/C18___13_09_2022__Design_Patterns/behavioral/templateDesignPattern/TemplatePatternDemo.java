package C18___13_09_2022__Design_Patterns.behavioral.templateDesignPattern;

public class TemplatePatternDemo {
    public static void main(String[] args) {

        Game game = new Cricket();
        game.play();
        System.out.println();
        game = new Football();
        game.play();
    }
}
