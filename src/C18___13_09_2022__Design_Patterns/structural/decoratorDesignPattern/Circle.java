package C18___13_09_2022__Design_Patterns.structural.decoratorDesignPattern;

public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Circle");
    }
}
