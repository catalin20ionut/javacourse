package C18___13_09_2022__Design_Patterns.structural.proxyDesignPattern;

public class ProxyPatternDemo {

    public static void main(String[] args) {
        Image image = new ProxyImage("test_10mb.jpg");

        //image will be loaded from disk
        image.display();

        System.out.println("");

        //image will not be loaded from disk
        image.display();
    }
}
