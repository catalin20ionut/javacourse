package C18___13_09_2022__Design_Patterns.structural.proxyDesignPattern;

public interface Image {
    void display();
}
