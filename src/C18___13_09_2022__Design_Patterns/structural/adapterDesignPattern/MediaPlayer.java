package C18___13_09_2022__Design_Patterns.structural.adapterDesignPattern;

public interface MediaPlayer {
    void play(String audioType, String fileName);
}
