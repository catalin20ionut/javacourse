package C18___13_09_2022__Design_Patterns.structural.adapterDesignPattern;

public interface AdvancedMediaPlayer {
    void playVlc(String fileName);
    void playMp4(String fileName);
}
