package C18___13_09_2022__Design_Patterns.structural.bridgeDesignPattern;

public interface DrawAPI {
    void drawCircle(int radius, int x, int y);
}
