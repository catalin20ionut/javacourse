package C18___13_09_2022__Design_Patterns.structural.facadeDesignPattern;

public interface Shape {
    void draw();
}
