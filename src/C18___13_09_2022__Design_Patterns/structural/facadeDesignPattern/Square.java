package C18___13_09_2022__Design_Patterns.structural.facadeDesignPattern;

public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("Square::draw()");
    }
}

