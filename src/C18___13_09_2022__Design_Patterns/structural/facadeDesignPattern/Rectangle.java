package C18___13_09_2022__Design_Patterns.structural.facadeDesignPattern;

public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("Rectangle::draw()");
    }
}
