package C18___13_09_2022__Design_Patterns.structural.facadeDesignPattern;

public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Circle::draw()");
    }
}
