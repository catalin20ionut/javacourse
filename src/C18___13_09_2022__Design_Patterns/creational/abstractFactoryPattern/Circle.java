package C18___13_09_2022__Design_Patterns.creational.abstractFactoryPattern;

public class Circle implements Shape{

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}
