package C18___13_09_2022__Design_Patterns.creational.abstractFactoryPattern;

public abstract class AbstractFactory {
    abstract Color getColor(String color);
    abstract Shape getShape(String shape);
}
