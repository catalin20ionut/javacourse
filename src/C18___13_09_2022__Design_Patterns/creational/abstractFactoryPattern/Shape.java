package C18___13_09_2022__Design_Patterns.creational.abstractFactoryPattern;

public interface Shape {
    void draw();
}
