package C18___13_09_2022__Design_Patterns.creational.abstractFactoryPattern;

public class Blue implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Blue::fill() method.");
    }
}
