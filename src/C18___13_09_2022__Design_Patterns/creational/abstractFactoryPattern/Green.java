package C18___13_09_2022__Design_Patterns.creational.abstractFactoryPattern;

public class Green implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Green::fill() method.");
    }
}
