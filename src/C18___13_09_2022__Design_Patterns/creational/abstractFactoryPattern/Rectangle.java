package C18___13_09_2022__Design_Patterns.creational.abstractFactoryPattern;

public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}