package C18___13_09_2022__Design_Patterns.creational.singletonPattern;

public class LazyInstantiation {
    private static LazyInstantiation obj;
    private LazyInstantiation(){}

    public static LazyInstantiation getLazyInstantiation(){
        if (obj == null){
            //instance will be created at request time
            obj = new LazyInstantiation();
        }
        return obj;
    }

    public void doSomething(){
            //write your code
    }
}
