package C18___13_09_2022__Design_Patterns.creational.singletonPattern;

public class SingleObject {

    //create an object of SingleObject
    private static SingleObject instance = new SingleObject();

    //make the constructor private so that this class cannot be
    //instantiated
    private SingleObject(){}

    //Get the only object available
    public static SingleObject getInstance(){
        return instance;
    }

    public void showMessage(){
        System.out.println("Hello World!");
    }
    public void showMessage1(){
        System.out.println("Hello World!");
    }
    public void showMessage3(){
        System.out.println("Hello World!");
    }
}