package C18___13_09_2022__Design_Patterns.creational.singletonPattern;

public class EarlyInstantiation {
    //Early, instance will be created at load time
    private static EarlyInstantiation obj=new EarlyInstantiation();
    private EarlyInstantiation(){}

    public static EarlyInstantiation getEarlyInstantiation(){
        return obj;
    }

    public void doSomething(){
        //write your code
    }
}
