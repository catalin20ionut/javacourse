package C18___13_09_2022__Design_Patterns.creational.builderPattern;

public interface Packing {
    String pack();
}
