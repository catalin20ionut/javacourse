package C18___13_09_2022__Design_Patterns.creational.builderPattern.tema;

public interface Packing {
    String pack();
    int price();
}
