package C18___13_09_2022__Design_Patterns.creational.builderPattern.tema;

public class Samsung extends Company {
    @Override
    public int price(){
        return 15;
    }
    @Override
    public String pack(){
        return "Samsung CD";
    }
}
