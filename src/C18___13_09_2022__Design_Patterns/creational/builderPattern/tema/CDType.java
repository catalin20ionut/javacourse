package C18___13_09_2022__Design_Patterns.creational.builderPattern.tema;

import java.util.ArrayList;
import java.util.List;

public class CDType {
    private List<Packing> items=new ArrayList<Packing>();
    public void addItem(Packing packs) {
        items.add(packs);
    }
    public void showItems(){
        for (Packing packing : items){
            System.out.print("CD name : "+packing.pack());
            System.out.println(", Price : "+packing.price());
        }
    }
}
