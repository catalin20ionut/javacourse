package C18___13_09_2022__Design_Patterns.creational.builderPattern;

public class Wrapper implements Packing {

    @Override
    public String pack() {
        return "Wrapper";
    }
}
