package C18___13_09_2022__Design_Patterns.creational.factoryPattern;

public interface Shape {
    void draw();
}
