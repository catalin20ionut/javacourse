package C18___13_09_2022__Design_Patterns.creational.factoryPattern;

public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
