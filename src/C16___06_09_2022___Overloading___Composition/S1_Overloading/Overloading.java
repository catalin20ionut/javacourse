package C16___06_09_2022___Overloading___Composition.S1_Overloading;

public class Overloading {
    private int x;
    private int y;
    private double z;

    public int sum (){
        x = 2;
        y = 5;
        return  x + y;
    }

    public int sum (int a, int b) {
        x = a;
        y = b;
        return x + y;
    }

    public int sum (int v, int l, int o) {
        return v + l + o;
    }

    public double sum (int b,  double h){
        x = b;
        z = h;
        return x + z;
    }

    void main(){
        System.out.println("5. -> " + "I overloaded main.");
    }
    void main(int a, int b){
        System.out.println("6. -> " + (a +b));
    }
    void main(float a, float b){
        System.out.println("7. -> " + (a +b));
    }
    void main(double a, double b){
        System.out.println("8. -> " + (a +b));
    }
    void main(long a, long b){
        System.out.println("9. -> " + (a +b));
    }
    void main(String a, String b){
        System.out.println("10. -> " + (a +b));
    }

    public static void main(String[] args) {
        Overloading overloading = new Overloading();
        System.out.println("1. -> " + overloading.sum());
        System.out.println("2. -> " + overloading.sum(3, 8));
        System.out.println("3. -> " + overloading.sum(1, 3, 7));
        System.out.println("4. -> " + overloading.sum(3, 9.8));
        overloading.main();
        overloading.main(2, 10);
        overloading.main(2f, 10f);
        overloading.main(2d, 10d);
        overloading.main(2L, 10L);
        overloading.main("2", "10");
    }
}