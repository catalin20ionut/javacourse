package C16___06_09_2022___Overloading___Composition.S2_Composition;

/**
PC              - carcasă, monitor, placă de bază
Monitor         - rezoluție, model, producător, mărime, metoda deseneazăPixeli
PlacaDeBaza     - model, producător, ram, cardSlot, bios, metoda incarcaPrograme
Rezoluție       - înălțime, lățime
Carcasa         -  model, producător, tip alimentare, dimensiuni, metoda apasarePeButon
Dimensiuni      - înălțime, lățime, adâncime
C22shi___19_10_2022.Main            - compuneți un PC apoi folosiți metodele sa apăsați pe butonul carcasei să porniți PC-ul, placa de bază
încarcă programele și monitorul desenează pixelii, să apăsați pe butonul carcasei să opriți PC-ul.                   */

public class Carcasa {
    private String model;
    private String producator;
    private String tipAlimentare;
    private Dimensiuni dimensiuni;

    public Carcasa(String model, String producator, String tipAlimentare, Dimensiuni dimesiuni) {
        this.model = model;
        this.producator = producator;
        this.tipAlimentare = tipAlimentare;
        this.dimensiuni = dimesiuni;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public String getTipAlimentare() {
        return tipAlimentare;
    }

    public void setTipAlimentare(String tipAlimentare) {
        this.tipAlimentare = tipAlimentare;
    }

    public Dimensiuni getDimensiuni() {
        return dimensiuni;
    }

    public void setDimensiuni(Dimensiuni dimensiuni) {
        this.dimensiuni = dimensiuni;
    }

    public void apasaPeButon(boolean on){
        if (on) {
            System.out.println("Calculatorul a pornit.");
        } else {
            System.out.println("Calculatorul este oprit.");
        }
    }
}
