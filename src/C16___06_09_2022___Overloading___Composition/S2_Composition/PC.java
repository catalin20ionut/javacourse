package C16___06_09_2022___Overloading___Composition.S2_Composition;
/**
PC              - carcasă, monitor, placă de bază
Monitor         - rezoluție, model, producător, mărime, metoda deseneazăPixeli
PlacaDeBaza     - model, producător, ram, cardSlot, bios, metoda incarcaPrograme
Rezoluție       - înălțime, lățime
Carcasa         -  model, producător, tip alimentare, dimensiuni, metoda apasarePeButon
Dimensiuni      - înălțime, lățime, adâncime
C22shi___19_10_2022.Main            - compuneți un PC apoi folosiți metodele sa apăsați pe butonul carcasei să porniți PC-ul, placa de bază
încarcă programele și monitorul desenează pixelii, să apăsați pe butonul carcasei să opriți PC-ul.                   */

public class PC {
    private Carcasa carcasa;
    private Monitor monitor;
    private PlacaDeBaza placaDeBaza;

    public PC(Carcasa carcasa, Monitor monitor, PlacaDeBaza placaDeBaza) {
        this.carcasa = carcasa;
        this.monitor = monitor;
        this.placaDeBaza = placaDeBaza;
    }

    public Carcasa getCarcasa() {
        return carcasa;
    }

    public void setCarcasa(Carcasa carcasa) {
        this.carcasa = carcasa;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public PlacaDeBaza getPlacaDeBaza() {
        return placaDeBaza;
    }

    public void setPlacaDeBaza(PlacaDeBaza placaDeBaza) {
        this.placaDeBaza = placaDeBaza;
    }
}
