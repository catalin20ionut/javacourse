package C16___06_09_2022___Overloading___Composition.S2_Composition;

/**
PC              - carcasă, monitor, placă de bază
Monitor         - rezoluție, model, producător, mărime, metoda deseneazăPixeli
PlacaDeBaza     - model, producător, ram, cardSlot, bios, metoda incarcaPrograme
Rezoluție       - înălțime, lățime
Carcasa         -  model, producător, tip alimentare, dimensiuni, metoda apasarePeButon
Dimensiuni      - înălțime, lățime, adâncime
C22shi___19_10_2022.Main            - compuneți un PC apoi folosiți metodele sa apăsați pe butonul carcasei să porniți PC-ul, placa de bază
încarcă programele și monitorul desenează pixelii, să apăsați pe butonul carcasei să opriți PC-ul.                   */

public class Dimensiuni {
    private double inaltime;
    private double latime;
    private double adancime;

    public Dimensiuni(double inaltime, double latime, double adancime) {
        this.inaltime = inaltime;
        this.latime = latime;
        this.adancime = adancime;
    }
}
