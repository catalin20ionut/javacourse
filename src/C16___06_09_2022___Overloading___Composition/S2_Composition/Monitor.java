package C16___06_09_2022___Overloading___Composition.S2_Composition;
/**
PC              - carcasă, monitor, placă de bază
Monitor         - rezoluție, model, producător, mărime, metoda deseneazăPixeli
PlacaDeBaza     - model, producător, ram, cardSlot, bios, metoda incarcaPrograme
Rezoluție       - înălțime, lățime
Carcasa         -  model, producător, tip alimentare, dimensiuni, metoda apasarePeButon
Dimensiuni      - înălțime, lățime, adâncime
C22shi___19_10_2022.Main            - compuneți un PC apoi folosiți metodele sa apăsați pe butonul carcasei să porniți PC-ul, placa de bază
încarcă programele și monitorul desenează pixelii, să apăsați pe butonul carcasei să opriți PC-ul.                   */

public class Monitor {
    private Rezolutie rezolutie;
    private String model;
    private String producator;
    private char marime;

    public Monitor(Rezolutie rezolutie, String model, String producator, char marime) {
        this.rezolutie = rezolutie;
        this.model = model;
        this.producator = producator;
        this.marime = marime;
    }

    public Rezolutie getRezolutie() {
        return rezolutie;
    }

    public void setRezolutie(Rezolutie rezolutie) {
        this.rezolutie = rezolutie;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public char getMarime() {
        return marime;
    }

    public void setMarime(char marime) {
        this.marime = marime;
    }

    public void deseneazaPixel(int x, int y, String culoare){
        System.out.println("Pixelul de culoarea\u001B[32m " + culoare +
                "\u001B[0m a fost desenat la poziția \u001B[32m" + x + y + "\u001B[0m.");
    }
}
