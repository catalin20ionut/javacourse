package C16___06_09_2022___Overloading___Composition.S2_Composition;

/**
PC              - carcasă, monitor, placă de bază
Monitor         - rezoluție, model, producător, mărime, metoda deseneazăPixeli
PlacaDeBaza     - model, producător, ram, cardSlot, bios, metoda incarcaPrograme
Rezoluție       - înălțime, lățime
Carcasa         -  model, producător, tip alimentare, dimensiuni, metoda apasarePeButon
Dimensiuni      - înălțime, lățime, adâncime
C22shi___19_10_2022.Main            - compuneți un PC apoi folosiți metodele sa apăsați pe butonul carcasei să porniți PC-ul, placa de bază
încarcă programele și monitorul desenează pixelii, să apăsați pe butonul carcasei să opriți PC-ul.                   */

public class PlacaDeBaza {
    private String model;
    private String producator;
    private int ram;
    private boolean cardSlot;
    private String bios;

    public PlacaDeBaza(String model, String producator, int ram, boolean cardSlot, String bios) {
        this.model = model;
        this.producator = producator;
        this.ram = ram;
        this.cardSlot = cardSlot;
        this.bios = bios;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public boolean isCardSlot() {
        return cardSlot;
    }

    public void setCardSlot(boolean cardSlot) {
        this.cardSlot = cardSlot;
    }

    public String getBios() {
        return bios;
    }

    public void setBios(String bios) {
        this.bios = bios;
    }

    public void incarcaPrograme(String numeProgram){
        System.out.println("Programul \u001B[33m" + numeProgram + "\u001B[0m a fost încărcat.");
    }
}
