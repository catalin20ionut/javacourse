package C16___06_09_2022___Overloading___Composition.S2_Composition;

/**
PC              - carcasă, monitor, placă de bază
Monitor         - rezoluție, model, producător, mărime, metoda deseneazăPixeli
PlacaDeBaza     - model, producător, ram, cardSlot, bios, metoda incarcaPrograme
Rezoluție       - înălțime, lățime
Carcasa         -  model, producător, tip alimentare, dimensiuni, metoda apasarePeButon
Dimensiuni      - înălțime, lățime, adâncime
C22shi___19_10_2022.Main            - compuneți un PC apoi folosiți metodele sa apăsați pe butonul carcasei să porniți PC-ul, placa de bază
încarcă programele și monitorul desenează pixelii, să apăsați pe butonul carcasei să opriți PC-ul.                   */

public class Main {
    public static void main(String[] args) {
    Dimensiuni dimensiuni = new Dimensiuni(12.9, 10.3, 11.4);
    Carcasa carcasa = new Carcasa("Cub", "Asus", "AC", dimensiuni);
    PlacaDeBaza placaDeBaza = new PlacaDeBaza("AMD", "Dell", 256, true, "Bios");
    Rezolutie rezolutie = new Rezolutie(198.9, 180.01);
    Monitor monitor = new Monitor(rezolutie, "HP", "Huawei", 'L');
    PC pc= new PC(carcasa, monitor, placaDeBaza);

    pc.getCarcasa().apasaPeButon(true);
    pc.getPlacaDeBaza().incarcaPrograme("Windows");
    pc.getMonitor().deseneazaPixel(90, 80, "verde");
    pc.getCarcasa().apasaPeButon(false);
    }
}
