package C19___15_09_2022___Enum___Exceptions.Exceptions;

public class FonduriInsuficienteExceptie extends Exception {
    public double account;
    public double getAccount() {
        return account;
    }
    public void setAccount(double account) {
        this.account = account;
    }
    public FonduriInsuficienteExceptie(double account) {
        this.account = account;
    }
}