package C19___15_09_2022___Enum___Exceptions.Exceptions;

import java.util.ArrayList;
import java.util.List;

/*
 Create a new class for a bank account
 Create fields for the account number, balance, list of transactions.

 Create getters and setters for each field
 Create two additional methods
 1. To allow the customer to deposit funds (this should increment the balance field).
 2. To allow the customer to withdraw funds. This should deduct from the balance field,
 but not allow the withdrawal to complete if their are insufficient funds, throw an exception in this case.
3.Calculate the balance after all the transactions are completed, you can not have negative balance, throw an
 exception if there is the case.
 */
public class BankAccount {
    public String accNumber;
    public double balance;
    List<Double> transactions = new ArrayList<>();

    public BankAccount(String accNumber, double balance) {
        this.accNumber = accNumber;
        this.balance = balance;
    }

    public String getAccNumber() {
        return accNumber;
    }
    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = 1000;
    }

    public void depunere(double dep) {
        if (dep > 0) {
            this.balance += dep;
            transactions.add(dep);}
        else {
            throw new RuntimeException(" NU poti depune sume negative");}
    }

    public void extrage(double extr) throws FonduriInsuficienteExceptie {
        if (extr > 0  && extr <= this.balance) {
            this.balance -= extr;
            transactions.add(-extr);}
        else {
            double rest = balance - extr;
            System.out.println("You can withdraw only " + balance);
            throw new FonduriInsuficienteExceptie(rest);}
    }

    public void balantaFinala (){
        setBalance(balance);
        for(double tranz: this.transactions){
            this.balance += tranz;}
        System.out.println(this.balance);
    }
}