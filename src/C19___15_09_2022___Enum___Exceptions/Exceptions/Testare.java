package C19___15_09_2022___Enum___Exceptions.Exceptions;

public class Testare {
    public static void main(String[] args) throws FonduriInsuficienteExceptie {
        BankAccount contulMeu = new BankAccount ("1253659WRXT", 1000);
        contulMeu.balantaFinala();
        contulMeu.depunere(200.7);
        contulMeu.balantaFinala();
        contulMeu.depunere(100.5);
        contulMeu.balantaFinala();
        contulMeu.extrage(100.00);
        contulMeu.balantaFinala();
        contulMeu.extrage(100.00);
        contulMeu.balantaFinala();
        contulMeu.extrage(1200.00);
        contulMeu.balantaFinala();
    }
}