package C19___15_09_2022___Enum___Exceptions.Enum;

public class TestEnum {
    public static void main(String[] args) {
        Animals animal = Animals.DOG;
        System.out.println("1. " + Animals.MOUSE);
        System.out.println("2. " + Animals.DOG.name()); // nu mai ține cont de numele lui Dog.
        System.out.println("3. " + Animals.DOG.getName()); // It is the value
        System.out.println("4. " + Animals.DOG.getClass());
        System.out.println(Animals.DOG instanceof Object); // always true because all are objects
        System.out.println(Animals.DOG instanceof Enum); // always true because all are objects Enum is an Object


        Animals animal1 = Animals.valueOf("DOG");
        System.out.println("7. " + animal1);

        switch (animal1){
            case CAT:
                System.out.println("8. Cat");
                break;
            case DOG:
                System.out.println("8. Dog");
                break;
            case MOUSE:
                System.out.println("8. Mouse");
                break;
            default:
                System.out.println("8. No more animals!");
                break;
        }
    }
}