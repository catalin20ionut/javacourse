package C19___15_09_2022___Enum___Exceptions.Enum;

public enum Animals {
    CAT("Tom"),
    MOUSE("Jerry"),
    DOG("Brownie");
    private String name;

    Animals (String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animals -- {" + "name = '" + name + '\'' + '}';
    }
}