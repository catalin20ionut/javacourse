package ProjectSchool2;

import ProjectSchool.User;

import java.util.ArrayList;
import java.util.List;

public class School {
    private final String schoolName;
    private List<ProjectSchool.User> schoolMembers = new ArrayList<>();

    public School(String schoolName) {
        this.schoolName = schoolName;
    }

    public void addUser(User userName) {
        if (schoolMembers.contains(userName)) {
            throw new RuntimeException("The user has already been registered.");
        } else {
            System.out.println("The user " + userName.getUserName() + " is added to the school list.");
            schoolMembers.add(userName);
        }
    }
    public int getUserNo() {
        return this.schoolMembers.size();
    }
}
