package ProjectSchool2;

public enum EnumGrades {
    GRADE_0("Clasa pregătitoare"),
    GRADE_1("Clasa întâi"),
    GRADE_2("Clasa a doua"),
    GRADE_3("Clasa a treia"),
    GRADE_4("Clasa a patra"),
    GRADE_5("Clasa a cincea"),
    GRADE_6("Clasa a șasea"),
    GRADE_7("Clasa a șaptea"),
    GRADE_8("Clasa a opta"),
    GRADE_9("Clasa a noua"),
    GRADE_10("Clasa a zecea"),
    GRADE_11("Clasa a unsprezecea"),
    GRADE_12("Clasa a douăsprezecea");
    public final String gradeName;

    EnumGrades(String gradeName) {
        this.gradeName = gradeName;
    }
    public String getGradeName () {
        return gradeName;
    }
}